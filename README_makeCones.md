## Author: Kevin Hildebrand
## Email: khildebrand@uchicago.edu

This package is for creating the A distributions of random cones ( A is difference in pT between two random cones).


## What this package does:

The makeCones package takes trees produced by the RandomCone packackage (located in tree maker) and generates random cones and produces distributions of the difference in pT between two random cones.
They can be ran locally or submitted in batch. I used a condor batch system and have included examples of how I submitted it. Also included are examples of how to run locally.
The A distributions are stored in 3-d histograms giving you A in blocks of eta as function of <mu> and NPV.

## How to configure this package:

Configuration of this package is set in a config file (makeCones.config). This file allows one to set the following:
1. name of the input tree (This should be the same as the output tree produced by the RandomCone package (located in TreeMaker)
2. Whether or not this is MC sample (default is false,example in config script "isMC	True").
3. Whether or not to scale <mu> by the scale factor (default is false, example in config script "muScale True").
4. What mu scale factor to use (default is 1.0, example in config script "muScaleFactor 1.16"). The value you set this to is the denominator. So if you set it to 1.16, the mu values are scaled by 1/1.16.
5. What cone radius to use for the random cones (default is 0.4, example in config script "coneRadius 0.6").
6. What eta blocks should be used for the primary A distributions. This should be a comma separated list. (default is "0,.8", example in config script "etaBlocks 0,.8,1.2,2.1,2.8,3.2,3.6,4.5"). So if for example you set this to x,y,z then there will be an A distribution for x<|eta|<y and y<|eta|<z.
7. Whether or not to do a second range of eta blocks (default is false, example in config script "doSecondary True").
	7a. What eta blocks should be used for the secondary A distributions. This should be a comma separated list. (default is "0,2.1", example in config script "etaBlocksSecondary 0,.8,1.2,2.1,2.8,3.2,3.6,4.5"). So if for example you set this to x,y,z then there will be an A distribution for x<|eta|<y and y<|eta|<z.
8. What range (in MeV) should be used in the A distribution histograms (default is 20000,example in config script "range 30000"). The range of the A histrograms will be from -range to range.
9. How many bins to use in the A distribution histograms (default is 1000, example in config script "nBinsForA 2000").
10. What the maximum number of primary vertex can be in the A distribution histogram (default is 30, example in config script "maxNPV 45")
11. What the minimum number of primary vertex can be in the A distribution histogram (default is 0, example in config script "minNPV 5")
12. What the maximum number of <mu> can be in the A distribution histogram (default is 35, example in config script "maxMu 45")
13. What the maximum number of <mu> can be in the A distribution histogram (default is 0, example in config script "minMu 5")


## How to run this package:

First make sure you have root setup. (On lxplus once you have atlas setup you can just do "localSetupROOT").

Once you have set the config script this package is very simply run by just providing the input file and ouput file.
Then from within the makeCones directory just type the following command:
```
root -b -q 'AtlasStyle.C' 'makeCones.cxx ("<full path to input root File>","<full path to output file>")'
```
So if for example you have /some/path/input.root that was produced by the RandomCone package (located in TreeMaker) and you want the output to be called output.root in some other directory you would do:
```
root -b -q 'AtlasStyle.C' 'makeCones.cxx ("/some/path/input.root","/some/other/path/output.root")'
```
The above is for running locally. To run on batch will depend on what you have available. So I will not go into detail on submitting this as batch. The two files CondorSubmitMakeCones.submit and CondorTestMakeCones.sh are two files I used for submitting this code to run on Condor.

## Understanding the output:

The output root file contains a number of histograms. 
There are of course the A distribution histograms which show the
A distribution as a function of `<mu>` and NPV for the different eta blocks.
These are stored as `R4ConeEM_etaSlicesPos#_#` and `R4ConeLC_etaSlicesPos#_#`.
The #_# tells which eta block the A distribution is for.

Let us assume you have etaBlocks in the config file set to x,y,z.
Then the output should contain the histograms 
`R4ConeEM_etaSlicesPos0_1` and `R4ConeEM_etaSlicesPos1_2`

The number corresponds to the position in the etaBlocks string. That is:
`R4ConeEM_etaSlicesPos0_1` is for the etaBlock `x<|eta|<y`

`R4ConeEM_etaSlicesPos1_2` is for the etaBlock `y<|eta|<z`

This same description applies if you included a secondary etaBlock in the config
file.

For the primary etaBlock ranges there is also a corresponding histogram for 
each of teh A distribution histograms that tells you how events were used 
and how many events were skipped in teh corresponding A distribution histogram.
Taking R4ConeEM_etaSlicesPos0_1 for example. There is a corresponding histogram 
called `emptyConesSkippedHistoEM0_1`. The number of events in bin 0 is the 
number of events that were used in `R4ConeEM_etaSlicesPos0_1` and the number of 
events in bin 1 is the number of events that were skipped and not included in 
`R4ConeEM_etaSlicesPos0_1`.

No such emptyConesSkipped histograms are saved for the secondary eta blocks.

There are 3 other histograms that are included in the output. They are the <mu>, corrected <mu>, and NPV distribution for the entire input file.

## More details on how this script produces the A distribution histograms:

The random cones are produced and the difference in pT between two random cones is all done within the method `calculateDifference3Sym`. 
This method has the following inputs:
```
calculateDifference3Sym(Cluster PT,Cluster E,Cluster M,Cluster phi,Cluster eta,lower Edge Eta Block,upper Edge Eta Block,cone Radius,Maximum number of tries);
```
- The first 5 are just the kinematics of all the clusters in the event.
- Lower/Upper edge eta block are the range in eta that the random cones should be generated in. 
- So if `lower=.8` and `upper=1.2` the random cones will be generated with eta in range `0.8<|eta|<1.2.`
Cone Radius is the radius of the random Cones.
Maximum number of tries is the max number of tries the code will attempt to generate new random cones because one of them was empty.

How difference in pT is determined:
Step1:
	The first random cone is generated at random phi and random eta (restricted to be in eta block range).
	The second random cone is then generated at phi +pi (where phi is phi of the first random cone) 
	and a new random eta (restricted to be in the eta block range).
If either of these two random cones is empty (ie there's no clusters in one of the cones) 
then return to start of step 1 unless you have already attempted step 1 the maximum number of times.
Step2:
	If couldn't find 2 non-empty cones then the event is skipped. 
	Otherwise, the cluseters in each random cone are summed up and the 
	difference between the pT of the two random cones is plotted.
	
