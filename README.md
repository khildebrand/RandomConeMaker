Author: Kevin Hildebrand
Email: khildebrand@uchicago.edu

This project contains packages for taking trees produced by the package RandomCone (in the project TreeMaker) and determining the noise term at the constituent scale.

The package makeCones uses the trees and random cones to produce 3-d histograms of the  A distributions ( A is difference in pT between two random cones) in blocks of eta as function of <mu> and NPV.

The package SavePlots takes the results from the package makeCones and produces the plots of the noise term at the constituent scale without error bars. The package ReweightHistograms separate from this package
is used to properly weight MC samples.

The package ToyGenerate uses toys to estimate the errors for the plots of the noise term at the constituent scale.

The project NoiseTermJES is then used to take the noise term at the constituent scale to the noise term at the JES.
