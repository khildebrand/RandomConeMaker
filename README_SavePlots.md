## Author: Kevin Hildebrand
## Email: khildebrand@uchicago.edu

This package is for taking the 3-d A histograms produced from the "makeCones" package and making plots of the width of the A distribution based on different restrictions (eta,<mu>,NPV)
It also will produce a root file that is used to produce toys to estimate the error bars on the width of the A distribution.

## What this package does:

This package takes the results from the makeCones package and produces plots of the noise term at the constituent scale.

## How to configure this package:

The only thing that is not configured in the config script is what file type to save the plots as. This can be changed at the very top of the script. The default is to save the plots as png and that is all. You can change it so that they are also saved as pdf.

Configuration of this package is set in a config file (SavePlots.config). This file allows one to set the following (Numbers with a '*' in them means they need to be the same as the ones in makeCones.config):

1. The output directory to save the plots in (default is blah, example in config script is `outPutDir /share/home/khildebr/qualTask/plots/outhistV201_300Data_scale/`) This directory must be created before running the script. If it isn't the script will complain that it doesn't exist. If plots of the same name already exist in the directory they will be overwritten.
2. Whether or not to run on data root files (default is true, example in config script is `doData true`)
  * The full path of the data root file (default is blah, example in config script is `AddedFileData /atlas/uct3/data/users/khildebr/outhistV300Data_scale/allHistosAdded.root`)
3. Whether or not to run on MC root files (default is true, example in config script is `doMC true`)
  * The full path of the MC root file (default is blah, example in config script is `AddedFileMC /atlas/uct3/data/users/khildebr/RandomConeV201MCweighted/allHistosAddedMC.root`)	
4. Name of the output file for doing the Toys (default is blah, example in config script is `ToyTestInputFile ToyTestInputFile_V201_300Data_scale.root`)
5. Name of the output file containing pileup histograms (default is blah, example in config script is `pileUpOutput pileUpOutputV201_300Data_scale.root`)
6. * What eta blocks should be used for the primary A distributions. This should be a comma separated list. (default is "0,.8", example in config script `etaBlocks 0,.8,1.2,2.1,2.8,3.2,3.6,4.5`). So if for example you set this to x,y,z then there will be an A distribution for x<|eta|<y and y<|eta|<z.
7. Whether or not to do a second range of eta blocks (default is false, example in config script `doSecondary True`).
  * What eta blocks should be used for the secondary A distributions. This should be a comma separated list. (default is "0,2.1", example in config script `etaBlocksSecondary 0,.8,1.2,2.1,2.8,3.2,3.6,4.5`). So if for example you set this to x,y,z then there will be an A distribution for x<|eta|<y and y<|eta|<z.
8. What eta step to start at (default is 0, example in config script "startEtaStep 0"). For all plots that are the width of the A distribution as function of anything other than eta this sets the lower cut on eta (for both primary and secondary eta blocks). So for example if etaBlocks was x,y,z then setting "startEtaStep 0" would require x<|eta| for the plots and if "startEtaStep 1" would require y<|eta| for the plots.
9. The number of eta steps to take from the starting eta step. (default is 1, example in config script `nEtaStepsToTake 1`). This is the upper cut on eta like "startEtaStep" is the lower cut. It counts out beggining at startEtaStep. So for example if etaBlocks was x,y,z and "startEtaStep 0" then if "nEtaStepsToTake 1" you would have x<|eta|<y but if "nEtaStepsToTake 2" then you would have x<|eta|<z. Be careful that nEtaStepsToTake does not take you past the end of etaBlocks. IE in the previous example if "nEtaStepsToTake 3" would cause the script to crash because there is no etaBlock 3 steps after x.
10. The number of eta steps to take from the starting eta step for secondary etablocks (default is 1, example in config script `nEtaStepsToTakeSecondary 1`). This is the same as the one for primary eta blocks (nEtaStepsToTake) except for secondary eta blocks.
11. *What the maximum number of primary vertex can be in the A distribution histogram (default is 30, example in config script `maxNPV 45`)
12. * What the minimum number of primary vertex can be in the A distribution histogram (default is 0, example in config script `minNPV 5`)
13. * What the maximum number of <mu> can be in the A distribution histogram (default is 35, example in config script `maxMu 45`)
14. * What the maximum number of <mu> can be in the A distribution histogram (default is 0, example in config script `minMu 5`)	
15. Whether or not to produce plots of pileup histograms that were produced by makeCones package. (default is false, example in config script `doMuComparisonsFromMakePlots4 true`)

## How to run this package:

First make sure you have root setup. (On lxplus once you have atlas setup you can just do `localSetupROOT`).

This script requires that all data histograms be in a single root file and all MC histograms be in a single root file. The output from the makeCones package however is in multiple root files. The data root files
can be simply added together using `hadd`.

Assuming they are all located in one directory and that you have root setup just go into the directory with all your data files and do "hadd allHistosAdded.root *.root" this will add all root files in the current directory
together into the root file `addedRootFiles.root`.

For MC if you are using more than one dataset then you need to weight the histograms appropriately before adding them together. The package "ReweightHistograms" can be used to do this. See the documentation 
included in that package on how to do this. Now assuming you used the package "ReweightHistograms" you can then hadd together all the weighted MC histograms into a single root file. Let's assume you called it
`allHistosAddedMC.root`.

Now once you have your input root files and config script setup just run the command:

```
root -b -q 'AtlasStyle.C' 'SavePlots.cxx ()'
```

## How the width is computed:

The width is computed from the A distribution as follows. It takes the mean of the A distribution and defines this as the center. It then moves one bin at a time to the right until the area betweeen the center the bin it is at is greater than 0.6827/2.0 (34%) of the total area of the histogram.
It then does the same thing except moving one bin 1 left at a time from the center. The width is then defined as the difference between these two bins divided by 2. This width is then divided by sqrt(2) to give the noise term at the constituent scale.

## All the plots that are produced:

For plots with `#_#` in the name (where # is some number ie 0_1) this tells which eta block the plot is for (this is also printed on the actual plot) in the same was as is described for the makeCones package documentation.

There are the emptyConesSkipped plots as described in the makeCones package documentation.
There are plots showing the <mu> and NPV distributions of data and MC.
There are plots showing the noise term as function of eta, as a function of <mu> with no restriction on NPV,as a function of <mu> with NPV restricted to a single value, and as a function of NPV.