bool debug=false;
bool savePNG=true;
bool savePDF=false;
void ATLASLabel3(Double_t x,Double_t y,const char* text,double labelOffset=1.,double tsize=.08,Color_t color = 1);
void ATLASLabel3(Double_t x,Double_t y,const char* text,double labelOffset,double tsize,Color_t color)
{
  TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize);
  l.SetTextSize(tsize);
  l.SetNDC();
  l.SetTextFont(72);
  l.SetTextColor(color);

  double delx = labelOffset*0.115*696*gPad->GetWh()/(472*gPad->GetWw());

  l.DrawLatex(x,y,"ATLAS");
  if (text) {
    TLatex p;
    p.SetTextSize(tsize);
    p.SetNDC();
    p.SetTextFont(42);
    p.SetTextColor(color);
    p.DrawLatex(x+delx,y,text);
  }
}
void saveSingleHisto(TH1* histoOrig,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale=false,bool rebin=false);
void saveSingleHisto(TH1* histoOrig,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);

	if (logScale) {
		c1->SetLogy();
	}
	TLatex latex;
	latex.SetNDC();
	latex.SetTextColor(1);
	latex.SetTextFont(42);
	latex.SetTextSize(.04);

	if (normalizedArea) {
		/*cout<<stack->GetNbinsX()<<" " <<dataHisto->GetNbinsX()<<endl;
		cout<<stack->Integral()<< " " << dataHisto->Integral()<<endl;
		stack->Scale(dataHisto->Integral()/(stack->Integral()));
		cout<<stack->Integral()<<endl;
		//dataHisto->Scale(1/(dataHisto->Integral()));*/

		cout<<histo->GetNbinsX()<<endl;
		cout<<histo->Integral(1,histo->GetNbinsX())<<endl;
		histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
		cout<<histo->Integral(1,histo->GetNbinsX())<<endl;
		//dataHisto->Scale(1/(dataHisto->Integral()));
		//stuffToWrite.push_back("Period A4");
	}
	if (rebin) {
		histo->Rebin();
	}

	histo->Draw("HIST");
	if (logScale) {
		histo->SetMinimum(.2);
		histo->SetMaximum(histo->GetMaximum()*1000);
	}
	histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
	histo->GetXaxis()->SetLabelSize(0.04);
	histo->GetXaxis()->SetNdivisions(505);
	histo->GetXaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
	histo->GetYaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitleOffset(1.75);
	//dataHisto->GetYaxis()->SetLabelSize(.04);
	histo->GetYaxis()->SetLabelSize(.04);


	ATLASLabel3(xStart,yStart,"Internal",.85,.04);
	Double_t x=xStart;Double_t y=yStart-.045;
	for (int j=0;j<stuffToWrite.size();j++) {
		latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
		y-=.045;
	}

	std::string temp(histo->GetName());
	if (rebin) {
		temp=temp+"rebin";
	}
	if (savePNG) {
		c1->Print((outPutDir+temp+".png").c_str(),"png");
	}
	if (savePDF) {
		c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");
	}

}
void saveTwoHisto(TH1* histoOrig,TH1* histoOrig2,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale=false,bool rebin=false);
void saveTwoHisto(TH1* histoOrig,TH1* histoOrig2,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale,bool rebin) {
	cout<<"heyo1"<<endl;
	TH1D *histo = (TH1D*)histoOrig->Clone();
	cout<<"heyo2"<<endl;
	TH1D *histo2 = (TH1D*)histoOrig2->Clone();
	cout<<"heyo3"<<endl;
	TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);
	cout<<"heyo4"<<endl;
	if (logScale) {
		c1->SetLogy();
	}
	TLatex latex;
	latex.SetNDC();
	latex.SetTextColor(1);
	latex.SetTextFont(42);
	latex.SetTextSize(.04);

	if (normalizedArea) {
		histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
		histo2->Scale(1/(histo2->Integral(1,histo2->GetNbinsX())));

	}
	if (rebin) {
		histo->Rebin();
		histo2->Rebin();
	}

	histo->Draw("HIST");
	//histo2->SetLineColor(2);
	histo2->Draw("HIST same");
	if (logScale) {
		histo->SetMinimum(.2);
		histo->SetMaximum(histo->GetMaximum()*1000);
	}
	histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
	histo->GetXaxis()->SetLabelSize(0.04);
	histo->GetXaxis()->SetNdivisions(505);
	histo->GetXaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
	histo->GetYaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitleOffset(1.75);
	//dataHisto->GetYaxis()->SetLabelSize(.04);
	histo->GetYaxis()->SetLabelSize(.04);

	legend->Draw();
	ATLASLabel3(xStart,yStart,"Internal",.85,.04);
	Double_t x=xStart;Double_t y=yStart-.045;
	for (int j=0;j<stuffToWrite.size();j++) {
		latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
		y-=.045;
	}

	std::string temp(histo->GetName());
	if (rebin) {
		temp=temp+"rebin";
	}
	if (!normalizedArea) {
		temp=temp+"NotNormalized";
	}
	if (savePNG) {
		c1->Print((outPutDir+temp+".png").c_str(),"png");
	}
	if (savePDF) {
		c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");
	}

}
void saveTwoHistoMarker(TH1* histoOrig,TH1* histoOrig2,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale=false,bool rebin=false);
void saveTwoHistoMarker(TH1* histoOrig,TH1* histoOrig2,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	TH1D *histo2 = (TH1D*)histoOrig2->Clone();
	if (histo->GetEntries()+histo2->GetEntries()==0) {
		cout<<"histogram was empty"<<endl;
	} else {
		TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);

		if (logScale) {
			c1->SetLogy();
		}
		TLatex latex;
		latex.SetNDC();
		latex.SetTextColor(1);
		latex.SetTextFont(42);
		latex.SetTextSize(.04);

		if (normalizedArea) {
			histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
			histo2->Scale(1/(histo2->Integral(1,histo2->GetNbinsX())));

		}
		if (rebin) {
			histo->Rebin();
			histo2->Rebin();
		}

		histo->Draw("HIST P");
		//histo->GetXaxis()->SetNdivisions(30);
		//histo2->SetLineColor(4);
		histo2->Draw("P same HIST");
		Double_t yMax=0;
		Double_t yMin=0;
		if (histo->GetMaximum()>histo2->GetMaximum()) {
			yMax=histo->GetMaximum()*1.33;
		} else {
			yMax=histo2->GetMaximum()*1.33;
		}
		if (histo->GetMinimum()>histo2->GetMinimum()) {
			yMin=histo2->GetMinimum()*.95;
		} else {
			yMin=histo->GetMinimum()*.95;
		}
		histo->SetMinimum(yMin);
		histo->SetMaximum(yMax);
		if (logScale) {
			histo->SetMinimum(.2);
			histo->SetMaximum(histo->GetMaximum()*1000);
		}
		histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
		histo->GetXaxis()->SetLabelSize(0.04);
		//histo->GetXaxis()->SetNdivisions(505);
		//histo->GetXaxis()->SetLimits(-4.5,4.5);

		histo->GetXaxis()->SetTitleSize(.04);
		histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
		histo->GetYaxis()->SetTitleSize(.04);
		histo->GetYaxis()->SetTitleOffset(1.75);
		//dataHisto->GetYaxis()->SetLabelSize(.04);
		histo->GetYaxis()->SetLabelSize(.04);

		legend->Draw();
		ATLASLabel3(xStart,yStart,"Internal",.85,.04);
		Double_t x=xStart;Double_t y=yStart-.045;
		for (int j=0;j<stuffToWrite.size();j++) {
			latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
			y-=.045;
		}

		std::string temp(histo->GetName());
		if (rebin) {
			temp=temp+"rebin";
		}
		if (savePNG) {
			c1->Print((outPutDir+temp+".png").c_str(),"png");
		}
		if (savePDF) {
			c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");
		}
	}

}
void saveFourHistoMarker(TH1* histoOrig,TH1* histoOrig2,TH1* histoOrig3,TH1* histoOrig4,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale=false,bool rebin=false);
void saveFourHistoMarker(TH1* histoOrig,TH1* histoOrig2,TH1* histoOrig3,TH1* histoOrig4,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	TH1D *histo2 = (TH1D*)histoOrig2->Clone();
	TH1D *histo3 = (TH1D*)histoOrig3->Clone();
	TH1D *histo4 = (TH1D*)histoOrig4->Clone();
	if (histo->GetEntries()+histo2->GetEntries()+histo3->GetEntries()+histo4->GetEntries()==0) {
		cout<<"histograms were empty"<<endl;
	} else {
		TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);

		if (logScale) {
			c1->SetLogy();
		}
		TLatex latex;
		latex.SetNDC();
		latex.SetTextColor(1);
		latex.SetTextFont(42);
		latex.SetTextSize(.04);

		if (normalizedArea) {
			histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
			histo2->Scale(1/(histo2->Integral(1,histo2->GetNbinsX())));
			histo3->Scale(1/(histo3->Integral(1,histo3->GetNbinsX())));
			histo4->Scale(1/(histo4->Integral(1,histo4->GetNbinsX())));

		}
		if (rebin) {
			histo->Rebin();
			histo2->Rebin();
			histo3->Rebin();
			histo4->Rebin();
		}

		histo->Draw("HIST P");
		//histo->GetXaxis()->SetNdivisions(30);
		//histo2->SetLineColor(4);
		histo2->Draw("P same HIST");
		histo3->Draw("P same HIST");
		histo4->Draw("P same HIST");
		Double_t yMax=histo->GetMaximum();
		Double_t yMin=histo->GetMinimum();

		if (histo2->GetMaximum()>yMax) {
			yMax=histo2->GetMaximum();
		}
		if (histo3->GetMaximum()>yMax) {
			yMax=histo3->GetMaximum();
		}
		if (histo4->GetMaximum()>yMax) {
			yMax=histo4->GetMaximum();
		}
		yMax=yMax*1.33;

		if (histo2->GetMinimum()<yMin) {
			yMin=histo2->GetMinimum();
		}
		if (histo3->GetMinimum()<yMin) {
			yMin=histo3->GetMinimum();
		}
		if (histo4->GetMinimum()<yMin) {
			yMin=histo4->GetMinimum();
		}
		yMin=yMin*.95;
		histo->SetMinimum(yMin);
		histo->SetMaximum(yMax);
		if (logScale) {
			histo->SetMinimum(.2);
			histo->SetMaximum(histo->GetMaximum()*1000);
		}
		histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
		histo->GetXaxis()->SetLabelSize(0.04);
		//histo->GetXaxis()->SetNdivisions(505);
		//histo->GetXaxis()->SetLimits(-4.5,4.5);

		histo->GetXaxis()->SetTitleSize(.04);
		histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
		histo->GetYaxis()->SetTitleSize(.04);
		histo->GetYaxis()->SetTitleOffset(1.75);
		//dataHisto->GetYaxis()->SetLabelSize(.04);
		histo->GetYaxis()->SetLabelSize(.04);

		legend->Draw();
		ATLASLabel3(xStart,yStart,"Internal",.85,.04);
		Double_t x=xStart;Double_t y=yStart-.045;
		for (int j=0;j<stuffToWrite.size();j++) {
			latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
			y-=.045;
		}

		std::string temp(histo->GetName());
		if (rebin) {
			temp=temp+"rebin";
		}
		if (savePNG) {
			c1->Print((outPutDir+temp+".png").c_str(),"png");
		}
		if (savePDF) {
			c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");
		}
	}

}
void saveTwoHistoRatio(TH1* histoOrig,TH1* histoOrig2,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale=false,bool rebin=false);
void saveTwoHistoRatio(TH1* histoOrig,TH1* histoOrig2,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	TH1D *histo2 = (TH1D*)histoOrig2->Clone();
	TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);
	TPad *pad1 = new TPad("pad1","pad1",0,0.35,1,1);
	pad1->SetBottomMargin(0.0);
	pad1->SetLeftMargin(0.185);
	pad1->Draw();
	c1->cd();
	TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.35);
	pad2->SetTopMargin(0.0);
	//pad2->SetBottomMargin(0.15);
	pad2->SetBottomMargin(0.3);
	pad2->SetLeftMargin(0.185);
	pad2->Draw();
	//pad2->SetGridy();

	pad1->cd();
	if (logScale) {
		c1->SetLogy();
	}
	TLatex latex;
	latex.SetNDC();
	latex.SetTextColor(1);
	latex.SetTextFont(42);
	latex.SetTextSize(.04);

	if (normalizedArea) {
		histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
		histo2->Scale(1/(histo2->Integral(1,histo2->GetNbinsX())));

	}
	if (rebin) {
		histo->Rebin();
		histo2->Rebin();
	}

	histo->Draw("hist");
	histo2->SetLineColor(2);
	histo2->Draw("hist same");
	if (logScale) {
		histo->SetMinimum(.2);
		histo->SetMaximum(histo->GetMaximum()*1000);
	}
	histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
	histo->GetXaxis()->SetLabelSize(0);
	histo->GetXaxis()->SetNdivisions(505);
	histo->GetXaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
	histo->GetYaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitleOffset(1.75);
	//dataHisto->GetYaxis()->SetLabelSize(.04);
	histo->GetYaxis()->SetLabelSize(.04);


	ATLASLabel3(xStart,yStart,"Internal",.85,.04);
	Double_t x=xStart;Double_t y=yStart-.045;
	for (int j=0;j<stuffToWrite.size();j++) {
		latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
		y-=.045;
	}
	pad2->cd();

	TH1F *h1_ratio = (TH1F*)histo->Clone();
	//h1_ratio->Add(h1_ratio,stack1,1,-1);
	h1_ratio->Divide(histo2);
	//h1_ratio->SetMarkerColor(colourAdd);
	//h1_ratio->GetYaxis()->SetRangeUser(0, 2.);
	h1_ratio->GetYaxis()->SetRangeUser(.5, 1.5);
	h1_ratio->GetYaxis()->SetTitle("Pos/Neg #eta");
	//h1_ratio->SetMaximum(.19);
	//h1_ratio->SetMinimum(-.19);
	h1_ratio->GetYaxis()->SetTitleSize(.12);
	h1_ratio->GetYaxis()->SetTitleOffset(.70);
	h1_ratio->GetYaxis()->SetLabelSize(.11);
	h1_ratio->GetXaxis()->SetTitle(xaxisTitle.c_str());
	h1_ratio->GetXaxis()->SetLabelSize(.11);
	h1_ratio->GetXaxis()->SetTitleOffset(1.1);
	/*h1_ratio->GetYaxis()->SetTitleSize(.08);
	h1_ratio->GetYaxis()->SetTitleOffset(.8);
	h1_ratio->GetYaxis()->SetLabelSize(.08);
	h1_ratio->GetXaxis()->SetTitle(xaxisTitle.c_str());
	h1_ratio->GetXaxis()->SetLabelSize(.08);
	h1_ratio->GetXaxis()->SetTitleOffset(.8);*/
	h1_ratio->GetYaxis()->SetNdivisions(10);
	//h1_ratio->GetYaxis()->SetTitleOffset(1.85);
	//h1_ratio->GetYaxis()->SetTitleFont(43);
	//h1_ratio->GetYaxis()->SetLabelSize(0);
	//h1_ratio->GetYaxis()->SetTitleSize(28);
	//h1_ratio->GetXaxis()->SetLabelFont(43);
	//h1_ratio->GetXaxis()->SetTitleFont(43);
	//h1_ratio->GetXaxis()->SetLabelSize(28);
	h1_ratio->GetXaxis()->SetTitleSize(.12);
	//h1_ratio->GetXaxis()->SetRangeUser(xmin, xmax);
	//TGaxis *ratioaxis = new TGaxis(xmin, 0.5, xmin, 1.5, 0.5, 1.5, 503, "");
	//ratioaxis->SetLabelFont(43);
	//ratioaxis->SetLabelSize(28);

	h1_ratio->Draw("P");


	TLine *line=new TLine(h1_ratio->GetXaxis()->GetXmin(),1,h1_ratio->GetXaxis()->GetXmax(),1);
	line->SetLineStyle(7);
	line->Draw();
	//ratioaxis->Draw();
	//cout<<"before save"<<endl;


	std::string temp(histo->GetName());
	if (rebin) {
		temp=temp+"rebin";
	}
	temp=temp+"ratio";
	if (savePNG) {
		c1->Print((outPutDir+temp+".png").c_str(),"png");
	}
	if (savePDF) {
		c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");
	}

}
void saveTwoHistoRatioAsy(TH1* histoOrig,TH1* histoOrig2,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale=false,bool rebin=false);
void saveTwoHistoRatioAsy(TH1* histoOrig,TH1* histoOrig2,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	TH1D *histo2 = (TH1D*)histoOrig2->Clone();
	TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);
	TPad *pad1 = new TPad("pad1","pad1",0,0.35,1,1);
	pad1->SetBottomMargin(0.0);
	pad1->SetLeftMargin(0.185);
	pad1->Draw();
	c1->cd();
	TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.35);
	pad2->SetTopMargin(0.0);
	//pad2->SetBottomMargin(0.15);
	pad2->SetBottomMargin(0.3);
	pad2->SetLeftMargin(0.185);
	pad2->Draw();
	//pad2->SetGridy();

	pad1->cd();
	if (logScale) {
		c1->SetLogy();
	}
	TLatex latex;
	latex.SetNDC();
	latex.SetTextColor(1);
	latex.SetTextFont(42);
	latex.SetTextSize(.04);
	Double_t chi2=histo->Chi2Test(histo2,"CHI2");
	Double_t chi2ndf=histo->Chi2Test(histo2,"CHI2/NDF");
	Double_t ksTest=histo->KolmogorovTest(histo2);
	stuffToWrite.push_back("CHI2="+std::to_string(chi2));
	stuffToWrite.push_back("chi2ndf="+std::to_string(chi2ndf));
	stuffToWrite.push_back("ksTest="+std::to_string(ksTest));
	if (normalizedArea) {
		histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
		histo2->Scale(1/(histo2->Integral(1,histo2->GetNbinsX())));

	}
	if (rebin) {
		histo->Rebin();
		histo2->Rebin();
	}

	histo->Draw("hist");
	histo2->SetLineColor(2);
	histo2->Draw("hist same");
	legend->Draw();
	if (logScale) {
		histo->SetMinimum(.2);
		histo->SetMaximum(histo->GetMaximum()*1000);
	}
	histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
	histo->GetXaxis()->SetLabelSize(0);
	histo->GetXaxis()->SetNdivisions(505);
	histo->GetXaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
	histo->GetYaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitleOffset(1.75);
	//dataHisto->GetYaxis()->SetLabelSize(.04);
	histo->GetYaxis()->SetLabelSize(.04);


	ATLASLabel3(xStart,yStart,"Internal",.85,.04);
	Double_t x=xStart;Double_t y=yStart-.045;
	for (int j=0;j<stuffToWrite.size();j++) {
		latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
		y-=.045;
	}
	pad2->cd();

	TH1F *h1_ratio = (TH1F*)histo->Clone();
	TH1F *h1_den=(TH1F*)histo->Clone();
	h1_den->Add(histo2);
	//h1_ratio->Add(h1_ratio,stack1,1,-1);
	h1_ratio->Add(histo2,-1);
	h1_ratio->Divide(h1_den);
	//h1_ratio->SetMarkerColor(colourAdd);
	//h1_ratio->GetYaxis()->SetRangeUser(0, 2.);
	h1_ratio->GetYaxis()->SetRangeUser(-.5, .5);
	h1_ratio->GetYaxis()->SetTitle("(Pos-Neg)/(Pos+Neg) #eta");
	//h1_ratio->SetMaximum(.19);
	//h1_ratio->SetMinimum(-.19);
	h1_ratio->GetXaxis()->SetTitle(xaxisTitle.c_str());
	h1_ratio->GetXaxis()->SetTitleSize(.075);
	h1_ratio->GetXaxis()->SetLabelSize(.075);
	//h1_ratio->GetYaxis()->SetTitle(yaxisTitle.c_str());
	h1_ratio->GetYaxis()->SetTitleSize(.075);
	h1_ratio->GetYaxis()->SetTitleOffset(1.0);
	//dataHisto->GetYaxis()->SetLabelSize(.04);
	h1_ratio->GetYaxis()->SetLabelSize(.075);

	/*h1_ratio->GetYaxis()->SetTitleSize(.08);
	h1_ratio->GetYaxis()->SetTitleOffset(.8);
	h1_ratio->GetYaxis()->SetLabelSize(.08);
	h1_ratio->GetXaxis()->SetTitle(xaxisTitle.c_str());
	h1_ratio->GetXaxis()->SetLabelSize(.08);
	h1_ratio->GetXaxis()->SetTitleOffset(.8);*/
	h1_ratio->GetYaxis()->SetNdivisions(5);
	//h1_ratio->GetXaxis()->SetRangeUser(xmin, xmax);
	//TGaxis *ratioaxis = new TGaxis(xmin, 0.5, xmin, 1.5, 0.5, 1.5, 503, "");
	//ratioaxis->SetLabelFont(43);
	//ratioaxis->SetLabelSize(28);

	h1_ratio->Draw("P");


	TLine *line=new TLine(h1_ratio->GetXaxis()->GetXmin(),1,h1_ratio->GetXaxis()->GetXmax(),1);
	line->SetLineStyle(7);
	line->Draw();
	//ratioaxis->Draw();
	//cout<<"before save"<<endl;


	std::string temp(histo->GetName());
	if (rebin) {
		temp=temp+"rebin";
	}
	temp=temp+"Asy";
	if (savePNG) {
		c1->Print((outPutDir+temp+".png").c_str(),"png");
	}
	if (savePDF) {
		c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");
	}

}
Double_t getWidth(TH1D* histo) {
	int mean_bin = histo->FindBin(histo->GetMean());
	if (debug==true) {
		cout<< "mean_bin="<<mean_bin<<" mean="<<histo->GetMean()<<endl;
	}

    int binup = mean_bin;
    int bindown = mean_bin-1;
    Double_t total = histo->Integral();
    Double_t runningup = 0;
    Double_t runningdown = 0;
    if (total<50) {
		return -10.;
	}
    while (runningup < 0.6827/2.0*total) {
        runningup += histo->GetBinContent(binup);
        binup += 1;
	}
    while (runningdown < 0.6827/2.0*total) {
        runningdown += histo->GetBinContent(bindown);
        bindown -= 1;
	}

    Double_t width = (histo->GetBinCenter(binup)-histo->GetBinCenter(bindown))/2.;
    return width/1000.;
}
void SavePlots() {
	TEnv *config = new TEnv("SavePlots.config");
	if ( !config ) {
		printf("configFile=%s doesn't appear to exists\n",configFile.c_str());
	}
	std::string outPutDir = config->GetValue("outPutDir","blah");
	bool doData = config->GetValue("doData",true);
	bool doMC = config->GetValue("doMC",true);
	std::string AddedFileData = config->GetValue("AddedFileData","blah");
	std::string AddedFileMC = config->GetValue("AddedFileMC","blah");
	std::string ToyTestInputFile = config->GetValue("ToyTestInputFile","blah");
	std::string pileUpOutput = config->GetValue("pileUpOutput","blah");
	std::string etaBlocksString= config->GetValue("etaBlocks","0,.8");
	bool doSecondary = config->GetValue("doSecondary",false);
	std::string etaBlocksSecondaryString= config->GetValue("etaBlocksSecondary","0,2.1");
	int startEtaStep = config->GetValue("startEtaStep",0);
	int nEtaStepsToTake = config->GetValue("nEtaStepsToTake",1);
	int nEtaStepsToTakeSecondary = config->GetValue("nEtaStepsToTakeSecondary",1);
	bool doMuComparisonsFromMakePlots4 = config->GetValue("doMuComparisonsFromMakePlots4",false);
	debug = config->GetValue("debug",false);
	int maxNPV = config->GetValue("maxNPV",30);
	int maxMu = config->GetValue("maxMu",35);
	int minNPV = config->GetValue("minNPV",30);
	int minMu = config->GetValue("minMu",35);

	std::string temp;

	int nEtaSteps=-1;
	printf("etaBlocksString=%s\n",etaBlocksString.c_str());
	stringstream ss(etaBlocksString);
	vector<double> etaBlocksVector;
	while(getline(ss,temp,',')) {
		etaBlocksVector.push_back(std::stod(temp));
		nEtaSteps++;
	}
	Double_t etaBlocks[nEtaSteps+1];
	for (int i=0;i<etaBlocksVector.size();i++) {
		printf("etaBlocks[%i]=%f\n",i,etaBlocksVector[i]);
		etaBlocks[i]=etaBlocksVector[i];
	}
	int nEtaStepsSecondary=-1;
	printf("etaBlocksSecondaryString=%s\n",etaBlocksSecondaryString.c_str());
	stringstream ss2(etaBlocksSecondaryString);
	vector<double> etaBlocksSecondaryVector;
	while(getline(ss2,temp,',')) {
		etaBlocksSecondaryVector.push_back(std::stod(temp));
		nEtaStepsSecondary++;
	}
	Double_t etaBlocksSecondary[nEtaStepsSecondary+1];
	for (int i=0;i<etaBlocksSecondaryVector.size();i++) {
		printf("etaBlocksSecondary[%i]=%f\n",i,etaBlocksSecondaryVector[i]);
		etaBlocksSecondary[i]=etaBlocksSecondaryVector[i];
	}

	SetAtlasStyle();
	int histNameInt=0;
	TH3D *R4ConeEM_etaSlicesPos[nEtaSteps];
	TH3D *R4ConeLC_etaSlicesPos[nEtaSteps];

	TH3D *R4ConeEM_etaSlicesSecondary[nEtaStepsSecondary];
	TH3D *R4ConeLC_etaSlicesSecondary[nEtaStepsSecondary];

	TH3D *R4ConeEM_etaSlicesPosMC[nEtaSteps];
	TH3D *R4ConeLC_etaSlicesPosMC[nEtaSteps];

	TH3D *R4ConeEM_etaSlicesSecondaryMC[nEtaStepsSecondary];
	TH3D *R4ConeLC_etaSlicesSecondaryMC[nEtaStepsSecondary];

	TH1D* dataMuDist[nEtaSteps+1];
	TH1D* mcMuDist[nEtaSteps+1];

	TH1D* dataNPVDist[nEtaSteps+1];
	TH1D* mcNPVDist[nEtaSteps+1];

	TH1D *averageMuHisto;
	TH1D *correctedAverageMuHisto;
	TH1D *NPVHisto;
	TH1D *emptyConesSkippedHistoEM[nEtaSteps];
	TH1D *emptyConesSkippedHistoLC[nEtaSteps];

	TH1D *averageMuHistoMC;
	TH1D *correctedAverageMuHistoMC;
	TH1D *NPVHistoMC;
	TH1D *emptyConesSkippedHistoEMMC[nEtaSteps];
	TH1D *emptyConesSkippedHistoLCMC[nEtaSteps];

	TFile *infile;
	TH1::SetDefaultSumw2("kFalse");
	if (doData) {
		infile = TFile::Open(AddedFileData.c_str());
		cout<<AddedFileData<<endl;
		for (int x=0;x<nEtaSteps;x++) {
			cout<<x<<endl;
			//cout<<vectorOfRunNumbers[0]<<endl;
			R4ConeEM_etaSlicesPos[x]=(TH3D*)infile->Get(("R4ConeEM_etaSlicesPos"+std::to_string(x)+"_"+std::to_string(x+1)).c_str())->Clone();
			R4ConeEM_etaSlicesPos[x]->SetDirectory(0);
			emptyConesSkippedHistoEM[x]=(TH1D*)infile->Get(("emptyConesSkippedHistoEM"+std::to_string(x)+"_"+std::to_string(x+1)).c_str())->Clone();
			emptyConesSkippedHistoEM[x]->SetDirectory(0);

			R4ConeLC_etaSlicesPos[x]=(TH3D*)infile->Get(("R4ConeLC_etaSlicesPos"+std::to_string(x)+"_"+std::to_string(x+1)).c_str())->Clone();
			R4ConeLC_etaSlicesPos[x]->SetDirectory(0);
			emptyConesSkippedHistoLC[x]=(TH1D*)infile->Get(("emptyConesSkippedHistoLC"+std::to_string(x)+"_"+std::to_string(x+1)).c_str())->Clone();
			emptyConesSkippedHistoLC[x]->SetDirectory(0);




		}

		if (doSecondary==true) {
			for (int x=0;x<nEtaStepsSecondary;x++) {
				cout<<x<<endl;
				//cout<<vectorOfRunNumbers[0]<<endl;
				R4ConeEM_etaSlicesSecondary[x]=(TH3D*)infile->Get(("R4ConeEM_etaSlicesSecondary"+std::to_string(x)+"_"+std::to_string(x+1)).c_str())->Clone();
				R4ConeEM_etaSlicesSecondary[x]->SetDirectory(0);

				R4ConeLC_etaSlicesSecondary[x]=(TH3D*)infile->Get(("R4ConeLC_etaSlicesSecondary"+std::to_string(x)+"_"+std::to_string(x+1)).c_str())->Clone();
				R4ConeLC_etaSlicesSecondary[x]->SetDirectory(0);


			}
		}
		if (doMuComparisonsFromMakePlots4==true) {
			averageMuHisto=(TH1D*)infile->Get("averageMuHisto")->Clone();
			averageMuHisto->SetDirectory(0);
			correctedAverageMuHisto=(TH1D*)infile->Get("correctedAverageMuHisto")->Clone();
			correctedAverageMuHisto->SetDirectory(0);
			NPVHisto=(TH1D*)infile->Get("NPVHisto")->Clone();
			NPVHisto->SetDirectory(0);
		}
		infile->Close();
	}
	if (doMC) {
		infile = TFile::Open(AddedFileMC.c_str());
		cout<<AddedFileMC<<endl;
		for (int x=0;x<nEtaSteps;x++) {
			cout<<x<<endl;
			//cout<<vectorOfRunNumbers[0]<<endl;
			R4ConeEM_etaSlicesPosMC[x]=(TH3D*)infile->Get(("R4ConeEM_etaSlicesPos"+std::to_string(x)+"_"+std::to_string(x+1)).c_str())->Clone();
			R4ConeEM_etaSlicesPosMC[x]->SetDirectory(0);
			emptyConesSkippedHistoEMMC[x]=(TH1D*)infile->Get(("emptyConesSkippedHistoEM"+std::to_string(x)+"_"+std::to_string(x+1)).c_str())->Clone();
			emptyConesSkippedHistoEMMC[x]->SetDirectory(0);


			R4ConeLC_etaSlicesPosMC[x]=(TH3D*)infile->Get(("R4ConeLC_etaSlicesPos"+std::to_string(x)+"_"+std::to_string(x+1)).c_str())->Clone();
			R4ConeLC_etaSlicesPosMC[x]->SetDirectory(0);
			emptyConesSkippedHistoLCMC[x]=(TH1D*)infile->Get(("emptyConesSkippedHistoLC"+std::to_string(x)+"_"+std::to_string(x+1)).c_str())->Clone();
			emptyConesSkippedHistoLCMC[x]->SetDirectory(0);

		}

		if (doSecondary==true) {
			for (int x=0;x<nEtaStepsSecondary;x++) {
				cout<<x<<endl;
				//cout<<vectorOfRunNumbers[0]<<endl;
				R4ConeEM_etaSlicesSecondaryMC[x]=(TH3D*)infile->Get(("R4ConeEM_etaSlicesSecondary"+std::to_string(x)+"_"+std::to_string(x+1)).c_str())->Clone();
				R4ConeEM_etaSlicesSecondaryMC[x]->SetDirectory(0);

				R4ConeLC_etaSlicesSecondaryMC[x]=(TH3D*)infile->Get(("R4ConeLC_etaSlicesSecondary"+std::to_string(x)+"_"+std::to_string(x+1)).c_str())->Clone();
				R4ConeLC_etaSlicesSecondaryMC[x]->SetDirectory(0);


			}
		}
		if (doMuComparisonsFromMakePlots4==true) {
			averageMuHistoMC=(TH1D*)infile->Get("averageMuHisto")->Clone();
			averageMuHistoMC->SetDirectory(0);
			correctedAverageMuHistoMC=(TH1D*)infile->Get("correctedAverageMuHisto")->Clone();
			correctedAverageMuHistoMC->SetDirectory(0);
			NPVHistoMC=(TH1D*)infile->Get("NPVHisto")->Clone();
			NPVHistoMC->SetDirectory(0);
		}
		infile->Close();
	}

	cout<<"heyo4"<<endl;
	TH1D *histo;
	TH1D *histo2;

	vector<std::string> stuffToWrite;
	stuffToWrite.push_back("R=0.4, EM Scale");
	stuffToWrite.push_back("Random Cone Jets");
	vector<std::string> stuffToWriteb;
	stuffToWriteb.push_back("R=0.4, EM Scale");
	stuffToWriteb.push_back("Random Cone Jets");
	char buff1[15];
	char buff2[15];
	char sbuff1[15];
	char sbuff2[15];
	sprintf(buff1,"%.1f",etaBlocks[startEtaStep]);
	sprintf(buff2,"%.1f",etaBlocks[startEtaStep+nEtaStepsToTake]);
	sprintf(sbuff1,"%.1f",etaBlocksSecondary[startEtaStep]);
	sprintf(sbuff2,"%.1f",etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary]);
	stuffToWrite.push_back(string(buff1)+"<|#eta|<"+string(buff2));
	stuffToWriteb.push_back(string(sbuff1)+"<|#eta|<"+string(sbuff2));

	legend2 = new TLegend(.60,.825,1.,.92);
	legend2->SetFillColor(0);
	legend2->SetFillStyle(0);//make it transparent
	legend2->SetBorderSize(0);
	legend2->SetTextSize(0.04);
	legend2->SetTextFont(42);
	legend2->SetMargin(.15);

	legend4 = new TLegend(.60,.73,1.,.92);
	legend4->SetFillColor(0);
	legend4->SetFillStyle(0);//make it transparent
	legend4->SetBorderSize(0);
	legend4->SetTextSize(0.04);
	legend4->SetTextFont(42);
	legend4->SetMargin(.15);
	//TH1::SetDefaultSumw2(false);
	if (doData&&!doMC) {
		histo = (TH1D*)R4ConeEM_etaSlicesPos[0]->ProjectionX("R4ConeEM_etaSlicesPos_0_1");
		saveSingleHisto(histo,"A [MeV]","Number of Events",stuffToWrite,outPutDir,false,0.20,0.88,0,0);
	} else if (doMC&&!doData) {
		histo = (TH1D*)R4ConeEM_etaSlicesPosMC[0]->ProjectionX("R4ConeEM_etaSlicesPosMC_0_1");
		histo->Rebin(20);
		saveSingleHisto(histo,"A [MeV]","Number of Events",stuffToWrite,outPutDir,false,0.20,0.88,0,0);

	} else if (doMC&&doData) {
		histo = (TH1D*)R4ConeEM_etaSlicesPos[0]->ProjectionX("R4ConeEM_etaSlicesPos_0_1");
		histo2 = (TH1D*)R4ConeEM_etaSlicesPosMC[0]->ProjectionX("R4ConeEM_etaSlicesPosMC_0_1");
		histo2->SetLineColor(2);
		legend2->AddEntry(histo,"R=0.4,EM Scale Data","L");
		legend2->AddEntry(histo2,"R=0.4,EM Scale MC","L");
		saveTwoHisto(histo,histo2,legend2,"A [MeV]","Number of Events",stuffToWrite,outPutDir,false,0.20,0.88,0,0);
		legend2->Clear();
	}
	if (doSecondary) {
		if (doData&&!doMC) {
			histo = (TH1D*)R4ConeEM_etaSlicesSecondary[0]->ProjectionX("R4ConeEM_etaSlicesSecondary_0_1");
			saveSingleHisto(histo,"A [MeV]","Number of Events",stuffToWriteb,outPutDir,false,0.20,0.88,0,0);
		} else if (doMC&&!doData) {
			histo = (TH1D*)R4ConeEM_etaSlicesSecondaryMC[0]->ProjectionX("R4ConeEM_etaSlicesSecondaryMC_0_1");
			histo->Rebin(20);
			saveSingleHisto(histo,"A [MeV]","Number of Events",stuffToWriteb,outPutDir,false,0.20,0.88,0,0);
		} else if (doMC&&doData) {
			histo = (TH1D*)R4ConeEM_etaSlicesSecondary[0]->ProjectionX("R4ConeEM_etaSlicesSecondary_0_1");
			histo2 = (TH1D*)R4ConeEM_etaSlicesSecondaryMC[0]->ProjectionX("R4ConeEM_etaSlicesSecondaryMC_0_1");
			histo2->SetLineColor(2);
			legend2->AddEntry(histo,"R=0.4,EM Scale Data","L");
			legend2->AddEntry(histo2,"R=0.4,EM Scale MC","L");
			saveTwoHisto(histo,histo2,legend2,"A [MeV]","Number of Events",stuffToWriteb,outPutDir,false,0.20,0.88,0,0);
			legend2->Clear();
		}
	}
cout<<"heyo4b"<<endl;
	if (doData&&!doMC) {
		histo = (TH1D*)R4ConeEM_etaSlicesPos[0]->ProjectionX("R4ConeEM_etaSlicesPos_0_1");
		saveSingleHisto(histo,"A [MeV]","Normalized Entries",stuffToWrite,outPutDir,true,0.20,0.88,0,0);
	} else if (doMC&&!doData) {
		histo = (TH1D*)R4ConeEM_etaSlicesPosMC[0]->ProjectionX("R4ConeEM_etaSlicesPosMC_0_1");
		histo->Rebin(20);
		saveSingleHisto(histo,"A [MeV]","Normalized Entries",stuffToWrite,outPutDir,true,0.20,0.88,0,0);
	} else if (doMC&&doData) {
		cout<<"heyo4c"<<endl;
		histo = (TH1D*)R4ConeEM_etaSlicesPos[0]->ProjectionX("R4ConeEM_etaSlicesPos_0_1");
		cout<<"heyo4d"<<endl;
		histo2 = (TH1D*)R4ConeEM_etaSlicesPosMC[0]->ProjectionX("R4ConeEM_etaSlicesPosMC_0_1");
		cout<<"heyo4e"<<endl;
		histo2->SetLineColor(2);
		legend2->AddEntry(histo,"R=0.4,EM Scale Data","L");
		legend2->AddEntry(histo2,"R=0.4,EM Scale MC","L");
		saveTwoHisto(histo,histo2,legend2,"A [MeV]","Normalized Entries",stuffToWrite,outPutDir,true,0.20,0.88,0,0);
		cout<<"heyo4f"<<endl;
		legend2->Clear();
	}
	cout<<"heyo4g"<<endl;
	if (doSecondary) {
		if (doData&&!doMC) {
			histo = (TH1D*)R4ConeEM_etaSlicesSecondary[0]->ProjectionX("R4ConeEM_etaSlicesSecondary_0_1");
			saveSingleHisto(histo,"A [MeV]","Normalized Entries",stuffToWriteb,outPutDir,true,0.20,0.88,0,0);
		} else if (doMC&&!doData) {
			histo = (TH1D*)R4ConeEM_etaSlicesSecondaryMC[0]->ProjectionX("R4ConeEM_etaSlicesSecondaryMC_0_1");
			histo->Rebin(20);
			saveSingleHisto(histo,"A [MeV]","Normalized Entries",stuffToWriteb,outPutDir,true,0.20,0.88,0,0);
		} else if (doMC&&doData) {
			histo = (TH1D*)R4ConeEM_etaSlicesSecondary[0]->ProjectionX("R4ConeEM_etaSlicesSecondary_0_1");
			histo2 = (TH1D*)R4ConeEM_etaSlicesSecondaryMC[0]->ProjectionX("R4ConeEM_etaSlicesSecondaryMC_0_1");
			histo2->SetLineColor(2);
			legend2->AddEntry(histo,"R=0.4,EM Scale Data","L");
			legend2->AddEntry(histo2,"R=0.4,EM Scale MC","L");
			saveTwoHisto(histo,histo2,legend2,"A [MeV]","Normalized Entries",stuffToWriteb,outPutDir,true,0.20,0.88,0,0);
			legend2->Clear();
		}
	}


	vector<std::string> stuffToWrite2;
	stuffToWrite2.push_back("R=0.4, LC Scale");
	stuffToWrite2.push_back("Random Cone Jets");
	vector<std::string> stuffToWrite2b;
	stuffToWrite2b.push_back("R=0.4, LC Scale");
	stuffToWrite2b.push_back("Random Cone Jets");
	stuffToWrite2.push_back(string(buff1)+"<|#eta|<"+string(buff2));
	stuffToWrite2b.push_back(string(sbuff1)+"<|#eta|<"+string(sbuff2));
	if (doData&&!doMC) {
		histo = (TH1D*)R4ConeLC_etaSlicesPos[0]->ProjectionX("R4ConeLC_etaSlicesPos_0_1");
		saveSingleHisto(histo,"A [MeV]","Normalized Entries",stuffToWrite2,outPutDir,true,0.20,0.88,0,0);
	} else if (doMC&&!doData) {
		histo = (TH1D*)R4ConeLC_etaSlicesPosMC[0]->ProjectionX("R4ConeLC_etaSlicesPosMC_0_1");
		histo->Rebin(20);
		saveSingleHisto(histo,"A [MeV]","Normalized Entries",stuffToWrite2,outPutDir,true,0.20,0.88,0,0);
	} else if (doMC&&doData) {
		histo = (TH1D*)R4ConeLC_etaSlicesPos[0]->ProjectionX("R4ConeLC_etaSlicesPos_0_1");
		histo2 = (TH1D*)R4ConeLC_etaSlicesPosMC[0]->ProjectionX("R4ConeLC_etaSlicesPosMC_0_1");
		histo2->SetLineColor(2);
		legend2->AddEntry(histo,"R=0.4,LC Scale Data","L");
		legend2->AddEntry(histo2,"R=0.4,LC Scale MC","L");
		saveTwoHisto(histo,histo2,legend2,"A [MeV]","Normalized Entries",stuffToWrite2,outPutDir,true,0.20,0.88,0,0);
		legend2->Clear();
	}
	if (doSecondary) {
		if (doData&&!doMC) {
			histo = (TH1D*)R4ConeLC_etaSlicesSecondary[0]->ProjectionX("R4ConeLC_etaSlicesSecondary_0_1");
			saveSingleHisto(histo,"A [MeV]","Normalized Entries",stuffToWrite2b,outPutDir,true,0.20,0.88,0,0);
		} else if (doMC&&!doData) {
			histo = (TH1D*)R4ConeLC_etaSlicesSecondaryMC[0]->ProjectionX("R4ConeLC_etaSlicesSecondaryMC_0_1");
			histo->Rebin(20);
			saveSingleHisto(histo,"A [MeV]","Normalized Entries",stuffToWrite2b,outPutDir,true,0.20,0.88,0,0);
		} else if (doMC&&doData) {
			histo = (TH1D*)R4ConeLC_etaSlicesSecondary[0]->ProjectionX("R4ConeLC_etaSlicesSecondary_0_1");
			histo2 = (TH1D*)R4ConeLC_etaSlicesSecondaryMC[0]->ProjectionX("R4ConeLC_etaSlicesSecondaryMC_0_1");
			histo2->SetLineColor(2);
			legend2->AddEntry(histo,"R=0.4,LC Scale Data","L");
			legend2->AddEntry(histo2,"R=0.4,LC Scale MC","L");
			saveTwoHisto(histo,histo2,legend2,"A [MeV]","Normalized Entries",stuffToWrite2b,outPutDir,true,0.20,0.88,0,0);
			legend2->Clear();
		}
	}
	/*
	legend2 = new TLegend(.60,.825,1.,.92);
	legend2->SetFillColor(0);
	legend2->SetFillStyle(0);//make it transparent
	legend2->SetBorderSize(0);
	legend2->SetTextSize(0.04);
	legend2->SetTextFont(42);
	//legend->SetTextAlign(13);
	legend2->SetMargin(.15);*/
	//int nEtaStepsInEtaBin=3;
	//int nEtaBins=nEtaSteps/nEtaStepsInEtaBin;
	Double_t tempWidth=0;
	TH1D* NoiseAsFcnEta4EM=new TH1D("NoiseAsFcnEta4EM","Noise as a function of eta for R=.4 EM",nEtaSteps,etaBlocks);
	TH1D* NoiseAsFcnEta4LC=new TH1D("NoiseAsFcnEta4LC","Noise as a function of eta for R=.4 LC",nEtaSteps,etaBlocks);

	TH1D* NoiseAsFcnEta4EMSecondary=new TH1D("NoiseAsFcnEta4EMSecondary","Noise as a function of eta for R=.4 EM",nEtaStepsSecondary,etaBlocksSecondary);
	TH1D* NoiseAsFcnEta4LCSecondary=new TH1D("NoiseAsFcnEta4LCSecondary","Noise as a function of eta for R=.4 LC",nEtaStepsSecondary,etaBlocksSecondary);

	TH1D* MCNoiseAsFcnEta4EM=new TH1D("MCNoiseAsFcnEta4EM","Noise as a function of eta for R=.4 EM",nEtaSteps,etaBlocks);
	TH1D* MCNoiseAsFcnEta4LC=new TH1D("MCNoiseAsFcnEta4LC","Noise as a function of eta for R=.4 LC",nEtaSteps,etaBlocks);

	TH1D* MCNoiseAsFcnEta4EMSecondary=new TH1D("MCNoiseAsFcnEta4EMSecondary","Noise as a function of eta for R=.4 EM",nEtaStepsSecondary,etaBlocksSecondary);
	TH1D* MCNoiseAsFcnEta4LCSecondary=new TH1D("MCNoiseAsFcnEta4LCSecondary","Noise as a function of eta for R=.4 LC",nEtaStepsSecondary,etaBlocksSecondary);

	TFile *outputFile=new TFile((ToyTestInputFile).c_str(),"RECREATE");
	cout<<"heyo5"<<endl;
	if (doData) {
		for (int x=0;x<nEtaSteps;x++) {
			histo = (TH1D*)R4ConeEM_etaSlicesPos[x]->ProjectionX(("R4ConeEM_etaSlicesPos"+std::to_string(x)+"_"+std::to_string((x+1))).c_str());
			tempWidth=getWidth(histo);
			if (tempWidth>=0){
				NoiseAsFcnEta4EM->Fill(etaBlocks[x]+.01,tempWidth/TMath::Sqrt(2));
			} else {
			}
			histo->Write();

			histo = (TH1D*)R4ConeLC_etaSlicesPos[x]->ProjectionX(("R4ConeLC_etaSlicesPos"+std::to_string(x)+"_"+std::to_string((x+1))).c_str());

			tempWidth=getWidth(histo);
			if (tempWidth>=0){
				NoiseAsFcnEta4LC->Fill(etaBlocks[x]+.01,tempWidth/TMath::Sqrt(2));
			}
			histo->Write();
		}
		NoiseAsFcnEta4EM->Write();
		NoiseAsFcnEta4LC->Write();
		if (doSecondary) {
			for (int x=0;x<nEtaStepsSecondary;x++) {
				histo = (TH1D*)R4ConeEM_etaSlicesSecondary[x]->ProjectionX(("R4ConeEM_etaSlicesSecondary"+std::to_string(x)+"_"+std::to_string((x+1))).c_str());
				tempWidth=getWidth(histo);
				if (tempWidth>=0){
					NoiseAsFcnEta4EMSecondary->Fill(etaBlocksSecondary[x]+.01,tempWidth/TMath::Sqrt(2));
				} else {
				}
				histo->Write();

				histo = (TH1D*)R4ConeLC_etaSlicesSecondary[x]->ProjectionX(("R4ConeLC_etaSlicesSecondary"+std::to_string(x)+"_"+std::to_string((x+1))).c_str());

				tempWidth=getWidth(histo);
				if (tempWidth>=0){
					NoiseAsFcnEta4LCSecondary->Fill(etaBlocksSecondary[x]+.01,tempWidth/TMath::Sqrt(2));
				}
				histo->Write();
			}
			NoiseAsFcnEta4EMSecondary->Write();
			NoiseAsFcnEta4LCSecondary->Write();
		}
	}
	if (doMC) {
		for (int x=0;x<nEtaSteps;x++) {
			histo = (TH1D*)R4ConeEM_etaSlicesPosMC[x]->ProjectionX(("R4ConeEM_etaSlicesPosMC"+std::to_string(x)+"_"+std::to_string((x+1))).c_str());
			tempWidth=getWidth(histo);
			if (tempWidth>=0){
				MCNoiseAsFcnEta4EM->Fill(etaBlocks[x]+.01,tempWidth/TMath::Sqrt(2));
			} else {
			}
			histo->Write();

			histo = (TH1D*)R4ConeLC_etaSlicesPosMC[x]->ProjectionX(("R4ConeLC_etaSlicesPosMC"+std::to_string(x)+"_"+std::to_string((x+1))).c_str());

			tempWidth=getWidth(histo);
			if (tempWidth>=0){
				MCNoiseAsFcnEta4LC->Fill(etaBlocks[x]+.01,tempWidth/TMath::Sqrt(2));
			}
			histo->Write();
		}
		MCNoiseAsFcnEta4EM->Write();
		MCNoiseAsFcnEta4LC->Write();
		if (doSecondary) {
			for (int x=0;x<nEtaStepsSecondary;x++) {
				histo = (TH1D*)R4ConeEM_etaSlicesSecondaryMC[x]->ProjectionX(("R4ConeEM_etaSlicesSecondaryMC"+std::to_string(x)+"_"+std::to_string((x+1))).c_str());
				tempWidth=getWidth(histo);
				if (tempWidth>=0){
					MCNoiseAsFcnEta4EMSecondary->Fill(etaBlocksSecondary[x]+.01,tempWidth/TMath::Sqrt(2));
				} else {
				}
				histo->Write();

				histo = (TH1D*)R4ConeLC_etaSlicesSecondaryMC[x]->ProjectionX(("R4ConeLC_etaSlicesSecondaryMC"+std::to_string(x)+"_"+std::to_string((x+1))).c_str());

				tempWidth=getWidth(histo);
				if (tempWidth>=0){
					MCNoiseAsFcnEta4LCSecondary->Fill(etaBlocksSecondary[x]+.01,tempWidth/TMath::Sqrt(2));
				}
				histo->Write();
			}
			MCNoiseAsFcnEta4EMSecondary->Write();
			MCNoiseAsFcnEta4LCSecondary->Write();
		}
	}
	cout<<"heyo6"<<endl;
	NoiseAsFcnEta4EM->SetMarkerStyle(20);
	NoiseAsFcnEta4LC->SetMarkerStyle(21);
	NoiseAsFcnEta4LC->SetMarkerColor(4);

	MCNoiseAsFcnEta4EM->SetMarkerStyle(24);
	MCNoiseAsFcnEta4LC->SetMarkerStyle(25);
	MCNoiseAsFcnEta4LC->SetMarkerColor(4);
	cout<<"heyo6b"<<endl;
	vector<std::string> stuffToWrite4;
	stuffToWrite4.push_back("Random Cone");
	stuffToWrite4.push_back("Noise Term");
	vector<std::string> stuffToWrite4b;
	stuffToWrite4b.push_back("Random Cone");
	stuffToWrite4b.push_back("Noise Term");
	if (doData&&!doMC) {
		legend2->AddEntry(NoiseAsFcnEta4EM,"R=0.4,EM Scale Data","P");
		legend2->AddEntry(NoiseAsFcnEta4LC,"R=0.4,LC Scale Data","P");
		saveTwoHistoMarker(NoiseAsFcnEta4EM,NoiseAsFcnEta4LC,legend2,"|#eta|","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0);
		legend2->Clear();
	} else if (!doData&&doMC) {
		legend2->AddEntry(MCNoiseAsFcnEta4EM,"R=0.4,EM Scale MC","P");
		legend2->AddEntry(MCNoiseAsFcnEta4LC,"R=0.4,LC Scale MC","P");
		saveTwoHistoMarker(MCNoiseAsFcnEta4EM,MCNoiseAsFcnEta4LC,legend2,"|#eta|","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0);
		legend2->Clear();
	} else if (doData&&doMC) {
		legend4->AddEntry(NoiseAsFcnEta4EM,"R=0.4,EM Scale Data","P");
		legend4->AddEntry(NoiseAsFcnEta4LC,"R=0.4,LC Scale Data","P");
		legend4->AddEntry(MCNoiseAsFcnEta4EM,"R=0.4,EM Scale MC","P");
		legend4->AddEntry(MCNoiseAsFcnEta4LC,"R=0.4,LC Scale MC","P");
		saveFourHistoMarker(NoiseAsFcnEta4EM,NoiseAsFcnEta4LC,MCNoiseAsFcnEta4EM,MCNoiseAsFcnEta4LC,legend4,"|#eta|","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0);
		legend4->Clear();
	}
	cout<<"heyo6c"<<endl;
	if (doSecondary) {
		NoiseAsFcnEta4EMSecondary->SetMarkerStyle(20);
		NoiseAsFcnEta4LCSecondary->SetMarkerStyle(21);
		NoiseAsFcnEta4LCSecondary->SetMarkerColor(4);

		MCNoiseAsFcnEta4EMSecondary->SetMarkerStyle(24);
		MCNoiseAsFcnEta4LCSecondary->SetMarkerStyle(25);
		MCNoiseAsFcnEta4LCSecondary->SetMarkerColor(4);
		if (doData&&!doMC) {
			legend2->AddEntry(NoiseAsFcnEta4EMSecondary,"R=0.4,EM Scale Data","P");
			legend2->AddEntry(NoiseAsFcnEta4LCSecondary,"R=0.4,LC Scale Data","P");
			saveTwoHistoMarker(NoiseAsFcnEta4EMSecondary,NoiseAsFcnEta4LCSecondary,legend2,"|#eta|","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0);
			legend2->Clear();
		} else if (!doData&&doMC) {
			legend2->AddEntry(MCNoiseAsFcnEta4EMSecondary,"R=0.4,EM Scale MC","P");
			legend2->AddEntry(MCNoiseAsFcnEta4LCSecondary,"R=0.4,LC Scale MC","P");
			saveTwoHistoMarker(MCNoiseAsFcnEta4EMSecondary,MCNoiseAsFcnEta4LCSecondary,legend2,"|#eta|","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0);
			legend2->Clear();
		} else if (doData&&doMC) {
			legend4->AddEntry(NoiseAsFcnEta4EMSecondary,"R=0.4,EM Scale Data","P");
			legend4->AddEntry(NoiseAsFcnEta4LCSecondary,"R=0.4,LC Scale Data","P");
			legend4->AddEntry(MCNoiseAsFcnEta4EMSecondary,"R=0.4,EM Scale MC","P");
			legend4->AddEntry(MCNoiseAsFcnEta4LCSecondary,"R=0.4,LC Scale MC","P");
			saveFourHistoMarker(NoiseAsFcnEta4EMSecondary,NoiseAsFcnEta4LCSecondary,MCNoiseAsFcnEta4EMSecondary,MCNoiseAsFcnEta4LCSecondary,legend4,"|#eta|","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0);
			legend4->Clear();
		}
	}

	cout <<"heyo7"<<endl;
	TH1D *NoiseAsFcnNPV4EM=new TH1D("NoiseAsFcnNPV4EM","Noise as a function of NPV for R=.4 EM ",maxNPV-minNPV,minNPV,maxNPV);
	TH1D *NoiseAsFcnNPV4LC=new TH1D("NoiseAsFcnNPV4LC","Noise as a function of NPV for R=.4 LC ",maxNPV-minNPV,minNPV,maxNPV);

	TH1D *NoiseAsFcnNPV4EMSecondary=new TH1D("NoiseAsFcnNPV4EMSecondary","Noise as a function of NPV for R=.4 EM ",maxNPV-minNPV,minNPV,maxNPV);
	TH1D *NoiseAsFcnNPV4LCSecondary=new TH1D("NoiseAsFcnNPV4LCSecondary","Noise as a function of NPV for R=.4 LC ",maxNPV-minNPV,minNPV,maxNPV);

	TH1D *MCNoiseAsFcnNPV4EM=new TH1D("MCNoiseAsFcnNPV4EM","Noise as a function of NPV for R=.4 EM ",maxNPV-minNPV,minNPV,maxNPV);
	TH1D *MCNoiseAsFcnNPV4LC=new TH1D("MCNoiseAsFcnNPV4LC","Noise as a function of NPV for R=.4 LC ",maxNPV-minNPV,minNPV,maxNPV);

	TH1D *MCNoiseAsFcnNPV4EMSecondary=new TH1D("MCNoiseAsFcnNPV4EMSecondary","Noise as a function of NPV for R=.4 EM ",maxNPV-minNPV,minNPV,maxNPV);
	TH1D *MCNoiseAsFcnNPV4LCSecondary=new TH1D("MCNoiseAsFcnNPV4LCSecondary","Noise as a function of NPV for R=.4 LC ",maxNPV-minNPV,minNPV,maxNPV);
	if (doData) {
		for (int y=minNPV;y<maxNPV;y++) {
			histo = (TH1D*)R4ConeEM_etaSlicesPos[0]->ProjectionX("temp",y-minNPV+1,y-minNPV+1)->Clone(("R4ConeEM_etaSlicesPos"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_NPV"+std::to_string(y)).c_str());
			for (int x=startEtaStep;x<startEtaStep+nEtaStepsToTake;x++) {
				histo->Add((TH1D*)R4ConeEM_etaSlicesPos[x]->ProjectionX("temp",y-minNPV+1,y-minNPV+1)->Clone(("hist"+std::to_string(histNameInt)).c_str()));
				histNameInt++;

			}
			tempWidth=getWidth(histo);
			//cout <<"y="<<y<<endl;
			//cout <<"histoIntegral="<<histo->Integral()<<endl;
			if (tempWidth>=0){
				NoiseAsFcnNPV4EM->Fill(y,tempWidth/TMath::Sqrt(2));
			}
			histo->Write();
			histo = (TH1D*)R4ConeLC_etaSlicesPos[startEtaStep]->ProjectionX("temp",y-minNPV+1,y-minNPV+1)->Clone(("R4ConeLC_etaSlicesPos"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_NPV"+std::to_string(y)).c_str());
			for (int x=startEtaStep;x<startEtaStep+nEtaStepsToTake;x++) {
				histo->Add((TH1D*)R4ConeLC_etaSlicesPos[x]->ProjectionX("temp",y-minNPV+1,y-minNPV+1)->Clone(("hist"+std::to_string(histNameInt)).c_str()));
				histNameInt++;
			}
			tempWidth=getWidth(histo);
			if (tempWidth>=0){
				NoiseAsFcnNPV4LC->Fill(y,tempWidth/TMath::Sqrt(2));
			}
			histo->Write();

		}
		NoiseAsFcnNPV4EM->Write();
		NoiseAsFcnNPV4LC->Write();
		if (doSecondary) {
			for (int y=minNPV;y<maxNPV;y++) {
				histo = (TH1D*)R4ConeEM_etaSlicesSecondary[0]->ProjectionX("temp",y-minNPV+1,y-minNPV+1)->Clone(("R4ConeEM_etaSlicesSecondary"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_NPV"+std::to_string(y)).c_str());
				for (int x=startEtaStep;x<startEtaStep+nEtaStepsToTakeSecondary;x++) {
					histo->Add((TH1D*)R4ConeEM_etaSlicesSecondary[x]->ProjectionX("temp",y-minNPV+1,y-minNPV+1)->Clone(("hist"+std::to_string(histNameInt)).c_str()));
					histNameInt++;

				}
				tempWidth=getWidth(histo);
				//cout <<"y="<<y<<endl;
				//cout <<"histoIntegral="<<histo->Integral()<<endl;
				if (tempWidth>=0){
					NoiseAsFcnNPV4EMSecondary->Fill(y,tempWidth/TMath::Sqrt(2));
				}
				histo->Write();
				histo = (TH1D*)R4ConeLC_etaSlicesSecondary[startEtaStep]->ProjectionX("temp",y-minNPV+1,y-minNPV+1)->Clone(("R4ConeLC_etaSlicesSecondary"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_NPV"+std::to_string(y)).c_str());
				for (int x=startEtaStep;x<startEtaStep+nEtaStepsToTakeSecondary;x++) {
					histo->Add((TH1D*)R4ConeLC_etaSlicesSecondary[x]->ProjectionX("temp",y-minNPV+1,y-minNPV+1)->Clone(("hist"+std::to_string(histNameInt)).c_str()));
					histNameInt++;
				}
				tempWidth=getWidth(histo);
				if (tempWidth>=0){
					NoiseAsFcnNPV4LCSecondary->Fill(y,tempWidth/TMath::Sqrt(2));
				}
				histo->Write();

			}
			NoiseAsFcnNPV4EMSecondary->Write();
			NoiseAsFcnNPV4LCSecondary->Write();
		}
	}
	if (doMC) {
		for (int y=minNPV;y<maxNPV;y++) {
			histo = (TH1D*)R4ConeEM_etaSlicesPosMC[0]->ProjectionX("temp",y-minNPV+1,y-minNPV+1)->Clone(("R4ConeEM_etaSlicesPosMC"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_NPV"+std::to_string(y)).c_str());
			for (int x=startEtaStep;x<startEtaStep+nEtaStepsToTake;x++) {
				histo->Add((TH1D*)R4ConeEM_etaSlicesPosMC[x]->ProjectionX("temp",y-minNPV+1,y-minNPV+1)->Clone(("hist"+std::to_string(histNameInt)).c_str()));
				histNameInt++;

			}
			tempWidth=getWidth(histo);
			//cout <<"y="<<y<<endl;
			//cout <<"histoIntegral="<<histo->Integral()<<endl;
			if (tempWidth>=0){
				MCNoiseAsFcnNPV4EM->Fill(y,tempWidth/TMath::Sqrt(2));
			}
			histo->Write();
			histo = (TH1D*)R4ConeLC_etaSlicesPosMC[startEtaStep]->ProjectionX("temp",y-minNPV+1,y-minNPV+1)->Clone(("R4ConeLC_etaSlicesPosMC"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_NPV"+std::to_string(y)).c_str());
			for (int x=startEtaStep;x<startEtaStep+nEtaStepsToTake;x++) {
				histo->Add((TH1D*)R4ConeLC_etaSlicesPosMC[x]->ProjectionX("temp",y-minNPV+1,y-minNPV+1)->Clone(("hist"+std::to_string(histNameInt)).c_str()));
				histNameInt++;
			}
			tempWidth=getWidth(histo);
			if (tempWidth>=0){
				MCNoiseAsFcnNPV4LC->Fill(y,tempWidth/TMath::Sqrt(2));
			}
			histo->Write();

		}
		MCNoiseAsFcnNPV4EM->Write();
		MCNoiseAsFcnNPV4LC->Write();
		if (doSecondary) {
			for (int y=minNPV;y<maxNPV;y++) {
				histo = (TH1D*)R4ConeEM_etaSlicesSecondaryMC[0]->ProjectionX("temp",y-minNPV+1,y-minNPV+1)->Clone(("R4ConeEM_etaSlicesSecondaryMC"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_NPV"+std::to_string(y)).c_str());
				for (int x=startEtaStep;x<startEtaStep+nEtaStepsToTakeSecondary;x++) {
					histo->Add((TH1D*)R4ConeEM_etaSlicesSecondaryMC[x]->ProjectionX("temp",y-minNPV+1,y-minNPV+1)->Clone(("hist"+std::to_string(histNameInt)).c_str()));
					histNameInt++;

				}
				tempWidth=getWidth(histo);
				//cout <<"y="<<y<<endl;
				//cout <<"histoIntegral="<<histo->Integral()<<endl;
				if (tempWidth>=0){
					MCNoiseAsFcnNPV4EMSecondary->Fill(y,tempWidth/TMath::Sqrt(2));
				}
				histo->Write();
				histo = (TH1D*)R4ConeLC_etaSlicesSecondaryMC[startEtaStep]->ProjectionX("temp",y-minNPV+1,y-minNPV+1)->Clone(("R4ConeLC_etaSlicesSecondaryMC"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_NPV"+std::to_string(y)).c_str());
				for (int x=startEtaStep;x<startEtaStep+nEtaStepsToTakeSecondary;x++) {
					histo->Add((TH1D*)R4ConeLC_etaSlicesSecondaryMC[x]->ProjectionX("temp",y-minNPV+1,y-minNPV+1)->Clone(("hist"+std::to_string(histNameInt)).c_str()));
					histNameInt++;
				}
				tempWidth=getWidth(histo);
				if (tempWidth>=0){
					MCNoiseAsFcnNPV4LCSecondary->Fill(y,tempWidth/TMath::Sqrt(2));
				}
				histo->Write();

			}
			MCNoiseAsFcnNPV4EMSecondary->Write();
			MCNoiseAsFcnNPV4LCSecondary->Write();
		}
	}
	NoiseAsFcnNPV4EM->SetMarkerStyle(20);
	NoiseAsFcnNPV4LC->SetMarkerStyle(21);
	NoiseAsFcnNPV4LC->SetMarkerColor(4);

	MCNoiseAsFcnNPV4EM->SetMarkerStyle(24);
	MCNoiseAsFcnNPV4LC->SetMarkerStyle(25);
	MCNoiseAsFcnNPV4LC->SetMarkerColor(4);
	/*char buff1[15];
	char buff2[15];
	char sbuff1[15];
	char sbuff2[15];
	sprintf(buff1,"%.1f",etaBlocks[startEtaStep]);
	sprintf(buff2,"%.1f",etaBlocks[startEtaStep+nEtaStepsToTake]);
	sprintf(sbuff1,"%.1f",etaBlocksSecondary[startEtaStep]);
	sprintf(sbuff2,"%.1f",etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary]);*/
	stuffToWrite4.push_back(string(buff1)+"<|#eta|<"+string(buff2));
	stuffToWrite4b.push_back(string(sbuff1)+"<|#eta|<"+string(sbuff2));
	if (doData&&!doMC) {
		legend2->AddEntry(NoiseAsFcnNPV4EM,"R=0.4,EM Scale Data","P");
		legend2->AddEntry(NoiseAsFcnNPV4LC,"R=0.4,LC Scale Data","P");
		saveTwoHistoMarker(NoiseAsFcnNPV4EM,NoiseAsFcnNPV4LC,legend2,"NPV","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0);
		legend2->Clear();
	} else if (!doData&&doMC) {
		legend2->AddEntry(MCNoiseAsFcnNPV4EM,"R=0.4,EM Scale MC","P");
		legend2->AddEntry(MCNoiseAsFcnNPV4LC,"R=0.4,LC Scale MC","P");
		saveTwoHistoMarker(MCNoiseAsFcnNPV4EM,MCNoiseAsFcnNPV4LC,legend2,"NPV","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0);
		legend2->Clear();
	} else if (doData&&doMC) {
		legend4->AddEntry(NoiseAsFcnNPV4EM,"R=0.4,EM Scale Data","P");
		legend4->AddEntry(NoiseAsFcnNPV4LC,"R=0.4,LC Scale Data","P");
		legend4->AddEntry(MCNoiseAsFcnNPV4EM,"R=0.4,EM Scale MC","P");
		legend4->AddEntry(MCNoiseAsFcnNPV4LC,"R=0.4,LC Scale MC","P");
		saveFourHistoMarker(NoiseAsFcnNPV4EM,NoiseAsFcnNPV4LC,MCNoiseAsFcnNPV4EM,MCNoiseAsFcnNPV4LC,legend4,"NPV","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0);
		legend4->Clear();
	}
	if (doSecondary) {
		NoiseAsFcnNPV4EMSecondary->SetMarkerStyle(20);
		NoiseAsFcnNPV4LCSecondary->SetMarkerStyle(21);
		NoiseAsFcnNPV4LCSecondary->SetMarkerColor(4);

		MCNoiseAsFcnNPV4EMSecondary->SetMarkerStyle(24);
		MCNoiseAsFcnNPV4LCSecondary->SetMarkerStyle(25);
		MCNoiseAsFcnNPV4LCSecondary->SetMarkerColor(4);
		if (doData&&!doMC) {
			legend2->AddEntry(NoiseAsFcnNPV4EMSecondary,"R=0.4,EM Scale Data","P");
			legend2->AddEntry(NoiseAsFcnNPV4LCSecondary,"R=0.4,LC Scale Data","P");
			saveTwoHistoMarker(NoiseAsFcnNPV4EMSecondary,NoiseAsFcnNPV4LCSecondary,legend2,"NPV","Pile-up Noise[GeV]",stuffToWrite4b,outPutDir,false,0.20,0.88,0,0);
			legend2->Clear();
		} else if (!doData&&doMC) {
			legend2->AddEntry(MCNoiseAsFcnNPV4EMSecondary,"R=0.4,EM Scale MC","P");
			legend2->AddEntry(MCNoiseAsFcnNPV4LCSecondary,"R=0.4,LC Scale MC","P");
			saveTwoHistoMarker(MCNoiseAsFcnNPV4EMSecondary,MCNoiseAsFcnNPV4LCSecondary,legend2,"NPV","Pile-up Noise[GeV]",stuffToWrite4b,outPutDir,false,0.20,0.88,0,0);
			legend2->Clear();
		} else if (doData&&doMC) {
			legend4->AddEntry(NoiseAsFcnNPV4EMSecondary,"R=0.4,EM Scale Data","P");
			legend4->AddEntry(NoiseAsFcnNPV4LCSecondary,"R=0.4,LC Scale Data","P");
			legend4->AddEntry(MCNoiseAsFcnNPV4EMSecondary,"R=0.4,EM Scale MC","P");
			legend4->AddEntry(MCNoiseAsFcnNPV4LCSecondary,"R=0.4,LC Scale MC","P");
			saveFourHistoMarker(NoiseAsFcnNPV4EMSecondary,NoiseAsFcnNPV4LCSecondary,MCNoiseAsFcnNPV4EMSecondary,MCNoiseAsFcnNPV4LCSecondary,legend4,"NPV","Pile-up Noise[GeV]",stuffToWrite4b,outPutDir,false,0.20,0.88,0,0);
			legend4->Clear();
		}
	}
	TH1D *NoiseAsFcnMu4EMAll=new TH1D("NoiseAsFcnMu4EMAll","Noise as a function of mu for R=.4 EM ",maxMu-minMu,minMu,maxMu);
	TH1D *NoiseAsFcnMu4LCAll=new TH1D("NoiseAsFcnMu4LCAll","Noise as a function of mu for R=.4 LC ",maxMu-minMu,minMu,maxMu);

	TH1D *NoiseAsFcnMu4EMAllSecondary=new TH1D("NoiseAsFcnMu4EMAllSecondary","Noise as a function of mu for R=.4 EM ",maxMu-minMu,minMu,maxMu);
	TH1D *NoiseAsFcnMu4LCAllSecondary=new TH1D("NoiseAsFcnMu4LCAllSecondary","Noise as a function of mu for R=.4 LC ",maxMu-minMu,minMu,maxMu);

	TH1D *MCNoiseAsFcnMu4EMAll=new TH1D("MCNoiseAsFcnMu4EMAll","Noise as a function of mu for R=.4 EM ",maxMu-minMu,minMu,maxMu);
	TH1D *MCNoiseAsFcnMu4LCAll=new TH1D("MCNoiseAsFcnMu4LCAll","Noise as a function of mu for R=.4 LC ",maxMu-minMu,minMu,maxMu);

	TH1D *MCNoiseAsFcnMu4EMAllSecondary=new TH1D("MCNoiseAsFcnMu4EMAllSecondary","Noise as a function of mu for R=.4 EM ",maxMu-minMu,minMu,maxMu);
	TH1D *MCNoiseAsFcnMu4LCAllSecondary=new TH1D("MCNoiseAsFcnMu4LCAllSecondary","Noise as a function of mu for R=.4 LC ",maxMu-minMu,minMu,maxMu);

	if (doData) {
		for (int y=minMu;y<maxMu;y++) {
			//cout<<"NYBINS= "<<R4ConeEM_etaSlicesPos[startEtaStep]->GetNbinsY()<<endl;
			histo = (TH1D*)R4ConeEM_etaSlicesPos[startEtaStep]->ProjectionX("temp",1,R4ConeEM_etaSlicesPos[startEtaStep]->GetNbinsY(),y-minMu+1,y-minMu+1)->Clone(("R4ConeEM_etaSlicesPos"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_mu"+std::to_string(y)).c_str());
			for (int x=startEtaStep+1;x<startEtaStep+nEtaStepsToTake;x++) {
				histo->Add((TH1D*)R4ConeEM_etaSlicesPos[x]->ProjectionX("temp",1,R4ConeEM_etaSlicesPos[startEtaStep]->GetNbinsY(),y-minMu+1,y-minMu+1)->Clone(("hist"+std::to_string(histNameInt)).c_str()));
				histNameInt++;
			}
			tempWidth=getWidth(histo);
			if (tempWidth>=0){
				NoiseAsFcnMu4EMAll->Fill(y,tempWidth/TMath::Sqrt(2));
			}
			histo->Write();
			histo = (TH1D*)R4ConeLC_etaSlicesPos[startEtaStep]->ProjectionX("temp",1,R4ConeEM_etaSlicesPos[startEtaStep]->GetNbinsY(),y-minMu+1,y-minMu+1)->Clone(("R4ConeLC_etaSlicesPos"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_mu"+std::to_string(y)).c_str());
			for (int x=startEtaStep+1;x<startEtaStep+nEtaStepsToTake;x++) {
				histo->Add((TH1D*)R4ConeLC_etaSlicesPos[x]->ProjectionX("temp",1,R4ConeEM_etaSlicesPos[startEtaStep]->GetNbinsY(),y-minMu+1,y-minMu+1)->Clone(("hist"+std::to_string(histNameInt)).c_str()));
				histNameInt++;

			}
			tempWidth=getWidth(histo);
			if (tempWidth>=0){
				NoiseAsFcnMu4LCAll->Fill(y,tempWidth/TMath::Sqrt(2));
			}
			histo->Write();

		}
		NoiseAsFcnMu4EMAll->Write();
		NoiseAsFcnMu4LCAll->Write();

		if (doSecondary) {
			for (int y=minMu;y<maxMu;y++) {
				//cout<<"NYBINS= "<<R4ConeEM_etaSlicesSecondary[startEtaStep]->GetNbinsY()<<endl;
				histo = (TH1D*)R4ConeEM_etaSlicesSecondary[startEtaStep]->ProjectionX("temp",1,R4ConeEM_etaSlicesSecondary[startEtaStep]->GetNbinsY(),y-minMu+1,y-minMu+1)->Clone(("R4ConeEM_etaSlicesSecondary"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTakeSecondary)+"_mu"+std::to_string(y)).c_str());
				for (int x=startEtaStep+1;x<startEtaStep+nEtaStepsToTakeSecondary;x++) {
					histo->Add((TH1D*)R4ConeEM_etaSlicesSecondary[x]->ProjectionX("temp",1,R4ConeEM_etaSlicesSecondary[startEtaStep]->GetNbinsY(),y-minMu+1,y-minMu+1)->Clone(("hist"+std::to_string(histNameInt)).c_str()));
					histNameInt++;
				}
				tempWidth=getWidth(histo);
				if (tempWidth>=0){
					NoiseAsFcnMu4EMAllSecondary->Fill(y,tempWidth/TMath::Sqrt(2));
				}
				histo->Write();
				histo = (TH1D*)R4ConeLC_etaSlicesSecondary[startEtaStep]->ProjectionX("temp",1,R4ConeEM_etaSlicesSecondary[startEtaStep]->GetNbinsY(),y-minMu+1,y-minMu+1)->Clone(("R4ConeLC_etaSlicesSecondary"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTakeSecondary)+"_mu"+std::to_string(y)).c_str());
				for (int x=startEtaStep+1;x<startEtaStep+nEtaStepsToTakeSecondary;x++) {
					histo->Add((TH1D*)R4ConeLC_etaSlicesSecondary[x]->ProjectionX("temp",1,R4ConeEM_etaSlicesSecondary[startEtaStep]->GetNbinsY(),y-minMu+1,y-minMu+1)->Clone(("hist"+std::to_string(histNameInt)).c_str()));
					histNameInt++;

				}
				tempWidth=getWidth(histo);
				if (tempWidth>=0){
					NoiseAsFcnMu4LCAllSecondary->Fill(y,tempWidth/TMath::Sqrt(2));
				}
				histo->Write();

			}
			NoiseAsFcnMu4EMAllSecondary->Write();
			NoiseAsFcnMu4LCAllSecondary->Write();
		}
	}
	if (doMC) {
		for (int y=minMu;y<maxMu;y++) {
			//cout<<"NYBINS= "<<R4ConeEM_etaSlicesPos[startEtaStep]->GetNbinsY()<<endl;
			histo = (TH1D*)R4ConeEM_etaSlicesPosMC[startEtaStep]->ProjectionX("temp",1,R4ConeEM_etaSlicesPosMC[startEtaStep]->GetNbinsY(),y-minMu+1,y-minMu+1)->Clone(("R4ConeEM_etaSlicesPosMC"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_mu"+std::to_string(y)).c_str());
			for (int x=startEtaStep+1;x<startEtaStep+nEtaStepsToTake;x++) {
				histo->Add((TH1D*)R4ConeEM_etaSlicesPosMC[x]->ProjectionX("temp",1,R4ConeEM_etaSlicesPosMC[startEtaStep]->GetNbinsY(),y-minMu+1,y-minMu+1)->Clone(("hist"+std::to_string(histNameInt)).c_str()));
				histNameInt++;
			}
			tempWidth=getWidth(histo);
			if (tempWidth>=0){
				MCNoiseAsFcnMu4EMAll->Fill(y,tempWidth/TMath::Sqrt(2));
			}
			histo->Write();
			histo = (TH1D*)R4ConeLC_etaSlicesPosMC[startEtaStep]->ProjectionX("temp",1,R4ConeEM_etaSlicesPosMC[startEtaStep]->GetNbinsY(),y-minMu+1,y-minMu+1)->Clone(("R4ConeLC_etaSlicesPosMC"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_mu"+std::to_string(y)).c_str());
			for (int x=startEtaStep+1;x<startEtaStep+nEtaStepsToTake;x++) {
				histo->Add((TH1D*)R4ConeLC_etaSlicesPosMC[x]->ProjectionX("temp",1,R4ConeEM_etaSlicesPos[startEtaStep]->GetNbinsY(),y-minMu+1,y-minMu+1)->Clone(("hist"+std::to_string(histNameInt)).c_str()));
				histNameInt++;

			}
			tempWidth=getWidth(histo);
			if (tempWidth>=0){
				MCNoiseAsFcnMu4LCAll->Fill(y,tempWidth/TMath::Sqrt(2));
			}
			histo->Write();

		}
		MCNoiseAsFcnMu4EMAll->Write();
		MCNoiseAsFcnMu4LCAll->Write();

		if (doSecondary) {
			for (int y=minMu;y<maxMu;y++) {
				//cout<<"NYBINS= "<<R4ConeEM_etaSlicesSecondary[startEtaStep]->GetNbinsY()<<endl;
				histo = (TH1D*)R4ConeEM_etaSlicesSecondaryMC[startEtaStep]->ProjectionX("temp",1,R4ConeEM_etaSlicesSecondaryMC[startEtaStep]->GetNbinsY(),y-minMu+1,y-minMu+1)->Clone(("R4ConeEM_etaSlicesSecondaryMC"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTakeSecondary)+"_mu"+std::to_string(y)).c_str());
				for (int x=startEtaStep+1;x<startEtaStep+nEtaStepsToTakeSecondary;x++) {
					histo->Add((TH1D*)R4ConeEM_etaSlicesSecondaryMC[x]->ProjectionX("temp",1,R4ConeEM_etaSlicesSecondaryMC[startEtaStep]->GetNbinsY(),y-minMu+1,y-minMu+1)->Clone(("hist"+std::to_string(histNameInt)).c_str()));
					histNameInt++;
				}
				tempWidth=getWidth(histo);
				if (tempWidth>=0){
					MCNoiseAsFcnMu4EMAllSecondary->Fill(y,tempWidth/TMath::Sqrt(2));
				}
				histo->Write();
				histo = (TH1D*)R4ConeLC_etaSlicesSecondaryMC[startEtaStep]->ProjectionX("temp",1,R4ConeEM_etaSlicesSecondaryMC[startEtaStep]->GetNbinsY(),y-minMu+1,y-minMu+1)->Clone(("R4ConeLC_etaSlicesSecondaryMC"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTakeSecondary)+"_mu"+std::to_string(y)).c_str());
				for (int x=startEtaStep+1;x<startEtaStep+nEtaStepsToTakeSecondary;x++) {
					histo->Add((TH1D*)R4ConeLC_etaSlicesSecondaryMC[x]->ProjectionX("temp",1,R4ConeEM_etaSlicesSecondaryMC[startEtaStep]->GetNbinsY(),y-minMu+1,y-minMu+1)->Clone(("hist"+std::to_string(histNameInt)).c_str()));
					histNameInt++;

				}
				tempWidth=getWidth(histo);
				if (tempWidth>=0){
					MCNoiseAsFcnMu4LCAllSecondary->Fill(y,tempWidth/TMath::Sqrt(2));
				}
				histo->Write();

			}
			MCNoiseAsFcnMu4EMAllSecondary->Write();
			MCNoiseAsFcnMu4LCAllSecondary->Write();
		}
	}
	NoiseAsFcnMu4EMAll->SetMarkerStyle(20);
	NoiseAsFcnMu4LCAll->SetMarkerStyle(21);
	NoiseAsFcnMu4LCAll->SetMarkerColor(4);

	MCNoiseAsFcnMu4EMAll->SetMarkerStyle(24);
	MCNoiseAsFcnMu4LCAll->SetMarkerStyle(25);
	MCNoiseAsFcnMu4LCAll->SetMarkerColor(4);
	if (doData&&!doMC) {
		legend2->AddEntry(NoiseAsFcnMu4EMAll,"R=0.4,EM Scale Data","P");
		legend2->AddEntry(NoiseAsFcnMu4LCAll,"R=0.4,LC Scale Data","P");
		saveTwoHistoMarker(NoiseAsFcnMu4EMAll,NoiseAsFcnMu4LCAll,legend2,"<#mu>","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0);
		legend2->Clear();
	} else if (!doData&&doMC) {
		legend2->AddEntry(MCNoiseAsFcnMu4EMAll,"R=0.4,EM Scale MC","P");
		legend2->AddEntry(MCNoiseAsFcnMu4LCAll,"R=0.4,LC Scale MC","P");
		saveTwoHistoMarker(MCNoiseAsFcnMu4EMAll,MCNoiseAsFcnMu4LCAll,legend2,"<#mu>","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0);
		legend2->Clear();
	} else if (doData&&doMC) {
		legend4->AddEntry(NoiseAsFcnMu4EMAll,"R=0.4,EM Scale Data","P");
		legend4->AddEntry(NoiseAsFcnMu4LCAll,"R=0.4,LC Scale Data","P");
		legend4->AddEntry(MCNoiseAsFcnMu4EMAll,"R=0.4,EM Scale MC","P");
		legend4->AddEntry(MCNoiseAsFcnMu4LCAll,"R=0.4,LC Scale MC","P");
		saveFourHistoMarker(NoiseAsFcnMu4EMAll,NoiseAsFcnMu4LCAll,MCNoiseAsFcnMu4EMAll,MCNoiseAsFcnMu4LCAll,legend4,"<#mu>","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0);
		legend4->Clear();
	}
	if (doSecondary) {
		NoiseAsFcnMu4EMAllSecondary->SetMarkerStyle(20);
		NoiseAsFcnMu4LCAllSecondary->SetMarkerStyle(21);
		NoiseAsFcnMu4LCAllSecondary->SetMarkerColor(4);

		MCNoiseAsFcnMu4EMAllSecondary->SetMarkerStyle(24);
		MCNoiseAsFcnMu4LCAllSecondary->SetMarkerStyle(25);
		MCNoiseAsFcnMu4LCAllSecondary->SetMarkerColor(4);
		if (doData&&!doMC) {
			legend2->AddEntry(NoiseAsFcnMu4EMAllSecondary,"R=0.4,EM Scale Data","P");
			legend2->AddEntry(NoiseAsFcnMu4LCAllSecondary,"R=0.4,LC Scale Data","P");
			saveTwoHistoMarker(NoiseAsFcnMu4EMAllSecondary,NoiseAsFcnMu4LCAllSecondary,legend2,"<#mu>","Pile-up Noise[GeV]",stuffToWrite4b,outPutDir,false,0.20,0.88,0,0);
			legend2->Clear();
		} else if (!doData&&doMC) {
			legend2->AddEntry(MCNoiseAsFcnMu4EMAllSecondary,"R=0.4,EM Scale MC","P");
			legend2->AddEntry(MCNoiseAsFcnMu4LCAllSecondary,"R=0.4,LC Scale MC","P");
			saveTwoHistoMarker(MCNoiseAsFcnMu4EMAllSecondary,MCNoiseAsFcnMu4LCAllSecondary,legend2,"<#mu>","Pile-up Noise[GeV]",stuffToWrite4b,outPutDir,false,0.20,0.88,0,0);
			legend2->Clear();
		} else if (doData&&doMC) {
			legend4->AddEntry(NoiseAsFcnMu4EMAllSecondary,"R=0.4,EM Scale Data","P");
			legend4->AddEntry(NoiseAsFcnMu4LCAllSecondary,"R=0.4,LC Scale Data","P");
			legend4->AddEntry(MCNoiseAsFcnMu4EMAllSecondary,"R=0.4,EM Scale MC","P");
			legend4->AddEntry(MCNoiseAsFcnMu4LCAllSecondary,"R=0.4,LC Scale MC","P");
			saveFourHistoMarker(NoiseAsFcnMu4EMAllSecondary,NoiseAsFcnMu4LCAllSecondary,MCNoiseAsFcnMu4EMAllSecondary,MCNoiseAsFcnMu4LCAllSecondary,legend4,"<#mu>","Pile-up Noise[GeV]",stuffToWrite4b,outPutDir,false,0.20,0.88,0,0);
			legend4->Clear();
		}
	}


	TH1D *NoiseAsFcnMu4EMNPV;
	TH1D *NoiseAsFcnMu4LCNPV;

	TH1D *NoiseAsFcnMu4EMNPVSecondary;
	TH1D *NoiseAsFcnMu4LCNPVSecondary;

	TH1D *MCNoiseAsFcnMu4EMNPV;
	TH1D *MCNoiseAsFcnMu4LCNPV;

	TH1D *MCNoiseAsFcnMu4EMNPVSecondary;
	TH1D *MCNoiseAsFcnMu4LCNPVSecondary;
	vector<std::string> stuffToWrite6;
	for (int z=minNPV;z<maxNPV;z++) {
		stuffToWrite6.push_back("Random Cone");
		stuffToWrite6.push_back("Noise Term");
		stuffToWrite6.push_back(string(buff1)+"<|#eta|<"+string(buff2)+" NPV="+std::to_string(z));

		NoiseAsFcnMu4EMNPV=new TH1D(("NoiseAsFcnMu4EMNPV"+std::to_string(z)).c_str(),"Noise as a function of NPV for R=.4 EM ",maxMu-minMu,minMu,maxMu);
		NoiseAsFcnMu4LCNPV=new TH1D(("NoiseAsFcnMu4LCNPV"+std::to_string(z)).c_str(),"Noise as a function of NPV for R=.4 LC ",maxMu-minMu,minMu,maxMu);

		MCNoiseAsFcnMu4EMNPV=new TH1D(("MCNoiseAsFcnMu4EMNPV"+std::to_string(z)).c_str(),"Noise as a function of NPV for R=.4 EM ",maxMu-minMu,minMu,maxMu);
		MCNoiseAsFcnMu4LCNPV=new TH1D(("MCNoiseAsFcnMu4LCNPV"+std::to_string(z)).c_str(),"Noise as a function of NPV for R=.4 LC ",maxMu-minMu,minMu,maxMu);
		if (doData) {
			for (int y=minMu;y<maxMu;y++) {
				histo = (TH1D*)R4ConeEM_etaSlicesPos[startEtaStep]->ProjectionX("temp",z-minNPV+1,z-minNPV+1,y-minMu+1,y-minMu+1)->Clone(("R4ConeEM_etaSlicesPos"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_mu"+std::to_string(y)+"_NPV"+std::to_string(z)).c_str());
				for (int x=startEtaStep+1;x<startEtaStep+nEtaStepsToTake;x++) {
					histo->Add((TH1D*)R4ConeEM_etaSlicesPos[x]->ProjectionX("temp",z-minNPV+1,z-minNPV+1,y-minMu+1,y-minMu+1)->Clone(("hist"+std::to_string(histNameInt)).c_str()));
					histNameInt++;
				}
				tempWidth=getWidth(histo);
				if (tempWidth>=0){
					NoiseAsFcnMu4EMNPV->Fill(y,tempWidth/TMath::Sqrt(2));
				}
				histo->Write();
				histo = (TH1D*)R4ConeLC_etaSlicesPos[startEtaStep]->ProjectionX("temp",z-minNPV+1,z-minNPV+1,y-minMu+1,y-minMu+1)->Clone(("R4ConeLC_etaSlicesPos"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_mu"+std::to_string(y)+"_NPV"+std::to_string(z)).c_str());
				for (int x=startEtaStep+1;x<startEtaStep+nEtaStepsToTake;x++) {
					histo->Add((TH1D*)R4ConeLC_etaSlicesPos[x]->ProjectionX("temp",z-minNPV+1,z-minNPV+1,y-minMu+1,y-minMu+1)->Clone(("hist"+std::to_string(histNameInt)).c_str()));
					histNameInt++;
				}
				tempWidth=getWidth(histo);
				if (tempWidth>=0){
					NoiseAsFcnMu4LCNPV->Fill(y,tempWidth/TMath::Sqrt(2));
				}
				histo->Write();
			}
			NoiseAsFcnMu4EMNPV->Write();
			NoiseAsFcnMu4LCNPV->Write();
		}
		if (doMC) {
			for (int y=minMu;y<maxMu;y++) {
				histo = (TH1D*)R4ConeEM_etaSlicesPosMC[startEtaStep]->ProjectionX("temp",z-minNPV+1,z-minNPV+1,y-minMu+1,y-minMu+1)->Clone(("R4ConeEM_etaSlicesPosMC"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_mu"+std::to_string(y)+"_NPV"+std::to_string(z)).c_str());
				for (int x=startEtaStep+1;x<startEtaStep+nEtaStepsToTake;x++) {
					histo->Add((TH1D*)R4ConeEM_etaSlicesPosMC[x]->ProjectionX("temp",z-minNPV+1,z-minNPV+1,y-minMu+1,y-minMu+1)->Clone(("hist"+std::to_string(histNameInt)).c_str()));
					histNameInt++;
				}
				tempWidth=getWidth(histo);
				if (tempWidth>=0){
					MCNoiseAsFcnMu4EMNPV->Fill(y,tempWidth/TMath::Sqrt(2));
				}
				histo->Write();
				histo = (TH1D*)R4ConeLC_etaSlicesPosMC[startEtaStep]->ProjectionX("temp",z-minNPV+1,z-minNPV+1,y-minMu+1,y-minMu+1)->Clone(("R4ConeLC_etaSlicesPosMC"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_mu"+std::to_string(y)+"_NPV"+std::to_string(z)).c_str());
				for (int x=startEtaStep+1;x<startEtaStep+nEtaStepsToTake;x++) {
					histo->Add((TH1D*)R4ConeLC_etaSlicesPosMC[x]->ProjectionX("temp",z-minNPV+1,z-minNPV+1,y-minMu+1,y-minMu+1)->Clone(("hist"+std::to_string(histNameInt)).c_str()));
					histNameInt++;
				}
				tempWidth=getWidth(histo);
				if (tempWidth>=0){
					MCNoiseAsFcnMu4LCNPV->Fill(y,tempWidth/TMath::Sqrt(2));
				}
				histo->Write();
			}
			MCNoiseAsFcnMu4EMNPV->Write();
			MCNoiseAsFcnMu4LCNPV->Write();
		}
		NoiseAsFcnMu4EMNPV->SetMarkerStyle(20);
		NoiseAsFcnMu4LCNPV->SetMarkerStyle(21);
		NoiseAsFcnMu4LCNPV->SetMarkerColor(4);

		MCNoiseAsFcnMu4EMNPV->SetMarkerStyle(24);
		MCNoiseAsFcnMu4LCNPV->SetMarkerStyle(25);
		MCNoiseAsFcnMu4LCNPV->SetMarkerColor(4);
		if (doData&&!doMC) {
			legend2->AddEntry(NoiseAsFcnMu4EMNPV,"R=0.4,EM Scale Data","P");
			legend2->AddEntry(NoiseAsFcnMu4LCNPV,"R=0.4,LC Scale Data","P");
			saveTwoHistoMarker(NoiseAsFcnMu4EMNPV,NoiseAsFcnMu4LCNPV,legend2,"<#mu>","Pile-up Noise[GeV]",stuffToWrite6,outPutDir,false,0.20,0.88,0,0);
			legend2->Clear();
		} else if (!doData&&doMC) {
			legend2->AddEntry(MCNoiseAsFcnMu4EMNPV,"R=0.4,EM Scale MC","P");
			legend2->AddEntry(MCNoiseAsFcnMu4LCNPV,"R=0.4,LC Scale MC","P");
			saveTwoHistoMarker(MCNoiseAsFcnMu4EMNPV,MCNoiseAsFcnMu4LCNPV,legend2,"<#mu>","Pile-up Noise[GeV]",stuffToWrite6,outPutDir,false,0.20,0.88,0,0);
			legend2->Clear();
		} else if (doData&&doMC) {
			legend4->AddEntry(NoiseAsFcnMu4EMNPV,"R=0.4,EM Scale Data","P");
			legend4->AddEntry(NoiseAsFcnMu4LCNPV,"R=0.4,LC Scale Data","P");
			legend4->AddEntry(MCNoiseAsFcnMu4EMNPV,"R=0.4,EM Scale MC","P");
			legend4->AddEntry(MCNoiseAsFcnMu4LCNPV,"R=0.4,LC Scale MC","P");
			saveFourHistoMarker(NoiseAsFcnMu4EMNPV,NoiseAsFcnMu4LCNPV,MCNoiseAsFcnMu4EMNPV,MCNoiseAsFcnMu4LCNPV,legend4,"<#mu>","Pile-up Noise[GeV]",stuffToWrite6,outPutDir,false,0.20,0.88,0,0);
			legend4->Clear();
		}
		stuffToWrite6.clear();
	}
	if (doSecondary) {
		for (int z=minNPV;z<maxNPV;z++) {
			stuffToWrite6.push_back("Random Cone");
			stuffToWrite6.push_back("Noise Term");
			stuffToWrite6.push_back(string(sbuff1)+"<|#eta|<"+string(sbuff2)+" NPV="+std::to_string(z));

			NoiseAsFcnMu4EMNPVSecondary=new TH1D(("NoiseAsFcnMu4EMNPVSecondary"+std::to_string(z)).c_str(),"Noise as a function of NPV for R=.4 EM ",maxMu-minMu,minMu,maxMu);
			NoiseAsFcnMu4LCNPVSecondary=new TH1D(("NoiseAsFcnMu4LCNPVSecondary"+std::to_string(z)).c_str(),"Noise as a function of NPV for R=.4 LC ",maxMu-minMu,minMu,maxMu);

			MCNoiseAsFcnMu4EMNPVSecondary=new TH1D(("MCNoiseAsFcnMu4EMNPVSecondary"+std::to_string(z)).c_str(),"Noise as a function of NPV for R=.4 EM ",maxMu-minMu,minMu,maxMu);
			MCNoiseAsFcnMu4LCNPVSecondary=new TH1D(("MCNoiseAsFcnMu4LCNPVSecondary"+std::to_string(z)).c_str(),"Noise as a function of NPV for R=.4 LC ",maxMu-minMu,minMu,maxMu);
			if (doData) {
				for (int y=minMu;y<maxMu;y++) {
					histo = (TH1D*)R4ConeEM_etaSlicesSecondary[startEtaStep]->ProjectionX("temp",z-minNPV+1,z-minNPV+1,y-minMu+1,y-minMu+1)->Clone(("R4ConeEM_etaSlicesSecondary"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_mu"+std::to_string(y)+"_NPV"+std::to_string(z)).c_str());
					for (int x=startEtaStep+1;x<startEtaStep+nEtaStepsToTakeSecondary;x++) {
						histo->Add((TH1D*)R4ConeEM_etaSlicesSecondary[x]->ProjectionX("temp",z-minNPV+1,z-minNPV+1,y-minMu+1,y-minMu+1)->Clone(("hist"+std::to_string(histNameInt)).c_str()));
						histNameInt++;
					}
					tempWidth=getWidth(histo);
					if (tempWidth>=0){
						NoiseAsFcnMu4EMNPVSecondary->Fill(y,tempWidth/TMath::Sqrt(2));
					}
					histo->Write();
					histo = (TH1D*)R4ConeLC_etaSlicesSecondary[startEtaStep]->ProjectionX("temp",z-minNPV+1,z-minNPV+1,y-minMu+1,y-minMu+1)->Clone(("R4ConeLC_etaSlicesSecondary"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_mu"+std::to_string(y)+"_NPV"+std::to_string(z)).c_str());
					for (int x=startEtaStep+1;x<startEtaStep+nEtaStepsToTakeSecondary;x++) {
						histo->Add((TH1D*)R4ConeLC_etaSlicesSecondary[x]->ProjectionX("temp",z-minNPV+1,z-minNPV+1,y-minMu+1,y-minMu+1)->Clone(("hist"+std::to_string(histNameInt)).c_str()));
						histNameInt++;
					}
					tempWidth=getWidth(histo);
					if (tempWidth>=0){
						NoiseAsFcnMu4LCNPVSecondary->Fill(y,tempWidth/TMath::Sqrt(2));
					}
					histo->Write();
				}
				NoiseAsFcnMu4EMNPVSecondary->Write();
				NoiseAsFcnMu4LCNPVSecondary->Write();
			}
			if (doMC) {
				for (int y=minMu;y<maxMu;y++) {
					histo = (TH1D*)R4ConeEM_etaSlicesSecondaryMC[startEtaStep]->ProjectionX("temp",z-minNPV+1,z-minNPV+1,y-minMu+1,y-minMu+1)->Clone(("R4ConeEM_etaSlicesSecondaryMC"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_mu"+std::to_string(y)+"_NPV"+std::to_string(z)).c_str());
					for (int x=startEtaStep+1;x<startEtaStep+nEtaStepsToTakeSecondary;x++) {
						histo->Add((TH1D*)R4ConeEM_etaSlicesSecondaryMC[x]->ProjectionX("temp",z-minNPV+1,z-minNPV+1,y-minMu+1,y-minMu+1)->Clone(("hist"+std::to_string(histNameInt)).c_str()));
						histNameInt++;
					}
					tempWidth=getWidth(histo);
					if (tempWidth>=0){
						MCNoiseAsFcnMu4EMNPVSecondary->Fill(y,tempWidth/TMath::Sqrt(2));
					}
					histo->Write();
					histo = (TH1D*)R4ConeLC_etaSlicesSecondaryMC[startEtaStep]->ProjectionX("temp",z-minNPV+1,z-minNPV+1,y-minMu+1,y-minMu+1)->Clone(("R4ConeLC_etaSlicesSecondaryMC"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_mu"+std::to_string(y)+"_NPV"+std::to_string(z)).c_str());
					for (int x=startEtaStep+1;x<startEtaStep+nEtaStepsToTakeSecondary;x++) {
						histo->Add((TH1D*)R4ConeLC_etaSlicesSecondaryMC[x]->ProjectionX("temp",z-minNPV+1,z-minNPV+1,y-minMu+1,y-minMu+1)->Clone(("hist"+std::to_string(histNameInt)).c_str()));
						histNameInt++;
					}
					tempWidth=getWidth(histo);
					if (tempWidth>=0){
						MCNoiseAsFcnMu4LCNPVSecondary->Fill(y,tempWidth/TMath::Sqrt(2));
					}
					histo->Write();
				}
				MCNoiseAsFcnMu4EMNPVSecondary->Write();
				MCNoiseAsFcnMu4LCNPVSecondary->Write();
			}
			NoiseAsFcnMu4EMNPVSecondary->SetMarkerStyle(20);
			NoiseAsFcnMu4LCNPVSecondary->SetMarkerStyle(21);
			NoiseAsFcnMu4LCNPVSecondary->SetMarkerColor(4);

			MCNoiseAsFcnMu4EMNPVSecondary->SetMarkerStyle(24);
			MCNoiseAsFcnMu4LCNPVSecondary->SetMarkerStyle(25);
			MCNoiseAsFcnMu4LCNPVSecondary->SetMarkerColor(4);
			if (doData&&!doMC) {
				legend2->AddEntry(NoiseAsFcnMu4EMNPVSecondary,"R=0.4,EM Scale Data","P");
				legend2->AddEntry(NoiseAsFcnMu4LCNPVSecondary,"R=0.4,LC Scale Data","P");
				saveTwoHistoMarker(NoiseAsFcnMu4EMNPVSecondary,NoiseAsFcnMu4LCNPVSecondary,legend2,"<#mu>","Pile-up Noise[GeV]",stuffToWrite6,outPutDir,false,0.20,0.88,0,0);
				legend2->Clear();
			} else if (!doData&&doMC) {
				legend2->AddEntry(MCNoiseAsFcnMu4EMNPVSecondary,"R=0.4,EM Scale MC","P");
				legend2->AddEntry(MCNoiseAsFcnMu4LCNPVSecondary,"R=0.4,LC Scale MC","P");
				saveTwoHistoMarker(MCNoiseAsFcnMu4EMNPVSecondary,MCNoiseAsFcnMu4LCNPVSecondary,legend2,"<#mu>","Pile-up Noise[GeV]",stuffToWrite6,outPutDir,false,0.20,0.88,0,0);
				legend2->Clear();
			} else if (doData&&doMC) {
				legend4->AddEntry(NoiseAsFcnMu4EMNPVSecondary,"R=0.4,EM Scale Data","P");
				legend4->AddEntry(NoiseAsFcnMu4LCNPVSecondary,"R=0.4,LC Scale Data","P");
				legend4->AddEntry(MCNoiseAsFcnMu4EMNPVSecondary,"R=0.4,EM Scale MC","P");
				legend4->AddEntry(MCNoiseAsFcnMu4LCNPVSecondary,"R=0.4,LC Scale MC","P");
				saveFourHistoMarker(NoiseAsFcnMu4EMNPVSecondary,NoiseAsFcnMu4LCNPVSecondary,MCNoiseAsFcnMu4EMNPVSecondary,MCNoiseAsFcnMu4LCNPVSecondary,legend4,"<#mu>","Pile-up Noise[GeV]",stuffToWrite6,outPutDir,false,0.20,0.88,0,0);
				legend4->Clear();
			}
			stuffToWrite6.clear();
		}
	}
	outputFile->Close();
	/*
	for (int x=0;x<nEtaSteps;x++) {
		if (doData) {
			dataMuDist[x]=new TH1D(("dataMuDist"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),("dataMuDist"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),maxMu-minMu,minMu,maxMu);
			dataNPVDist[x]=new TH1D(("dataNPVDist"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),("dataMuDist"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),maxNPV-minNPV,minNPV,maxNPV);

		}
		if (doMC) {
			mcMuDist[x]=new TH1D(("mcMuDist"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),("mcMuDist"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),maxMu-minMu,minMu,maxMu);
			mcNPVDist[x]=new TH1D(("mcNPVDist"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),("mcMuDist"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),maxNPV-minNPV,minNPV,maxNPV);
		}
	}
	if (doData) {
		dataMuDist[nEtaSteps]=new TH1D("dataMuDistALL","dataMuDistALL",maxMu-minMu,minMu,maxMu);
		dataNPVDist[nEtaSteps]=new TH1D("dataNPVDistALL","dataMuDistALL",maxNPV-minNPV,minNPV,maxNPV);
	}
	if (doMC) {
		mcMuDist[nEtaSteps]=new TH1D("mcMuDistALL","mcMuDistALL",maxMu-minMu,minMu,maxMu);
		mcNPVDist[nEtaSteps]=new TH1D("mcNPVDistALL","mcMuDistALL",maxNPV-minNPV,minNPV,maxNPV);
	}
	*/
	vector<std::string> stuffToWrite7;
	cout<<"hi4"<<endl;
	if (!doMC&&doData) {
		for (int x=0;x<nEtaSteps;x++) {
			saveSingleHisto(emptyConesSkippedHistoEM[x],"EM","Number of Events",stuffToWrite7,outPutDir,false,0.20,0.88,0,0);
			saveSingleHisto(emptyConesSkippedHistoLC[x],"LC","Number of Events",stuffToWrite7,outPutDir,false,0.20,0.88,0,0);
		}
	}
	if (doMC&&!doData) {
		for (int x=0;x<nEtaSteps;x++) {
			saveSingleHisto(emptyConesSkippedHistoEMMC[x],"EM","Number of Events",stuffToWrite7,outPutDir,false,0.20,0.88,0,0);
			saveSingleHisto(emptyConesSkippedHistoLCMC[x],"LC","Number of Events",stuffToWrite7,outPutDir,false,0.20,0.88,0,0);
		}
	}
	if (doMC&&doData) {
		for (int x=0;x<nEtaSteps;x++) {
			emptyConesSkippedHistoEMMC[x]->SetLineColor(2);
			cout<<"hi5"<<endl;
			legend2->AddEntry(emptyConesSkippedHistoEM[x],"R=0.4,EM Data","L");
			cout<<"hi6"<<endl;
			legend2->AddEntry(emptyConesSkippedHistoEMMC[x],"R=0.4,EM MC","L");
			cout<<"hi7"<<endl;
			saveTwoHisto(emptyConesSkippedHistoEM[x],emptyConesSkippedHistoEMMC[x],legend2,"blank","number",stuffToWrite7,outPutDir,true,0.20,0.88,0,0);
			cout<<"hi8"<<endl;
			legend2->Clear();

			emptyConesSkippedHistoLCMC[x]->SetLineColor(2);
			legend2->AddEntry(emptyConesSkippedHistoLC[x],"R=0.4,LC Data","L");
			legend2->AddEntry(emptyConesSkippedHistoLCMC[x],"R=0.4,LC MC","L");
			saveTwoHisto(emptyConesSkippedHistoLC[x],emptyConesSkippedHistoLCMC[x],legend2,"blank","number",stuffToWrite7,outPutDir,true,0.20,0.88,0,0);
			legend2->Clear();
		}
	}
	outputFile=new TFile((pileUpOutput).c_str(),"RECREATE");
	cout<<"hi1"<<endl;
	for (int x=0;x<nEtaSteps;x++) {
		if (doData) {
			dataMuDist[x]=(TH1D*)R4ConeEM_etaSlicesPos[x]->ProjectionZ("temp",1,R4ConeEM_etaSlicesPos[x]->GetNbinsX(),1,R4ConeEM_etaSlicesPos[x]->GetNbinsY())->Clone(("R4ConeEM_etaSlicesPosDataMu"+std::to_string(x)+"_"+std::to_string(x+1)).c_str());
			dataMuDist[x]->Write();
			dataNPVDist[x]=(TH1D*)R4ConeEM_etaSlicesPos[x]->ProjectionY("temp",1,R4ConeEM_etaSlicesPos[x]->GetNbinsX(),1,R4ConeEM_etaSlicesPos[x]->GetNbinsZ())->Clone(("R4ConeEM_etaSlicesPosDataNPV"+std::to_string(x)+"_"+std::to_string(x+1)).c_str());
			dataNPVDist[x]->Write();

		}
		if (doMC) {
			mcMuDist[x]=(TH1D*)R4ConeEM_etaSlicesPosMC[x]->ProjectionZ("temp",1,R4ConeEM_etaSlicesPosMC[x]->GetNbinsX(),1,R4ConeEM_etaSlicesPosMC[x]->GetNbinsY())->Clone(("R4ConeEM_etaSlicesPosMCMu"+std::to_string(x)+"_"+std::to_string(x+1)).c_str());
			mcMuDist[x]->Write();
			mcNPVDist[x]=(TH1D*)R4ConeEM_etaSlicesPosMC[x]->ProjectionY("temp",1,R4ConeEM_etaSlicesPosMC[x]->GetNbinsX(),1,R4ConeEM_etaSlicesPosMC[x]->GetNbinsZ())->Clone(("R4ConeEM_etaSlicesPosMCNPV"+std::to_string(x)+"_"+std::to_string(x+1)).c_str());
			mcNPVDist[x]->Write();
		}
		if (doMC&&doData) {
			mcMuDist[x]->SetLineColor(2);
			legend2->AddEntry(dataMuDist[x],"R=0.4,EM Data","L");
			legend2->AddEntry(mcMuDist[x],"R=0.4,EM MC","L");
			saveTwoHisto(dataMuDist[x],mcMuDist[x],legend2,"<mu>","Normalized Entries",stuffToWrite7,outPutDir,true,0.20,0.88,0,0);
			legend2->Clear();

			mcNPVDist[x]->SetLineColor(2);
			legend2->AddEntry(dataNPVDist[x],"R=0.4,EM Data","L");
			legend2->AddEntry(mcNPVDist[x],"R=0.4,EM MC","L");
			saveTwoHisto(dataNPVDist[x],mcNPVDist[x],legend2,"NPV","Normalized Entries",stuffToWrite7,outPutDir,true,0.20,0.88,0,0);
			legend2->Clear();
		}
	}
	cout<<"hi2"<<endl;
	if (doData) {
		dataMuDist[nEtaSteps]=dataMuDist[0];
		dataMuDist[nEtaSteps]->SetName("R4ConeEM_etaSlicesPosDataMuALL");
		dataNPVDist[nEtaSteps]=dataNPVDist[0];
		dataNPVDist[nEtaSteps]->SetName("R4ConeEM_etaSlicesPosDataNPVALL");
	}
	if (doMC) {
		mcMuDist[nEtaSteps]=mcMuDist[0];
		mcMuDist[nEtaSteps]->SetName("R4ConeEM_etaSlicesPosMCMuALL");
		mcNPVDist[nEtaSteps]=mcNPVDist[0];
		mcNPVDist[nEtaSteps]->SetName("R4ConeEM_etaSlicesPosMCNPVALL");
	}
	cout<<"hi3"<<endl;
	for (int x=1;x<nEtaSteps;x++) {
		if (doData) {
			dataMuDist[nEtaSteps]->Add(dataMuDist[x]);
			dataNPVDist[nEtaSteps]->Add(dataNPVDist[x]);
		}
		if (doMC) {
			mcMuDist[nEtaSteps]->Add(mcMuDist[x]);
			mcNPVDist[nEtaSteps]->Add(mcNPVDist[x]);
		}
	}

	cout<<"hi3d"<<endl;
	if (doData) {
		dataMuDist[nEtaSteps]->Write();
		dataNPVDist[nEtaSteps]->Write();
	}
	if (doMC) {
		mcMuDist[nEtaSteps]->Write();
		mcNPVDist[nEtaSteps]->Write();
	}
	cout<<"hi6"<<endl;
	if (doMC&&doData) {

		mcMuDist[nEtaSteps]->SetLineColor(2);
		legend2->AddEntry(dataMuDist[nEtaSteps],"R=0.4,EM Data","L");
		legend2->AddEntry(mcMuDist[nEtaSteps],"R=0.4,EM MC","L");
		saveTwoHisto(dataMuDist[nEtaSteps],mcMuDist[nEtaSteps],legend2,"<mu>","Normalized Entries",stuffToWrite7,outPutDir,true,0.20,0.88,0,0);
		legend2->Clear();

		mcNPVDist[nEtaSteps]->SetLineColor(2);
		legend2->AddEntry(dataNPVDist[nEtaSteps],"R=0.4,EM Data","L");
		legend2->AddEntry(mcNPVDist[nEtaSteps],"R=0.4,EM MC","L");
		saveTwoHisto(dataNPVDist[nEtaSteps],mcNPVDist[nEtaSteps],legend2,"NPV","Normalized Entries",stuffToWrite7,outPutDir,true,0.20,0.88,0,0);
		legend2->Clear();

		if (doMuComparisonsFromMakePlots4==true) {
			averageMuHistoMC->SetLineColor(2);
			legend2->AddEntry(averageMuHisto,"R=0.4,EM Data","L");
			legend2->AddEntry(averageMuHistoMC,"R=0.4,EM MC","L");
			saveTwoHisto(averageMuHisto,averageMuHistoMC,legend2,"<mu>","Normalized Entries",stuffToWrite7,outPutDir,true,0.20,0.88,0,0);
			legend2->Clear();

			correctedAverageMuHistoMC->SetLineColor(2);
			legend2->AddEntry(correctedAverageMuHisto,"R=0.4,EM Data","L");
			legend2->AddEntry(correctedAverageMuHistoMC,"R=0.4,EM MC","L");
			saveTwoHisto(correctedAverageMuHisto,correctedAverageMuHistoMC,legend2,"<mu>","Normalized Entries",stuffToWrite7,outPutDir,true,0.20,0.88,0,0);
			legend2->Clear();

			NPVHistoMC->SetLineColor(2);
			legend2->AddEntry(NPVHisto,"R=0.4,EM Data","L");
			legend2->AddEntry(NPVHistoMC,"R=0.4,EM MC","L");
			saveTwoHisto(NPVHisto,NPVHistoMC,legend2,"NPV","Normalized Entries",stuffToWrite7,outPutDir,true,0.20,0.88,0,0);
			legend2->Clear();
		}

	}
	cout<<"hi7"<<endl;
	if (!doMC&&doData) {
		saveSingleHisto(averageMuHisto,"<mu>","Number of Events",stuffToWrite7,outPutDir,false,0.20,0.88,0,0);
		saveSingleHisto(correctedAverageMuHisto,"<mu>","Number of Events",stuffToWrite7,outPutDir,false,0.20,0.88,0,0);
		saveSingleHisto(NPVHisto,"NPV","Number of Events",stuffToWrite7,outPutDir,false,0.20,0.88,0,0);
	}
	if (doMC&&!doData) {
		saveSingleHisto(averageMuHistoMC,"<mu>","Number of Events",stuffToWrite7,outPutDir,false,0.20,0.88,0,0);
		saveSingleHisto(correctedAverageMuHistoMC,"<mu>","Number of Events",stuffToWrite7,outPutDir,false,0.20,0.88,0,0);
		saveSingleHisto(NPVHistoMC,"NPV","Number of Events",stuffToWrite7,outPutDir,false,0.20,0.88,0,0);
	}
	outputFile->Close();
}