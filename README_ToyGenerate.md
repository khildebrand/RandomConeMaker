### Author: Kevin Hildebrand
### Email: khildebrand@uchicago.edu

This package is for producing toys to estimate the error on the noise term.

## What this package does:

This package generates toys using a root file from the SavePlots package and then adds error bars to the plots from the SavePlots package. This is done by two different scripts.

## How to configure this package:

This package has two different scripts each with there own config file.

For `ToyTest2Condor.cxx` lets assume the config script is called `ToyTest2Condor.config`
This file allows one to set the following
NB: Numbers with a '\*' in them means they need to be the same as the ones in `makeCones.config`
Numbers with a '\*\*' in them means they need to be the same as the one in `SavePlots.config`

1. Name of the input file for doing the Toys (default is blah, example in config script is `ToyTestInputFile /share/home/khildebr/qualTask/run/ToyTestInputFile_V201_300Data_scale.root`). This is the root file produced by SavePlots package and so it will be whatever you named it in the config script for that package.
2. Whether or not to run on data root files (default is true, example in config script is `doData true`)
3. Whether or not to run on MC root files (default is true, example in config script is `doMC true`)
4. * What eta blocks should be used for the primary A distributions. This should be a comma separated list. (default is "0,.8", example in config script "etaBlocks 0,.8,1.2,2.1,2.8,3.2,3.6,4.5"). So if for example you set this to x,y,z then there will be an A distribution for x<|eta|<y and y<|eta|<z.
5. Whether or not to do a second range of eta blocks (default is false, example in config script "doSecondary True").
  * What eta blocks should be used for the secondary A distributions. This should be a comma separated list. (default is "0,2.1", example in config script "etaBlocksSecondary 0,.8,1.2,2.1,2.8,3.2,3.6,4.5"). So if for example you set this to x,y,z then there will be an A distribution for x<|eta|<y and y<|eta|<z.
6. ** What eta step to start at (default is 0, example in config script "startEtaStep 0"). For all plots that are the width of the A distribution as function of anything other than eta this sets the lower cut on eta (for both primary and secondary eta blocks). So for example if etaBlocks was x,y,z then setting "startEtaStep 0" would require x<|eta| for the plots and if "startEtaStep 1" would require y<|eta| for the plots.
7. ** The number of eta steps to take from the starting eta step. (default is 1, example in config script "nEtaStepsToTake 1"). This is the upper cut on eta like "startEtaStep" is the lower cut. It counts out beggining at startEtaStep. So for example if etaBlocks was x,y,z and "startEtaStep 0" then if "nEtaStepsToTake 1" you would have x<|eta|<y but if "nEtaStepsToTake 2" then you would have x<|eta|<z. Be careful that nEtaStepsToTake does not take you past the end of etaBlocks. IE in the previous example if "nEtaStepsToTake 3" would cause the script to crash because there is no etaBlock 3 steps after x.
8. ** The number of eta steps to take from the starting eta step for secondary etablocks (default is 1, example in config script "nEtaStepsToTakeSecondary 1"). This is the same as the one for primary eta blocks (nEtaStepsToTake) except for secondary eta blocks.
9. * What the maximum number of primary vertex can be in the A distribution histogram (default is 30, example in config script "maxNPV 45")
10. * What the minimum number of primary vertex can be in the A distribution histogram (default is 0, example in config script "minNPV 5")
11. * What the maximum number of <mu> can be in the A distribution histogram (default is 35, example in config script "maxMu 45")
12. * What the maximum number of <mu> can be in the A distribution histogram (default is 0, example in config script "minMu 5")	
13. * The number of toys to produce (default is 1, example in config script is "nToys 10"). This is the number of toys produced for each plot from SavePlots. 
14. * How the toys are to be produced (default is 0, example in config script is "toyType 0"). There are currently only 2 methods (0 and 1), 0 is the recommended method and description of the method can be found further down. This option and how the script was written was with the possibly of someone wanting to add additional methods and making it as simple as possible to do.
15. * What range (in MeV) should be used in the A distribution histograms (default is 20000,example in config script "range 30000"). The range of the A histrograms will be from -range to range.
16. * How many bins to use in the A distribution histograms (default is 1000, example in config script "nBinsForA 2000").
17. The lower bound range (in GeV) for the histogram of the noise term (width of A) (default is 0, example in config script "rangeLowerNoise 0")
18. The upper bound range (in GeV) for the histogram of the noise term (width of A) (default is 10, example in config script "rangeUpperNoise	10")
19. The number of bins to use for the histogram of the noise term (default is 500, example in config script "nBinsForNoise 500")
20. Whether or not to produce toys for the noise term as a function of eta (default is false, example in config script "doEtaToys true")
21. Whether or not to produce toys for the noise term as a function of <mu> with no restriction on NPV (default is false, example in config script "doMuNPVequalsAllToys true")
22. Whether or not to produce toys for the noise term as a function of <mu> with NPV restricted to a single value (default is false, example in config script "doMuNPVFixedToys true")
23. Whether or not to produce toys for the noise term as a function of NPV (default is false, example in config script "doNPVToys true")

Producing the toys can take a while hence the reason for the existence of the last 4 config options if one only cares about errors on a particular type of plot. Setting all 4 options to false obviously means the script won't do anything.

For ToyTest2Result.cxx lets assume the config script is called "ToyTest2Results.config". Most, but not all, of the config options from ToyTest2Condor.config need to be in ToyTest2Results.config. So just copy make sure you copy all lines from ToyTest2Condor.config into ToyTest2Results.config.
There are also the following additional options for ToyTest2Result.cxx:

1. The output directory to save the plots in (default is blah, example in config script is "outPutDir /share/home/khildebr/qualTask/plots/outhistV201Errors_scale/") This directory must be created before running the script. If it isn't the script will complain that it doesn't exist. If plots of the same name already exist in the directory they will be overwritten.
2. The input file that contains the resulting noise terms from the toys produced by ToyTest2Condor.cxx (default is blah, example in config script "OutputFileToyNoise /atlas/uct3/data/users/khildebr/RandomConeV201MCweighted/tempScale/allHistosAddedToys.root")
3. The output file to save the histograms into (default is blah, example in config script "OutputFile2 outHistTestRemoveToyV201ErrorsAdded_scale.root") this just saves the histograms that are plotted in a root file.
4. What method should be use to get the error from the toys (default is 1, example in config script "errorType 1").There are currently only 2 methods (0 and 1). 1 is recommended and description of the method can be found further down. 


## How to run this package:

ToyTest2Condor.cxx as its name suggests was intended to be ran on Condor batch system. It can however be ran locally or on other batch systems. I will not go into detail how to run on Condor but have included two scripts that I used to do this.
Thea condor submit scripts gets 20 jobs running each producing 10 toys in parrallel which when done running can be added together so that 200 toys are produced for each plot.

ToyTest2Result.cxx was intended to be run locally. The reason for having this package broken into 2 scripts instead of just 1 is for being able to produce toys in parrellel running jobs in batch.

ToyTest2Condor.cxx takes two input. The first is the full path to the config file, and the second is the name you want for the output root file.
ToyTest2Result.cxx takes just one input. The full path to the config file.

Assuming you have the two config scripts setup as desired this package is simply run (locally) by running these commands:

```
root -b -q 'AtlasStyle.C' 'ToyTest2Condor.cxx("'ToyTest2Condor.config'","'outputTest.root'")'
```

And if you ran `ToyTest2Condor.cxx` multiple times make sure you hadd all the root files together for `ToyTest2Result.cxx`

Once this is finished running you then run the command:
root -b -q 'AtlasStyle.C' 'ToyTest2Result.cxx("ToyTest2Results.config")'

## More details on ToyTest2Condor.cxx and how the toys are produced:

ToyTest2Condor.cxx takes all the A-distribution histograms from the SavePlots package and produces toys from them. How the toys is produced is dependent on what one is chosen in the config file. For the recommended method (0) 
the toy is generated as follows:

A random number, r, is generated from a Poisson distribution with mean N. Where N is the number of events in the A distribution. Then a toy is constructed by taking r events from the A distribution. 

For method (1) toys are generated by just doing a Gaussian fluctuation on each bin of the A distribution.

The noise term of each toy is calculated and then plotted in a histogram and saved.

## More details on ToyTest2Result.cxx and how the errors are determined:

ToyTest2Result.cxx takes the histograms of all toys noise terms and extracts the error and a single value for the noise term and produces the same plots as the SavePlots package but with error bars.

How the error is extracted depends on what one is chosen in the config file. For the recommended method (1) the error is taken as the rms of the distribution of noise terms of the toys and the mean of the distribution is taken as the noise term.
For method (0) a gaussian is fitted to the distribution of the noise terms and the mean and sigma of the gaussian are taken as the noise term and error, respectively. The distribution of noise terms, however, tends to be very sharp and the gaussian does not fit it well,
so it is not recommended. This may be due to not using enough toys.

