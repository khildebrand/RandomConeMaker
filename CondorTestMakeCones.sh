#!/bin/bash
echo "STARTING <<<<<<<<<<<<<<<<<<<<<<<"
echo `date`
pwd
echo "Host is ..."
hostname

# Unpack code tarball
echo "UNPACKING <<<<<<<<<<<<<<<<<<<<<<<"
echo `date`
pwd
tar -xvf makeCones.tar
pwd
cd makeCones

echo "SETUP ROOT <<<<<<<<<<<<<<<<<<<<<<<"
echo `date`
pwd
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export ALRB_localConfigDir=$HOME/localConfig
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet
localSetupROOT

root -b -q 'AtlasStyle.C' 'makeCones.cxx("'${3}'","'${1}'")'

echo ${1} ${2}
xrdcp ${1} ${2}
#cp ${1} ${2}
pwd
ls


# Clean up worker so no files are transferred back
echo "CLEANING <<<<<<<<<<<<<<<<<<<<<<<"
echo `date`
cd ..
pwd
ls
rm -rf makeCones
pwd
ls


