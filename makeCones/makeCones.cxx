//#include "TMath.h"
//#include "TRandom1.h"

int counterBad =0;
int counterGood =0;
void ATLASLabel(Double_t x,Double_t y,const char* text,double tsize=.08,Color_t color = 1);
void ATLASLabel(Double_t x,Double_t y,const char* text,double tsize,Color_t color)
{
  TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize);
  l.SetTextSize(tsize);
  l.SetNDC();
  l.SetTextFont(72);
  l.SetTextColor(color);

  double delx = 0.115*696*gPad->GetWh()/(472*gPad->GetWw());

  l.DrawLatex(x,y,"ATLAS");
  if (text) {
    TLatex p;
    p.SetTextSize(tsize);
    p.SetNDC();
    p.SetTextFont(42);
    p.SetTextColor(color);
    p.DrawLatex(x+delx,y,text);
  }
}
void ATLASLabel2(Double_t x,Double_t y,const char* text,double tsize=.08,Color_t color = 1);
void ATLASLabel2(Double_t x,Double_t y,const char* text,double tsize,Color_t color)
{
  TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize);
  l.SetTextSize(tsize);
  l.SetNDC();
  l.SetTextFont(72);
  l.SetTextColor(color);

  double delx = .7*0.115*696*gPad->GetWh()/(472*gPad->GetWw());

  l.DrawLatex(x,y,"ATLAS");
  if (text) {
    TLatex p;
    p.SetTextSize(tsize);
    p.SetNDC();
    p.SetTextFont(42);
    p.SetTextColor(color);
    p.DrawLatex(x+delx,y,text);
  }
}
void ATLASLabel3(Double_t x,Double_t y,const char* text,double labelOffset=1.,double tsize=.08,Color_t color = 1);
void ATLASLabel3(Double_t x,Double_t y,const char* text,double labelOffset,double tsize,Color_t color)
{
  TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize);
  l.SetTextSize(tsize);
  l.SetNDC();
  l.SetTextFont(72);
  l.SetTextColor(color);

  double delx = labelOffset*0.115*696*gPad->GetWh()/(472*gPad->GetWw());

  l.DrawLatex(x,y,"ATLAS");
  if (text) {
    TLatex p;
    p.SetTextSize(tsize);
    p.SetNDC();
    p.SetTextFont(42);
    p.SetTextColor(color);
    p.DrawLatex(x+delx,y,text);
  }
}


Double_t calculateDifference3Sym(vector<float>* tCaloCluster_pt,vector<float>* tCaloCluster_e,vector<float>* tCaloCluster_m,vector<float>* tCaloCluster_phi,vector<float>* tCaloCluster_eta,Double_t lowerEta,Double_t upperEta,Double_t RCone,int maxTries,bool extraHist,TH1D* histoEta1,TH1D* histoEta2,TH1D* histoPhi1,TH1D* histoPhi2);
Double_t calculateDifference3Sym(vector<float>* tCaloCluster_pt,vector<float>* tCaloCluster_e,vector<float>* tCaloCluster_m,vector<float>* tCaloCluster_phi,vector<float>* tCaloCluster_eta,Double_t lowerEta,Double_t upperEta,Double_t RCone,int maxTries=1,bool extraHist=false,TH1D* histoEta1=NULL,TH1D* histoEta2=NULL,TH1D* histoPhi1=NULL,TH1D* histoPhi2=NULL) {
	TRandom *r1=new TRandom1();
	r1->SetSeed(0);
	bool flagSomethingInCone1=false;
	bool flagSomethingInCone2=false;

	Double_t pt1=0;
	Double_t pt2=0;
	TLorentzVector clusterVectorE;
	TLorentzVector clusterVectorM;

	TLorentzVector ConeSum1;
	TLorentzVector ConeSum2;


	TLorentzVector rndmCone1;
	TLorentzVector rndmCone2;

	Double_t eta1;
	Double_t eta2;
	Double_t phi1;
	Double_t phi2;

	int numberOfTries=0;
	while ((flagSomethingInCone1==false||flagSomethingInCone2==false)&&numberOfTries<maxTries) {
		numberOfTries++;
		Double_t randomNumber=r1->Rndm();
		Double_t rangePhi=TMath::Pi()*2.;
		Double_t phi1=-TMath::Pi()+randomNumber*rangePhi;
		Double_t phi2;
		if (phi1>0) {
			phi2= phi1-TMath::Pi();
		} else {
			phi2=phi1+TMath::Pi();
		}

		//cout << "phi1="<< phi1 << " phi2="<<phi2<<endl;
		randomNumber=r1->Rndm();
		Double_t eta1;
		Double_t eta2;
		Double_t rangeEta=(upperEta-lowerEta)*2;
		if (randomNumber<.5) {
			eta1=-upperEta+randomNumber*rangeEta;
		} else {
			eta1=lowerEta+(randomNumber-.5)*rangeEta;
		}

		randomNumber=r1->Rndm();
		if (randomNumber<.5) {
			eta2=-upperEta+randomNumber*rangeEta;
		} else {
			eta2=lowerEta+(randomNumber-.5)*rangeEta;
		}

		rndmCone1.SetPtEtaPhiE(300,eta1,phi1,100);
		//rndmCone1b.SetPtEtaPhiE(4000.,eta1,phi1,3000.);
		rndmCone2.SetPtEtaPhiE(1000,eta2,phi2,3000);
		//cout <<"rndmCone1: "<< "PseudoRapidity=" << 	rndmCone1.PseudoRapidity()<< " Rapidity="<<rndmCone1.Rapidity() << " Phi=" <<rndmCone1.Phi()<<" Px="<<rndmCone1.Px()<<" Py="<<rndmCone1.Py()<<" Pz="<<rndmCone1.Pz()<<" E="<<rndmCone1.E()<<" M="<<rndmCone1.M()<<endl;
		//cout <<"rndmCone1b: "<< "PseudoRapidity=" << 	rndmCone1b.PseudoRapidity()<< " Rapidity="<<rndmCone1b.Rapidity() << " Phi=" <<rndmCone1b.Phi()<<" Px="<<rndmCone1b.Px()<<" Py="<<rndmCone1b.Py()<<" Pz="<<rndmCone1b.Pz()<<" E="<<rndmCone1b.E()<<" M="<<rndmCone1b.M()<<endl;
		//cout <<"rndmCone2: "<< "PseudoRapidity=" << 	rndmCone2.PseudoRapidity()<< " Rapidity="<<rndmCone2.Rapidity() << " Phi=" <<rndmCone2.Phi()<<" Px="<<rndmCone2.Px()<<" Py="<<rndmCone2.Py()<<" Pz="<<rndmCone2.Pz()<<" E="<<rndmCone2.E()<<" M="<<rndmCone2.M()<<endl;

		if (extraHist==true) {
			histoPhi1->Fill(phi1);
			histoPhi2->Fill(phi2);
			histoEta1->Fill(eta1);
			histoEta2->Fill(eta2);

		}


		ConeSum1.SetXYZT(0,0,0,0);
		ConeSum2.SetXYZT(0,0,0,0);

		flagSomethingInCone1=false;
		flagSomethingInCone2=false;
		bool inBothCones=false;
		bool tempInCone1=false;
		bool tempInCone2=false;
		for (int i=0;i<tCaloCluster_pt->size();i++) {
			clusterVectorE.SetPtEtaPhiE(tCaloCluster_pt->at(i),tCaloCluster_eta->at(i),tCaloCluster_phi->at(i),tCaloCluster_e->at(i));
			tempInCone1=false;
			tempInCone2=false;
			if (TMath::Abs(rndmCone1.DeltaR(clusterVectorE))<RCone) {
				ConeSum1=ConeSum1+clusterVectorE;
				flagSomethingInCone1=true;
				tempInCone1=true;

			}

			if (TMath::Abs(rndmCone2.DeltaR(clusterVectorE))<RCone) {
				ConeSum2=ConeSum2+clusterVectorE;
				flagSomethingInCone2=true;
				tempInCone2=true;

			}
			if (tempInCone1==true&&tempInCone2==true) {
				cout <<"In both cones"<<endl;
			}

		}
	}
	if (numberOfTries<=maxTries&&flagSomethingInCone1==true&&flagSomethingInCone2==true) {
		return (ConeSum1.Pt()-ConeSum2.Pt());

	} else {
		return 3000000.;
	}

	//return 3000000;
}

void makeCones(const std::string& inputFilename,const std::string& outputFilename) {
	//TEnv *config = new TEnv("/share/home/khildebr/qualTask/run/makePlotsRanCone4.config");
	TEnv *config = new TEnv("makeCones.config");
	if ( !config ) {
		printf("configFile=makeCones.config doesn't appear to exists\n");
	}
	std::string treeName = config->GetValue("treeName"     ,     "CaloCalTopoClusters");
	bool isMC = config->GetValue("isMC",false);
	bool muScale = config->GetValue("muScale",false);
	double muScaleFactor= config->GetValue("muScaleFactor",1.0);
	double coneRadius= config->GetValue("coneRadius",0.4);
	char coneRadiusCharBuff[15];
	sprintf(coneRadiusCharBuff,"%.1f",coneRadius);
	bool doSecondary = config->GetValue("doSecondary",false);
	int maxNPV = config->GetValue("maxNPV",30);
	int maxMu = config->GetValue("maxMu",35);
	int minNPV = config->GetValue("minNPV",0);
	int minMu = config->GetValue("minMu",0);
	int nBinsForA = config->GetValue("nBinsForA",1000);
	int range = config->GetValue("range",20000);
	cout<<"maxNPV= "<<maxNPV<<endl;
	cout<<"minNPV= "<<minNPV<<endl;
	cout<<"maxMu= "<<maxMu<<endl;
	cout<<"minMu= "<<minMu<<endl;
	cout<<"range= "<<range<<endl;
	cout<<"isMC= "<<isMC<<endl;

	std::string etaBlocksString= config->GetValue("etaBlocks","0,.8");
	std::string etaBlocksSecondaryString= config->GetValue("etaBlocksSecondary","0,2.1");

	std::string temp;

	int nEtaSteps=-1;
	printf("etaBlocksString=%s\n",etaBlocksString.c_str());
	stringstream ss(etaBlocksString);
	vector<double> etaBlocks;
	while(getline(ss,temp,',')) {
		etaBlocks.push_back(std::stod(temp));
		nEtaSteps++;
	}
	for (int i=0;i<etaBlocks.size();i++) {
		printf("etaBlocks[%i]=%f\n",i,etaBlocks[i]);
	}
	int nEtaStepsSecondary=-1;
	printf("etaBlocksSecondaryString=%s\n",etaBlocksSecondaryString.c_str());
	stringstream ss2(etaBlocksSecondaryString);
	vector<double> etaBlocksSecondary;
	while(getline(ss2,temp,',')) {
		etaBlocksSecondary.push_back(std::stod(temp));
		nEtaStepsSecondary++;
	}
	for (int i=0;i<etaBlocksSecondary.size();i++) {
		printf("etaBlocksSecondary[%i]=%f\n",i,etaBlocksSecondary[i]);
	}
	SetAtlasStyle();
	gStyle->SetHistLineWidth(1.0);
	gStyle->SetMarkerSize(1.0);
  	gStyle->SetPalette(1);

	TFile *inFile=new TFile((inputFilename).c_str());
	TTree *inTree=(TTree*)inFile->Get(treeName.c_str());

	vector<float>* tCaloCluster_EM_rapidity=new vector<float>();
	vector<float>* tCaloCluster_EM_pt=new vector<float>();
	vector<float>* tCaloCluster_EM_e=new vector<float>();
	vector<float>* tCaloCluster_EM_et=new vector<float>();
	vector<float>* tCaloCluster_EM_phi=new vector<float>();
	vector<float>* tCaloCluster_EM_eta=new vector<float>();
	vector<float>* tCaloCluster_EM_m=new vector<float>();

	vector<float>* tCaloCluster_LC_rapidity=new vector<float>();
	vector<float>* tCaloCluster_LC_pt=new vector<float>();
	vector<float>* tCaloCluster_LC_e=new vector<float>();
	vector<float>* tCaloCluster_LC_et=new vector<float>();
	vector<float>* tCaloCluster_LC_phi=new vector<float>();
	vector<float>* tCaloCluster_LC_eta=new vector<float>();
	vector<float>* tCaloCluster_LC_m=new vector<float>();

	int tCaloCluster_n;
	int tNPV_n;
	float taverageMu;
	float tcorrectedAverageMu;
	float tEventWeight;
	float tPileupWeight;


	inTree->SetBranchAddress("CaloCalTopoClusters_EM_pt",&tCaloCluster_EM_pt);
	inTree->SetBranchAddress("CaloCalTopoClusters_EM_rapidity",&tCaloCluster_EM_rapidity);
	inTree->SetBranchAddress("CaloCalTopoClusters_EM_e",&tCaloCluster_EM_e);
	inTree->SetBranchAddress("CaloCalTopoClusters_EM_et",&tCaloCluster_EM_et);
	inTree->SetBranchAddress("CaloCalTopoClusters_EM_phi",&tCaloCluster_EM_phi);
	inTree->SetBranchAddress("CaloCalTopoClusters_EM_eta",&tCaloCluster_EM_eta);
	inTree->SetBranchAddress("CaloCalTopoClusters_EM_m",&tCaloCluster_EM_m);

	inTree->SetBranchAddress("CaloCalTopoClusters_LC_pt",&tCaloCluster_LC_pt);
	inTree->SetBranchAddress("CaloCalTopoClusters_LC_rapidity",&tCaloCluster_LC_rapidity);
	inTree->SetBranchAddress("CaloCalTopoClusters_LC_e",&tCaloCluster_LC_e);
	inTree->SetBranchAddress("CaloCalTopoClusters_LC_et",&tCaloCluster_LC_et);
	inTree->SetBranchAddress("CaloCalTopoClusters_LC_phi",&tCaloCluster_LC_phi);
	inTree->SetBranchAddress("CaloCalTopoClusters_LC_eta",&tCaloCluster_LC_eta);
	inTree->SetBranchAddress("CaloCalTopoClusters_LC_m",&tCaloCluster_LC_m);

	inTree->SetBranchAddress("CaloCalTopoClusters_n",&tCaloCluster_n);

	inTree->SetBranchAddress("PrimaryVertices_n",&tNPV_n);

	inTree->SetBranchAddress("MCEventWeight",&tEventWeight);
	inTree->SetBranchAddress("PileupWeight",&tPileupWeight);
	inTree->SetBranchAddress("averageMu",&taverageMu);
	inTree->SetBranchAddress("correctedAverageMu",&tcorrectedAverageMu);

	int nEntries = inTree->GetEntries(); // Get the number of entries in this tree
	//cout<<"before entering for loop"<<endl;
	cout<<"nEntries="<<nEntries<<endl;
	nEntries=500;
	cout<<"inputFilename="<<inputFilename<<endl;

	TH3D *R4ConeEM_etaSlicesPos[nEtaSteps];

	TH3D *R4ConeLC_etaSlicesPos[nEtaSteps];

	TH3D *R4ConeEM_etaSlicesSecondary[nEtaStepsSecondary];

	TH3D *R4ConeLC_etaSlicesSecondary[nEtaStepsSecondary];

	TH1D *averageMuHisto=new TH1D("averageMuHisto","averageMuHisto",maxMu-minMu,minMu,maxMu);
	TH1D *correctedAverageMuHisto=new TH1D("correctedAverageMuHisto","correctedAverageMuHisto",maxMu-minMu,minMu,maxMu);
	TH1D *NPVHisto=new TH1D("NPVHisto","NPVHisto",maxNPV-minNPV,minNPV,maxNPV);
	TH1D *emptyConesSkippedHistoEM[nEtaSteps];
	TH1D *emptyConesSkippedHistoLC[nEtaSteps];
	for (int x=0;x<nEtaSteps;x++) {
		R4ConeEM_etaSlicesPos[x]=new TH3D(("R4ConeEM_etaSlicesPos"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),("R="+string(coneRadiusCharBuff)+" EM Scale").c_str(),nBinsForA,-range,range,maxNPV-minNPV,minNPV,maxNPV,maxMu-minMu,minMu,maxMu);

		R4ConeLC_etaSlicesPos[x]=new TH3D(("R4ConeLC_etaSlicesPos"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),("R="+string(coneRadiusCharBuff)+" LC Scale").c_str(),nBinsForA,-range,range,maxNPV-minNPV,minNPV,maxNPV,maxMu-minMu,minMu,maxMu);

		emptyConesSkippedHistoEM[x]=new TH1D(("emptyConesSkippedHistoEM"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),"emptyConesSkippedHistoEM",2,0,2);
		emptyConesSkippedHistoLC[x]=new TH1D(("emptyConesSkippedHistoLC"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),"emptyConesSkippedHistoLC",2,0,2);

	}
	if (doSecondary==true) {
		for (int x=0;x<nEtaStepsSecondary;x++) {
			R4ConeEM_etaSlicesSecondary[x]=new TH3D(("R4ConeEM_etaSlicesSecondary"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),("R="+string(coneRadiusCharBuff)+" EM Scale").c_str(),nBinsForA,-range,range,maxNPV-minNPV,minNPV,maxNPV,maxMu-minMu,minMu,maxMu);

			R4ConeLC_etaSlicesSecondary[x]=new TH3D(("R4ConeLC_etaSlicesSecondary"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),("R="+string(coneRadiusCharBuff)+" LC Scale").c_str(),nBinsForA,-range,range,maxNPV-minNPV,minNPV,maxNPV,maxMu-minMu,minMu,maxMu);

		}
	}
	Double_t tempDifference=0;
	Double_t weight=1.;

	for (int iEnt = 0; iEnt < nEntries; iEnt++) {

		//cout<<"after entering for loop"<<endl;
		inTree->GetEntry(iEnt); // Gets the next entry (filling the linked variables)
		if (iEnt%500==0) {
			cout<<"Finished processing "<<iEnt<< " events!!!"<<endl;
		}

		if (isMC) {
			weight=tEventWeight*tPileupWeight;
		}
		averageMuHisto->Fill(taverageMu,weight);
		if (muScale) {
			correctedAverageMuHisto->Fill(tcorrectedAverageMu/muScaleFactor,weight);
		} else {
			correctedAverageMuHisto->Fill(tcorrectedAverageMu,weight);
		}
		NPVHisto->Fill(tNPV_n,weight);


		for (int x=0;x<nEtaSteps;x++) {

			tempDifference=calculateDifference3Sym(tCaloCluster_EM_pt,tCaloCluster_EM_e,tCaloCluster_EM_m,tCaloCluster_EM_phi,tCaloCluster_EM_eta,etaBlocks[x],etaBlocks[x+1],coneRadius,4);
			if (tempDifference <2500000) {
				emptyConesSkippedHistoEM[x]->Fill(0);
				if (tNPV_n<maxNPV&&tcorrectedAverageMu<maxMu&&tcorrectedAverageMu>=minMu&&tNPV_n>=minNPV) {
					if (muScale) {
						R4ConeEM_etaSlicesPos[x]->Fill(tempDifference,tNPV_n,tcorrectedAverageMu/muScaleFactor,weight);
					} else {
						R4ConeEM_etaSlicesPos[x]->Fill(tempDifference,tNPV_n,tcorrectedAverageMu,weight);
					}

				} else {
					cout <<"				NPV="<<tNPV_n<<"  Mu="<< tcorrectedAverageMu<<endl;
				}
			} else {
				emptyConesSkippedHistoEM[x]->Fill(1);
			}
			tempDifference=calculateDifference3Sym(tCaloCluster_LC_pt,tCaloCluster_LC_e,tCaloCluster_LC_m,tCaloCluster_LC_phi,tCaloCluster_LC_eta,etaBlocks[x],etaBlocks[x+1],coneRadius,4);
			if (tempDifference <2500000) {
				emptyConesSkippedHistoLC[x]->Fill(0);
				if (tNPV_n<maxNPV&&tcorrectedAverageMu<maxMu&&tcorrectedAverageMu>=minMu&&tNPV_n>=minNPV) {
					if (muScale) {
						R4ConeLC_etaSlicesPos[x]->Fill(tempDifference,tNPV_n,tcorrectedAverageMu/muScaleFactor,weight);
					} else {
						R4ConeLC_etaSlicesPos[x]->Fill(tempDifference,tNPV_n,tcorrectedAverageMu,weight);
					}
				} else {
					cout <<"				NPV="<<tNPV_n<<"  Mu="<< tcorrectedAverageMu<<endl;
				}
			} else {
				emptyConesSkippedHistoLC[x]->Fill(1);
			}

		}
		if (doSecondary==true) {
			for (int x=0;x<nEtaStepsSecondary;x++) {
				tempDifference=calculateDifference3Sym(tCaloCluster_EM_pt,tCaloCluster_EM_e,tCaloCluster_EM_m,tCaloCluster_EM_phi,tCaloCluster_EM_eta,etaBlocksSecondary[x],etaBlocksSecondary[x+1],coneRadius,2);
				if (tempDifference <2500000) {
					if (tNPV_n<maxNPV&&tcorrectedAverageMu<maxMu&&tcorrectedAverageMu>=minMu&&tNPV_n>=minNPV) {
						if (muScale) {
							R4ConeEM_etaSlicesSecondary[x]->Fill(tempDifference,tNPV_n,tcorrectedAverageMu/muScaleFactor,weight);
						} else {
							R4ConeEM_etaSlicesSecondary[x]->Fill(tempDifference,tNPV_n,tcorrectedAverageMu,weight);
						}

					} else {
						cout <<"				NPV="<<tNPV_n<<"  Mu="<< tcorrectedAverageMu<<endl;
					}
				}
				tempDifference=calculateDifference3Sym(tCaloCluster_LC_pt,tCaloCluster_LC_e,tCaloCluster_LC_m,tCaloCluster_LC_phi,tCaloCluster_LC_eta,etaBlocksSecondary[x],etaBlocksSecondary[x+1],coneRadius,2);
				if (tempDifference <2500000) {
					if (tNPV_n<maxNPV&&tcorrectedAverageMu<maxMu&&tcorrectedAverageMu>=minMu&&tNPV_n>=minNPV) {
						if (muScale) {
							R4ConeLC_etaSlicesSecondary[x]->Fill(tempDifference,tNPV_n,tcorrectedAverageMu/muScaleFactor,weight);
						} else {
							R4ConeLC_etaSlicesSecondary[x]->Fill(tempDifference,tNPV_n,tcorrectedAverageMu,weight);
						}
					} else {
						cout <<"				NPV="<<tNPV_n<<"  Mu="<< tcorrectedAverageMu<<endl;
					}
				}

			}
		}


	}

	TFile *outFile=new TFile((outputFilename).c_str(),"RECREATE");
	for (int x=0;x<nEtaSteps;x++) {
		R4ConeEM_etaSlicesPos[x]->Write();
		R4ConeLC_etaSlicesPos[x]->Write();
		emptyConesSkippedHistoEM[x]->Write();
		emptyConesSkippedHistoLC[x]->Write();

	}
	averageMuHisto->Write();
	correctedAverageMuHisto->Write();
	NPVHisto->Write();
	if (doSecondary==true) {
		for (int x=0;x<nEtaStepsSecondary;x++) {
			R4ConeEM_etaSlicesSecondary[x]->Write();
			R4ConeLC_etaSlicesSecondary[x]->Write();
		}
	}

	outFile->Close();


}