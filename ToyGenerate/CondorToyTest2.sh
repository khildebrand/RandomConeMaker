#!/bin/bash
echo "STARTING <<<<<<<<<<<<<<<<<<<<<<<"
echo `date`
pwd
echo "Host is ..."
hostname

# Unpack code tarball
echo "UNPACKING <<<<<<<<<<<<<<<<<<<<<<<"
echo `date`
pwd
tar -xvf packagesToys.tar
pwd
cd packagesToys

echo "SETUP ROOT <<<<<<<<<<<<<<<<<<<<<<<"
echo `date`
pwd
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export ALRB_localConfigDir=$HOME/localConfig
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet
localSetupROOT

root -b -q 'AtlasStyle.C' 'ToyTest2Condor.cxx("'ToyTest2Condor.config'","'${1}'")'

echo ${1}.root ${2}.root
xrdcp ${1}.root ${2}.root
pwd
ls


# Clean up worker so no files are transferred back
echo "CLEANING <<<<<<<<<<<<<<<<<<<<<<<"
echo `date`
cd ..
pwd
ls
rm -rf packagesToys
pwd
ls


