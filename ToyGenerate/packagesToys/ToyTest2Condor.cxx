bool debug=false;


Double_t getWidth(TH1D* histo) {
	int mean_bin = histo->FindBin(histo->GetMean());
	if (debug==true) {
		cout<< "mean_bin="<<mean_bin<<" mean="<<histo->GetMean()<<endl;
	}

    int binup = mean_bin;
    int bindown = mean_bin-1;
    Double_t total = histo->Integral();
    Double_t runningup = 0;
    Double_t runningdown = 0;
    if (total<50) {
		return -10.;
	}
    while (runningup < 0.6827/2.0*total) {
        runningup += histo->GetBinContent(binup);
        binup += 1;
	}
    while (runningdown < 0.6827/2.0*total) {
        runningdown += histo->GetBinContent(bindown);
        bindown -= 1;
	}

    Double_t width = (histo->GetBinCenter(binup)-histo->GetBinCenter(bindown))/2.;
    return width/1000.;
}
void generateToy(TH1D* histoReal,TH1D* histoToy,int toyType) {
	TRandom *r1=new TRandom1();
	r1->SetSeed(0);
	if (toyType==0) {
		int numberOfEventsToFill=r1->Poisson(histoReal->GetEntries());
		if (debug) {
			cout<<"number of Events in real histo="<<histoReal->GetEntries()<<endl;
			cout<<"numberOfEventsToFill="<<numberOfEventsToFill<<endl;
		}
		for (int x=0;x<numberOfEventsToFill;x++) {
			histoToy->Fill(histoReal->GetRandom());
		}
	} else if (toyType==1) {
		//idiot check
		if (histoReal->GetNbinsX()!=histoToy->GetNbinsX()) {
			cout <<"AHHHH!!!!!!!!!!!!!!!!!!!!!!!!!!!!! The toy and real histograms dont have the same number of bins!!!"<<endl;
		}
		for (int bin=1;bin<=histoReal->GetNbinsX();bin++) {
			Double_t value = histoReal->GetBinContent(bin);
			Double_t error = histoReal->GetBinError(bin);
			histoToy->SetBinContent(bin,r1->Gaus(value,error));
		}
	}

}
void ToyTest2Condor(const std::string& configFile,const std::string& OutputFileToyNoise) {
	TEnv *config = new TEnv(configFile.c_str());
	if ( !config ) {
		printf("configFile=%s doesn't appear to exists\n",configFile.c_str());
	}
	std::string ToyTestInputFile = config->GetValue("ToyTestInputFile","blah");
	bool doData = config->GetValue("doData",true);
	bool doMC = config->GetValue("doMC",true);
	std::string etaBlocksString= config->GetValue("etaBlocks","0,.8");
	bool doSecondary = config->GetValue("doSecondary",false);
	std::string etaBlocksSecondaryString= config->GetValue("etaBlocksSecondary","0,2.1");
	int startEtaStep = config->GetValue("startEtaStep",0);
	int nEtaStepsToTake = config->GetValue("nEtaStepsToTake",1);
	int nEtaStepsToTakeSecondary = config->GetValue("nEtaStepsToTakeSecondary",1);
	int maxNPV = config->GetValue("maxNPV",30);
	int maxMu = config->GetValue("maxMu",35);
	int minNPV = config->GetValue("minNPV",0);
	int minMu = config->GetValue("minMu",0);
	int nToys = config->GetValue("nToys",1);
	int toyType = config->GetValue("toyType",0);
	int nBinsForA = config->GetValue("nBinsForA",1000);
	int range = config->GetValue("range",20000);
	int nBinsForNoise = config->GetValue("nBinsForNoise",500);
	int rangeLowerNoise = config->GetValue("rangeLowerNoise",0);
	int rangeUpperNoise = config->GetValue("rangeUpperNoise",10);
	bool doEtaToys = config->GetValue("doEtaToys",false);
	bool doMuNPVequalsAllToys = config->GetValue("doMuNPVequalsAllToys",false);
	bool doMuNPVFixedToys = config->GetValue("doMuNPVFixedToys",false);
	bool doNPVToys = config->GetValue("doNPVToys",false);
	debug = config->GetValue("debug",false);



	std::string temp;

	int nEtaSteps=-1;
	printf("etaBlocksString=%s\n",etaBlocksString.c_str());
	stringstream ss(etaBlocksString);
	vector<double> etaBlocks;
	while(getline(ss,temp,',')) {
		etaBlocks.push_back(std::stod(temp));
		nEtaSteps++;
	}
	for (int i=0;i<etaBlocks.size();i++) {
		printf("etaBlocks[%i]=%f\n",i,etaBlocks[i]);
	}
	int nEtaStepsSecondary=-1;
	printf("etaBlocksSecondaryString=%s\n",etaBlocksSecondaryString.c_str());
	stringstream ss2(etaBlocksSecondaryString);
	vector<double> etaBlocksSecondary;
	while(getline(ss2,temp,',')) {
		etaBlocksSecondary.push_back(std::stod(temp));
		nEtaStepsSecondary++;
	}
	for (int i=0;i<etaBlocksSecondary.size();i++) {
		printf("etaBlocksSecondary[%i]=%f\n",i,etaBlocksSecondary[i]);
	}

	SetAtlasStyle();

	int histNameInt=0;
	//int nBinsForA=1000;
	//int range=20000;
	//int nBinsForNoise=2000;
	//int rangeLowerNoise=0;
	//int rangeUpperNoise=10;
	//int maxMu=35;
	//int maxNPV=30;

	//these are what hold all the noise terms from each toy
	//as fcn of eta
	TH1D *R4ConeEM_etaSlicesNoise[nEtaSteps];
	TH1D *R4ConeLC_etaSlicesNoise[nEtaSteps];

	TH1D *R4ConeEM_etaSlicesNoiseSecondary[nEtaSteps];
	TH1D *R4ConeLC_etaSlicesNoiseSecondary[nEtaSteps];

	TH1D *R4ConeEM_etaSlicesNoiseMC[nEtaSteps];
	TH1D *R4ConeLC_etaSlicesNoiseMC[nEtaSteps];

	TH1D *R4ConeEM_etaSlicesNoiseSecondaryMC[nEtaSteps];
	TH1D *R4ConeLC_etaSlicesNoiseSecondaryMC[nEtaSteps];

	//as fcn of NPV
	TH1D *R4ConeEM_NoiseAsFcnOfNPV[maxNPV-minNPV];
	TH1D *R4ConeLC_NoiseAsFcnOfNPV[maxNPV-minNPV];

	TH1D *R4ConeEM_NoiseAsFcnOfNPVSecondary[maxNPV-minNPV];
	TH1D *R4ConeLC_NoiseAsFcnOfNPVSecondary[maxNPV-minNPV];

	TH1D *R4ConeEM_NoiseAsFcnOfNPVMC[maxNPV-minNPV];
	TH1D *R4ConeLC_NoiseAsFcnOfNPVMC[maxNPV-minNPV];

	TH1D *R4ConeEM_NoiseAsFcnOfNPVSecondaryMC[maxNPV-minNPV];
	TH1D *R4ConeLC_NoiseAsFcnOfNPVSecondaryMC[maxNPV-minNPV];

	//as fcn of mu with no restriction on NPV
	TH1D *R4ConeEM_NoiseAsFcnOfMuNPVAll[maxMu-minMu];
	TH1D *R4ConeLC_NoiseAsFcnOfMuNPVAll[maxMu-minMu];

	TH1D *R4ConeEM_NoiseAsFcnOfMuNPVAllSecondary[maxMu-minMu];
	TH1D *R4ConeLC_NoiseAsFcnOfMuNPVAllSecondary[maxMu-minMu];

	TH1D *R4ConeEM_NoiseAsFcnOfMuNPVAllMC[maxMu-minMu];
	TH1D *R4ConeLC_NoiseAsFcnOfMuNPVAllMC[maxMu-minMu];

	TH1D *R4ConeEM_NoiseAsFcnOfMuNPVAllSecondaryMC[maxMu-minMu];
	TH1D *R4ConeLC_NoiseAsFcnOfMuNPVAllSecondaryMC[maxMu-minMu];

	//as fcn of mu with NPV restricted to single value
	TH1D *R4ConeEM_NoiseAsFcnOfMuNPVFixed[maxNPV-minNPV][maxMu-minMu];
	TH1D *R4ConeLC_NoiseAsFcnOfMuNPVFixed[maxNPV-minNPV][maxMu-minMu];

	TH1D *R4ConeEM_NoiseAsFcnOfMuNPVFixedSecondary[maxNPV-minNPV][maxMu-minMu];
	TH1D *R4ConeLC_NoiseAsFcnOfMuNPVFixedSecondary[maxNPV-minNPV][maxMu-minMu];

	TH1D *R4ConeEM_NoiseAsFcnOfMuNPVFixedMC[maxNPV-minNPV][maxMu-minMu];
	TH1D *R4ConeLC_NoiseAsFcnOfMuNPVFixedMC[maxNPV-minNPV][maxMu-minMu];

	TH1D *R4ConeEM_NoiseAsFcnOfMuNPVFixedSecondaryMC[maxNPV-minNPV][maxMu-minMu];
	TH1D *R4ConeLC_NoiseAsFcnOfMuNPVFixedSecondaryMC[maxNPV-minNPV][maxMu-minMu];

	//these are the noise term real ones from save plots
	//as fcn of eta
	TH1D* NoiseAsFcnEta4EM;
	TH1D* NoiseAsFcnEta4LC;

	TH1D* NoiseAsFcnEta4EMSecondary;
	TH1D* NoiseAsFcnEta4LCSecondary;

	TH1D* MCNoiseAsFcnEta4EM;
	TH1D* MCNoiseAsFcnEta4LC;

	TH1D* MCNoiseAsFcnEta4EMSecondary;
	TH1D* MCNoiseAsFcnEta4LCSecondary;

	//as fcn NPV
	TH1D *NoiseAsFcnNPV4EM;
	TH1D *NoiseAsFcnNPV4LC;

	TH1D *NoiseAsFcnNPV4EMSecondary;
	TH1D *NoiseAsFcnNPV4LCSecondary;

	TH1D *MCNoiseAsFcnNPV4EM;
	TH1D *MCNoiseAsFcnNPV4LC;

	TH1D *MCNoiseAsFcnNPV4EMSecondary;
	TH1D *MCNoiseAsFcnNPV4LCSecondary;

	//as fcn of mu with no restriction on NPV
	TH1D *NoiseAsFcnMu4EMAll;
	TH1D *NoiseAsFcnMu4LCAll;

	TH1D *NoiseAsFcnMu4EMAllSecondary;
	TH1D *NoiseAsFcnMu4LCAllSecondary;

	TH1D *MCNoiseAsFcnMu4EMAll;
	TH1D *MCNoiseAsFcnMu4LCAll;

	TH1D *MCNoiseAsFcnMu4EMAllSecondary;
	TH1D *MCNoiseAsFcnMu4LCAllSecondary;

	//as fcn of mu with NPV restricted to single value
	TH1D *NoiseAsFcnMu4EMNPV[maxNPV-minNPV];
	TH1D *NoiseAsFcnMu4LCNPV[maxNPV-minNPV];

	TH1D *NoiseAsFcnMu4EMNPVSecondary[maxNPV-minNPV];
	TH1D *NoiseAsFcnMu4LCNPVSecondary[maxNPV-minNPV];

	TH1D *MCNoiseAsFcnMu4EMNPV[maxNPV-minNPV];
	TH1D *MCNoiseAsFcnMu4LCNPV[maxNPV-minNPV];

	TH1D *MCNoiseAsFcnMu4EMNPVSecondary[maxNPV-minNPV];
	TH1D *MCNoiseAsFcnMu4LCNPVSecondary[maxNPV-minNPV];
	cout<<"heyo1a"<<endl;

	//these are the A histrograms from save plots
	TH1D *R4ConeEM_etaSlicesPos[nEtaSteps];
	TH1D *R4ConeLC_etaSlicesPos[nEtaSteps];

	TH1D *R4ConeEM_etaSlicesSecondary[nEtaSteps];
	TH1D *R4ConeLC_etaSlicesSecondary[nEtaSteps];

	TH1D *R4ConeEM_etaSlicesPosMC[nEtaSteps];
	TH1D *R4ConeLC_etaSlicesPosMC[nEtaSteps];

	TH1D *R4ConeEM_etaSlicesSecondaryMC[nEtaSteps];
	TH1D *R4ConeLC_etaSlicesSecondaryMC[nEtaSteps];


	TH1D *R4ConeEM_NPVSlicesPos[maxNPV-minNPV];
	TH1D *R4ConeLC_NPVSlicesPos[maxNPV-minNPV];

	TH1D *R4ConeEM_NPVSlicesSecondary[maxNPV-minNPV];
	TH1D *R4ConeLC_NPVSlicesSecondary[maxNPV-minNPV];

	TH1D *R4ConeEM_NPVSlicesPosMC[maxNPV-minNPV];
	TH1D *R4ConeLC_NPVSlicesPosMC[maxNPV-minNPV];

	TH1D *R4ConeEM_NPVSlicesSecondaryMC[maxNPV-minNPV];
	TH1D *R4ConeLC_NPVSlicesSecondaryMC[maxNPV-minNPV];


	TH1D *R4ConeEM_MuSlicesNPVAll[maxMu-minMu];
	TH1D *R4ConeLC_MuSlicesNPVAll[maxMu-minMu];

	TH1D *R4ConeEM_MuSlicesNPVAllSecondary[maxMu-minMu];
	TH1D *R4ConeLC_MuSlicesNPVAllSecondary[maxMu-minMu];

	TH1D *R4ConeEM_MuSlicesNPVAllMC[maxMu-minMu];
	TH1D *R4ConeLC_MuSlicesNPVAllMC[maxMu-minMu];

	TH1D *R4ConeEM_MuSlicesNPVAllSecondaryMC[maxMu-minMu];
	TH1D *R4ConeLC_MuSlicesNPVAllSecondaryMC[maxMu-minMu];


	TH1D *R4ConeEM_muSlicesPos[maxNPV-minNPV][maxMu-minMu];
	TH1D *R4ConeLC_muSlicesPos[maxNPV-minNPV][maxMu-minMu];

	TH1D *R4ConeEM_muSlicesSecondary[maxNPV-minNPV][maxMu-minMu];
	TH1D *R4ConeLC_muSlicesSecondary[maxNPV-minNPV][maxMu-minMu];

	TH1D *R4ConeEM_muSlicesPosMC[maxNPV-minNPV][maxMu-minMu];
	TH1D *R4ConeLC_muSlicesPosMC[maxNPV-minNPV][maxMu-minMu];

	TH1D *R4ConeEM_muSlicesSecondaryMC[maxNPV-minNPV][maxMu-minMu];
	TH1D *R4ConeLC_muSlicesSecondaryMC[maxNPV-minNPV][maxMu-minMu];

	TFile *infile;
	//this is so that when file is closed histogram is not lost.
	//TH1::AddDirectory(kFALSE);

	//gets the A plots for data
	if (doData) {
		infile = TFile::Open((ToyTestInputFile).c_str());
		cout<<"heyo1"<<endl;
		if (doEtaToys) {
			cout<<"doEtaToys"<<endl;
			for (int x=0;x<nEtaSteps;x++) {
				cout<<x<<endl;
				//cout<<vectorOfRunNumbers[0]<<endl;
				R4ConeEM_etaSlicesPos[x]=(TH1D*)infile->Get(("R4ConeEM_etaSlicesPos"+std::to_string(x)+"_"+std::to_string(x+1)).c_str())->Clone();
				R4ConeEM_etaSlicesPos[x]->SetDirectory(0);

				R4ConeLC_etaSlicesPos[x]=(TH1D*)infile->Get(("R4ConeLC_etaSlicesPos"+std::to_string(x)+"_"+std::to_string(x+1)).c_str())->Clone();
				R4ConeLC_etaSlicesPos[x]->SetDirectory(0);

			}
			if (doSecondary) {
				for (int x=0;x<nEtaStepsSecondary;x++) {
					cout<<x<<endl;
					//cout<<vectorOfRunNumbers[0]<<endl;
					R4ConeEM_etaSlicesSecondary[x]=(TH1D*)infile->Get(("R4ConeEM_etaSlicesSecondary"+std::to_string(x)+"_"+std::to_string(x+1)).c_str())->Clone();
					R4ConeEM_etaSlicesSecondary[x]->SetDirectory(0);

					R4ConeLC_etaSlicesSecondary[x]=(TH1D*)infile->Get(("R4ConeLC_etaSlicesSecondary"+std::to_string(x)+"_"+std::to_string(x+1)).c_str())->Clone();
					R4ConeLC_etaSlicesSecondary[x]->SetDirectory(0);

				}
			}
			//NoiseAsFcnEta4EM=(TH1D*)infile->Get("NoiseAsFcnEta4EM")->Clone();
			//NoiseAsFcnEta4LC=(TH1D*)infile->Get("NoiseAsFcnEta4LC")->Clone();
		}

		if (doNPVToys) {
			cout<<"doNPVToys"<<endl;
			for (int y=minNPV;y<maxNPV;y++) {
				cout<<y<<endl;
				R4ConeEM_NPVSlicesPos[y-minNPV]=(TH1D*)infile->Get(("R4ConeEM_etaSlicesPos"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_NPV"+std::to_string(y)).c_str())->Clone();
				R4ConeEM_NPVSlicesPos[y-minNPV]->SetDirectory(0);
				R4ConeLC_NPVSlicesPos[y-minNPV]=(TH1D*)infile->Get(("R4ConeLC_etaSlicesPos"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_NPV"+std::to_string(y)).c_str())->Clone();
				R4ConeLC_NPVSlicesPos[y-minNPV]->SetDirectory(0);
			}
			if (doSecondary) {
				for (int y=minNPV;y<maxNPV;y++) {
					cout<<y<<endl;
					R4ConeEM_NPVSlicesSecondary[y-minNPV]=(TH1D*)infile->Get(("R4ConeEM_etaSlicesSecondary"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTakeSecondary)+"_NPV"+std::to_string(y)).c_str())->Clone();
					R4ConeEM_NPVSlicesSecondary[y-minNPV]->SetDirectory(0);
					R4ConeLC_NPVSlicesSecondary[y-minNPV]=(TH1D*)infile->Get(("R4ConeLC_etaSlicesSecondary"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTakeSecondary)+"_NPV"+std::to_string(y)).c_str())->Clone();
					R4ConeLC_NPVSlicesSecondary[y-minNPV]->SetDirectory(0);
				}
			}
			//NoiseAsFcnNPV4EM=(TH1D*)infile->Get("NoiseAsFcnNPV4EM")->Clone();
			//NoiseAsFcnNPV4LC=(TH1D*)infile->Get("NoiseAsFcnNPV4LC")->Clone();
		}
		if (doMuNPVequalsAllToys) {
			cout<<"doMuNPVequalsAllToys"<<endl;
			for (int y=minMu;y<maxMu;y++) {
				cout<<y<<endl;
				R4ConeEM_MuSlicesNPVAll[y-minMu]=(TH1D*)infile->Get(("R4ConeEM_etaSlicesPos"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_mu"+std::to_string(y)).c_str())->Clone();
				R4ConeEM_MuSlicesNPVAll[y-minMu]->SetDirectory(0);
				R4ConeLC_MuSlicesNPVAll[y-minMu]=(TH1D*)infile->Get(("R4ConeLC_etaSlicesPos"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_mu"+std::to_string(y)).c_str())->Clone();
				R4ConeLC_MuSlicesNPVAll[y-minMu]->SetDirectory(0);
			}
			if (doSecondary) {
				for (int y=minMu;y<maxMu;y++) {
					cout<<y<<endl;
					R4ConeEM_MuSlicesNPVAllSecondary[y-minMu]=(TH1D*)infile->Get(("R4ConeEM_etaSlicesPos"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTakeSecondary)+"_mu"+std::to_string(y)).c_str())->Clone();
					R4ConeEM_MuSlicesNPVAllSecondary[y-minMu]->SetDirectory(0);
					R4ConeLC_MuSlicesNPVAllSecondary[y-minMu]=(TH1D*)infile->Get(("R4ConeLC_etaSlicesPos"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTakeSecondary)+"_mu"+std::to_string(y)).c_str())->Clone();
					R4ConeLC_MuSlicesNPVAllSecondary[y-minMu]->SetDirectory(0);
				}
			}
			//NoiseAsFcnMu4EMAll=(TH1D*)infile->Get("NoiseAsFcnMu4EMAll")->Clone();
			//NoiseAsFcnMu4LCAll=(TH1D*)infile->Get("NoiseAsFcnMu4LCAll")->Clone();
		}
		if (doMuNPVFixedToys) {
			cout<<"doMuNPVFixedToys"<<endl;
			for (int y=minNPV;y<maxNPV;y++) {
				for (int z=minMu;z<maxMu;z++) {
					R4ConeEM_muSlicesPos[y-minNPV][z-minMu]=(TH1D*)infile->Get(("R4ConeEM_etaSlicesPos"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_mu"+std::to_string(z)+"_NPV"+std::to_string(y)).c_str())->Clone();
					R4ConeEM_muSlicesPos[y-minNPV][z-minMu]->SetDirectory(0);
					R4ConeLC_muSlicesPos[y-minNPV][z-minMu]=(TH1D*)infile->Get(("R4ConeLC_etaSlicesPos"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_mu"+std::to_string(z)+"_NPV"+std::to_string(y)).c_str())->Clone();
					R4ConeLC_muSlicesPos[y-minNPV][z-minMu]->SetDirectory(0);
				}
				//NoiseAsFcnMu4EMNPV[y-1]=(TH1D*)infile->Get(("NoiseAsFcnMu4EMNPV"+std::to_string(y)).c_str())->Clone();
				//NoiseAsFcnMu4LCNPV[y-1]=(TH1D*)infile->Get(("NoiseAsFcnMu4LCNPV"+std::to_string(y)).c_str())->Clone();
			}
			if (doSecondary) {
				for (int y=minNPV;y<maxNPV;y++) {
					for (int z=minMu;z<maxMu;z++) {
						R4ConeEM_muSlicesSecondary[y-minNPV][z-minMu]=(TH1D*)infile->Get(("R4ConeEM_etaSlicesSecondary"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTakeSecondary)+"_mu"+std::to_string(z)+"_NPV"+std::to_string(y)).c_str())->Clone();
						R4ConeEM_muSlicesSecondary[y-minNPV][z-minMu]->SetDirectory(0);
						R4ConeLC_muSlicesSecondary[y-minNPV][z-minMu]=(TH1D*)infile->Get(("R4ConeLC_etaSlicesSecondary"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTakeSecondary)+"_mu"+std::to_string(z)+"_NPV"+std::to_string(y)).c_str())->Clone();
						R4ConeLC_muSlicesSecondary[y-minNPV][z-minMu]->SetDirectory(0);
					}
					//NoiseAsFcnMu4EMNPV[y-1]=(TH1D*)infile->Get(("NoiseAsFcnMu4EMNPV"+std::to_string(y)).c_str())->Clone();
					//NoiseAsFcnMu4LCNPV[y-1]=(TH1D*)infile->Get(("NoiseAsFcnMu4LCNPV"+std::to_string(y)).c_str())->Clone();
				}
			}
		}
		infile->Close();
	}
	if (doMC) {
		infile = TFile::Open((ToyTestInputFile).c_str());
		cout<<"heyo1"<<endl;
		if (doEtaToys) {
			cout<<"doEtaToys"<<endl;
			for (int x=0;x<nEtaSteps;x++) {
				cout<<x<<endl;
				//cout<<vectorOfRunNumbers[0]<<endl;
				R4ConeEM_etaSlicesPosMC[x]=(TH1D*)infile->Get(("R4ConeEM_etaSlicesPosMC"+std::to_string(x)+"_"+std::to_string(x+1)).c_str())->Clone();
				R4ConeEM_etaSlicesPosMC[x]->SetDirectory(0);

				R4ConeLC_etaSlicesPosMC[x]=(TH1D*)infile->Get(("R4ConeLC_etaSlicesPosMC"+std::to_string(x)+"_"+std::to_string(x+1)).c_str())->Clone();
				R4ConeLC_etaSlicesPosMC[x]->SetDirectory(0);

			}
			if (doSecondary) {
				for (int x=0;x<nEtaStepsSecondary;x++) {
					cout<<x<<endl;
					//cout<<vectorOfRunNumbers[0]<<endl;
					R4ConeEM_etaSlicesSecondaryMC[x]=(TH1D*)infile->Get(("R4ConeEM_etaSlicesSecondaryMC"+std::to_string(x)+"_"+std::to_string(x+1)).c_str())->Clone();
					R4ConeEM_etaSlicesSecondaryMC[x]->SetDirectory(0);

					R4ConeLC_etaSlicesSecondaryMC[x]=(TH1D*)infile->Get(("R4ConeLC_etaSlicesSecondaryMC"+std::to_string(x)+"_"+std::to_string(x+1)).c_str())->Clone();
					R4ConeLC_etaSlicesSecondaryMC[x]->SetDirectory(0);

				}
			}
			//NoiseAsFcnEta4EM=(TH1D*)infile->Get("NoiseAsFcnEta4EM")->Clone();
			//NoiseAsFcnEta4LC=(TH1D*)infile->Get("NoiseAsFcnEta4LC")->Clone();
		}

		if (doNPVToys) {
			cout<<"doNPVToys"<<endl;
			for (int y=minNPV;y<maxNPV;y++) {
				cout<<y<<endl;
				R4ConeEM_NPVSlicesPosMC[y-minNPV]=(TH1D*)infile->Get(("R4ConeEM_etaSlicesPosMC"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_NPV"+std::to_string(y)).c_str())->Clone();
				R4ConeEM_NPVSlicesPosMC[y-minNPV]->SetDirectory(0);
				R4ConeLC_NPVSlicesPosMC[y-minNPV]=(TH1D*)infile->Get(("R4ConeLC_etaSlicesPosMC"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_NPV"+std::to_string(y)).c_str())->Clone();
				R4ConeLC_NPVSlicesPosMC[y-minNPV]->SetDirectory(0);
			}
			if (doSecondary) {
				for (int y=minNPV;y<maxNPV;y++) {
					cout<<y<<endl;
					R4ConeEM_NPVSlicesSecondaryMC[y-minNPV]=(TH1D*)infile->Get(("R4ConeEM_etaSlicesSecondaryMC"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTakeSecondary)+"_NPV"+std::to_string(y)).c_str())->Clone();
					R4ConeEM_NPVSlicesSecondaryMC[y-minNPV]->SetDirectory(0);
					R4ConeLC_NPVSlicesSecondaryMC[y-minNPV]=(TH1D*)infile->Get(("R4ConeLC_etaSlicesSecondaryMC"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTakeSecondary)+"_NPV"+std::to_string(y)).c_str())->Clone();
					R4ConeLC_NPVSlicesSecondaryMC[y-minNPV]->SetDirectory(0);
				}
			}
			//NoiseAsFcnNPV4EM=(TH1D*)infile->Get("NoiseAsFcnNPV4EM")->Clone();
			//NoiseAsFcnNPV4LC=(TH1D*)infile->Get("NoiseAsFcnNPV4LC")->Clone();
		}
		if (doMuNPVequalsAllToys) {
			cout<<"doMuNPVequalsAllToys"<<endl;
			for (int y=minMu;y<maxMu;y++) {
				cout<<y<<endl;
				R4ConeEM_MuSlicesNPVAllMC[y-minMu]=(TH1D*)infile->Get(("R4ConeEM_etaSlicesPosMC"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_mu"+std::to_string(y)).c_str())->Clone();
				R4ConeEM_MuSlicesNPVAllMC[y-minMu]->SetDirectory(0);
				R4ConeLC_MuSlicesNPVAllMC[y-minMu]=(TH1D*)infile->Get(("R4ConeLC_etaSlicesPosMC"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_mu"+std::to_string(y)).c_str())->Clone();
				R4ConeLC_MuSlicesNPVAllMC[y-minMu]->SetDirectory(0);
			}
			if (doSecondary) {
				for (int y=minMu;y<maxMu;y++) {
					cout<<y<<endl;
					R4ConeEM_MuSlicesNPVAllSecondaryMC[y-minMu]=(TH1D*)infile->Get(("R4ConeEM_etaSlicesPosMC"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTakeSecondary)+"_mu"+std::to_string(y)).c_str())->Clone();
					R4ConeEM_MuSlicesNPVAllSecondaryMC[y-minMu]->SetDirectory(0);
					R4ConeLC_MuSlicesNPVAllSecondaryMC[y-minMu]=(TH1D*)infile->Get(("R4ConeLC_etaSlicesPosMC"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTakeSecondary)+"_mu"+std::to_string(y)).c_str())->Clone();
					R4ConeLC_MuSlicesNPVAllSecondaryMC[y-minMu]->SetDirectory(0);
				}
			}
			//NoiseAsFcnMu4EMAll=(TH1D*)infile->Get("NoiseAsFcnMu4EMAll")->Clone();
			//NoiseAsFcnMu4LCAll=(TH1D*)infile->Get("NoiseAsFcnMu4LCAll")->Clone();
		}
		if (doMuNPVFixedToys) {
			cout<<"doMuNPVFixedToys"<<endl;
			for (int y=minNPV;y<maxNPV;y++) {
				for (int z=minMu;z<maxMu;z++) {
					R4ConeEM_muSlicesPosMC[y-minNPV][z-minMu]=(TH1D*)infile->Get(("R4ConeEM_etaSlicesPosMC"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_mu"+std::to_string(z)+"_NPV"+std::to_string(y)).c_str())->Clone();
					R4ConeEM_muSlicesPosMC[y-minNPV][z-minMu]->SetDirectory(0);
					R4ConeLC_muSlicesPosMC[y-minNPV][z-minMu]=(TH1D*)infile->Get(("R4ConeLC_etaSlicesPosMC"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTake)+"_mu"+std::to_string(z)+"_NPV"+std::to_string(y)).c_str())->Clone();
					R4ConeLC_muSlicesPosMC[y-minNPV][z-minMu]->SetDirectory(0);
				}
				//NoiseAsFcnMu4EMNPV[y-1]=(TH1D*)infile->Get(("NoiseAsFcnMu4EMNPV"+std::to_string(y)).c_str())->Clone();
				//NoiseAsFcnMu4LCNPV[y-1]=(TH1D*)infile->Get(("NoiseAsFcnMu4LCNPV"+std::to_string(y)).c_str())->Clone();
			}
			if (doSecondary) {
				for (int y=minNPV;y<maxNPV;y++) {
					for (int z=minMu;z<maxMu;z++) {
						R4ConeEM_muSlicesSecondaryMC[y-minNPV][z-minMu]=(TH1D*)infile->Get(("R4ConeEM_etaSlicesSecondaryMC"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTakeSecondary)+"_mu"+std::to_string(z)+"_NPV"+std::to_string(y)).c_str())->Clone();
						R4ConeEM_muSlicesSecondaryMC[y-minNPV][z-minMu]->SetDirectory(0);
						R4ConeLC_muSlicesSecondaryMC[y-minNPV][z-minMu]=(TH1D*)infile->Get(("R4ConeLC_etaSlicesSecondaryMC"+std::to_string(startEtaStep)+"_"+std::to_string(startEtaStep+nEtaStepsToTakeSecondary)+"_mu"+std::to_string(z)+"_NPV"+std::to_string(y)).c_str())->Clone();
						R4ConeLC_muSlicesSecondaryMC[y-minNPV][z-minMu]->SetDirectory(0);
					}
					//NoiseAsFcnMu4EMNPV[y-1]=(TH1D*)infile->Get(("NoiseAsFcnMu4EMNPV"+std::to_string(y)).c_str())->Clone();
					//NoiseAsFcnMu4LCNPV[y-1]=(TH1D*)infile->Get(("NoiseAsFcnMu4LCNPV"+std::to_string(y)).c_str())->Clone();
				}
			}
		}
		infile->Close();
	}
	if (doData) {
		if (doEtaToys) {
			for (int x=0;x<nEtaSteps;x++) {
				R4ConeEM_etaSlicesNoise[x]=new TH1D(("R4ConeEM_etaSlicesNoise"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocks[x])+"<|#eta|<"+std::to_string(etaBlocks[x+1])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
				R4ConeLC_etaSlicesNoise[x]=new TH1D(("R4ConeLC_etaSlicesNoise"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),("R=.4 LC Scale,"+std::to_string(etaBlocks[x])+"<|#eta|<"+std::to_string(etaBlocks[x+1])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
			}
			if (doSecondary) {
				for (int x=0;x<nEtaStepsSecondary;x++) {
					R4ConeEM_etaSlicesNoiseSecondary[x]=new TH1D(("R4ConeEM_etaSlicesNoiseSecondary"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocksSecondary[x])+"<|#eta|<"+std::to_string(etaBlocksSecondary[x+1])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
					R4ConeLC_etaSlicesNoiseSecondary[x]=new TH1D(("R4ConeLC_etaSlicesNoiseSecondary"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),("R=.4 LC Scale,"+std::to_string(etaBlocksSecondary[x])+"<|#eta|<"+std::to_string(etaBlocksSecondary[x+1])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
				}
			}
		}
		if (doNPVToys) {
			for (int y=minNPV;y<maxNPV;y++) {
				R4ConeEM_NoiseAsFcnOfNPV[y-minNPV]=new TH1D(("R4ConeEM_NoiseAsFcnOfNPV"+std::to_string(y)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocks[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocks[startEtaStep+nEtaStepsToTake])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
				R4ConeLC_NoiseAsFcnOfNPV[y-minNPV]=new TH1D(("R4ConeLC_NoiseAsFcnOfNPV"+std::to_string(y)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocks[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocks[startEtaStep+nEtaStepsToTake])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
			}
			if (doSecondary) {
				for (int y=minNPV;y<maxNPV;y++) {
					R4ConeEM_NoiseAsFcnOfNPVSecondary[y-minNPV]=new TH1D(("R4ConeEM_NoiseAsFcnOfNPVSecondary"+std::to_string(y)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocksSecondary[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
					R4ConeLC_NoiseAsFcnOfNPVSecondary[y-minNPV]=new TH1D(("R4ConeLC_NoiseAsFcnOfNPVSecondary"+std::to_string(y)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocksSecondary[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
				}
			}
		}
		if (doMuNPVequalsAllToys) {
			for (int y=minMu;y<maxMu;y++) {
				R4ConeEM_NoiseAsFcnOfMuNPVAll[y-minMu]=new TH1D(("R4ConeEM_NoiseAsFcnOfMuNPVAll"+std::to_string(y)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocks[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocks[startEtaStep+nEtaStepsToTake])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
				R4ConeLC_NoiseAsFcnOfMuNPVAll[y-minMu]=new TH1D(("R4ConeLC_NoiseAsFcnOfMuNPVAll"+std::to_string(y)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocks[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocks[startEtaStep+nEtaStepsToTake])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
			}
			if (doSecondary) {
				for (int y=minMu;y<maxMu;y++) {
					R4ConeEM_NoiseAsFcnOfMuNPVAllSecondary[y-minMu]=new TH1D(("R4ConeEM_NoiseAsFcnOfMuNPVAllSecondary"+std::to_string(y)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocksSecondary[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
					R4ConeLC_NoiseAsFcnOfMuNPVAllSecondary[y-minMu]=new TH1D(("R4ConeLC_NoiseAsFcnOfMuNPVAllSecondary"+std::to_string(y)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocksSecondary[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
				}
			}
		}
		if (doMuNPVFixedToys) {
			for (int y=minNPV;y<maxNPV;y++) {
				for (int z=minMu;z<maxMu;z++) {
					R4ConeEM_NoiseAsFcnOfMuNPVFixed[y-minNPV][z-minMu]=new TH1D(("R4ConeEM_NoiseAsFcnOfMuNPVFixed_NPV"+std::to_string(y)+"_mu"+std::to_string(z)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocks[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocks[startEtaStep+nEtaStepsToTake])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
					R4ConeLC_NoiseAsFcnOfMuNPVFixed[y-minNPV][z-minMu]=new TH1D(("R4ConeLC_NoiseAsFcnOfMuNPVFixed_NPV"+std::to_string(y)+"_mu"+std::to_string(z)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocks[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocks[startEtaStep+nEtaStepsToTake])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
				}
			}
			if (doSecondary) {
				for (int y=minNPV;y<maxNPV;y++) {
					for (int z=minMu;z<maxMu;z++) {
						R4ConeEM_NoiseAsFcnOfMuNPVFixedSecondary[y-minNPV][z-minMu]=new TH1D(("R4ConeEM_NoiseAsFcnOfMuNPVFixed_NPVSecondary"+std::to_string(y)+"_mu"+std::to_string(z)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocksSecondary[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
						R4ConeLC_NoiseAsFcnOfMuNPVFixedSecondary[y-minNPV][z-minMu]=new TH1D(("R4ConeLC_NoiseAsFcnOfMuNPVFixed_NPVSecondary"+std::to_string(y)+"_mu"+std::to_string(z)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocksSecondary[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
					}
				}
			}

		}
	}
	if (doMC) {
		if (doEtaToys) {
			for (int x=0;x<nEtaSteps;x++) {
				R4ConeEM_etaSlicesNoiseMC[x]=new TH1D(("R4ConeEM_etaSlicesNoiseMC"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocks[x])+"<|#eta|<"+std::to_string(etaBlocks[x+1])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
				R4ConeLC_etaSlicesNoiseMC[x]=new TH1D(("R4ConeLC_etaSlicesNoiseMC"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),("R=.4 LC Scale,"+std::to_string(etaBlocks[x])+"<|#eta|<"+std::to_string(etaBlocks[x+1])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
			}
			if (doSecondary) {
				for (int x=0;x<nEtaStepsSecondary;x++) {
					R4ConeEM_etaSlicesNoiseSecondaryMC[x]=new TH1D(("R4ConeEM_etaSlicesNoiseSecondaryMC"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocksSecondary[x])+"<|#eta|<"+std::to_string(etaBlocksSecondary[x+1])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
					R4ConeLC_etaSlicesNoiseSecondaryMC[x]=new TH1D(("R4ConeLC_etaSlicesNoiseSecondaryMC"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),("R=.4 LC Scale,"+std::to_string(etaBlocksSecondary[x])+"<|#eta|<"+std::to_string(etaBlocksSecondary[x+1])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
				}
			}
		}
		if (doNPVToys) {
			for (int y=minNPV;y<maxNPV;y++) {
				R4ConeEM_NoiseAsFcnOfNPVMC[y-minNPV]=new TH1D(("R4ConeEM_NoiseAsFcnOfNPVMC"+std::to_string(y)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocks[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocks[startEtaStep+nEtaStepsToTake])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
				R4ConeLC_NoiseAsFcnOfNPVMC[y-minNPV]=new TH1D(("R4ConeLC_NoiseAsFcnOfNPVMC"+std::to_string(y)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocks[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocks[startEtaStep+nEtaStepsToTake])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
			}
			if (doSecondary) {
				for (int y=minNPV;y<maxNPV;y++) {
					R4ConeEM_NoiseAsFcnOfNPVSecondaryMC[y-minNPV]=new TH1D(("R4ConeEM_NoiseAsFcnOfNPVSecondaryMC"+std::to_string(y)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocksSecondary[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
					R4ConeLC_NoiseAsFcnOfNPVSecondaryMC[y-minNPV]=new TH1D(("R4ConeLC_NoiseAsFcnOfNPVSecondaryMC"+std::to_string(y)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocksSecondary[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
				}
			}
		}
		if (doMuNPVequalsAllToys) {
			for (int y=minMu;y<maxMu;y++) {
				R4ConeEM_NoiseAsFcnOfMuNPVAllMC[y-minMu]=new TH1D(("R4ConeEM_NoiseAsFcnOfMuNPVAllMC"+std::to_string(y)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocks[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocks[startEtaStep+nEtaStepsToTake])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
				R4ConeLC_NoiseAsFcnOfMuNPVAllMC[y-minMu]=new TH1D(("R4ConeLC_NoiseAsFcnOfMuNPVAllMC"+std::to_string(y)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocks[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocks[startEtaStep+nEtaStepsToTake])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
			}
			if (doSecondary) {
				for (int y=minMu;y<maxMu;y++) {
					R4ConeEM_NoiseAsFcnOfMuNPVAllSecondaryMC[y-minMu]=new TH1D(("R4ConeEM_NoiseAsFcnOfMuNPVAllSecondaryMC"+std::to_string(y)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocksSecondary[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
					R4ConeLC_NoiseAsFcnOfMuNPVAllSecondaryMC[y-minMu]=new TH1D(("R4ConeLC_NoiseAsFcnOfMuNPVAllSecondaryMC"+std::to_string(y)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocksSecondary[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
				}
			}
		}
		if (doMuNPVFixedToys) {
			for (int y=minNPV;y<maxNPV;y++) {
				for (int z=minMu;z<maxMu;z++) {
					R4ConeEM_NoiseAsFcnOfMuNPVFixedMC[y-minNPV][z-minMu]=new TH1D(("R4ConeEM_NoiseAsFcnOfMuNPVFixed_NPVMC"+std::to_string(y)+"_mu"+std::to_string(z)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocks[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocks[startEtaStep+nEtaStepsToTake])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
					R4ConeLC_NoiseAsFcnOfMuNPVFixedMC[y-minNPV][z-minMu]=new TH1D(("R4ConeLC_NoiseAsFcnOfMuNPVFixed_NPVMC"+std::to_string(y)+"_mu"+std::to_string(z)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocks[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocks[startEtaStep+nEtaStepsToTake])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
				}
			}
			if (doSecondary) {
				for (int y=minNPV;y<maxNPV;y++) {
					for (int z=minMu;z<maxMu;z++) {
						R4ConeEM_NoiseAsFcnOfMuNPVFixedSecondaryMC[y-minNPV][z-minMu]=new TH1D(("R4ConeEM_NoiseAsFcnOfMuNPVFixed_NPVSecondaryMC"+std::to_string(y)+"_mu"+std::to_string(z)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocksSecondary[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
						R4ConeLC_NoiseAsFcnOfMuNPVFixedSecondaryMC[y-minNPV][z-minMu]=new TH1D(("R4ConeLC_NoiseAsFcnOfMuNPVFixed_NPVSecondaryMC"+std::to_string(y)+"_mu"+std::to_string(z)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocksSecondary[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary])).c_str(),nBinsForNoise,rangeLowerNoise,rangeUpperNoise);
					}
				}
			}

		}
	}

	//the A distributions for the toys
	TH1D *R4ConeEM_etaSlicesPosToys[nEtaSteps];
	TH1D *R4ConeLC_etaSlicesPosToys[nEtaSteps];

	TH1D *R4ConeEM_etaSlicesSecondaryToys[nEtaSteps];
	TH1D *R4ConeLC_etaSlicesSecondaryToys[nEtaSteps];

	TH1D *R4ConeEM_etaSlicesPosToysMC[nEtaSteps];
	TH1D *R4ConeLC_etaSlicesPosToysMC[nEtaSteps];

	TH1D *R4ConeEM_etaSlicesSecondaryToysMC[nEtaSteps];
	TH1D *R4ConeLC_etaSlicesSecondaryToysMC[nEtaSteps];


	TH1D *R4ConeEM_NPVSlicesPosToys[maxNPV-minNPV];
	TH1D *R4ConeLC_NPVSlicesPosToys[maxNPV-minNPV];

	TH1D *R4ConeEM_NPVSlicesSecondaryToys[maxNPV-minNPV];
	TH1D *R4ConeLC_NPVSlicesSecondaryToys[maxNPV-minNPV];

	TH1D *R4ConeEM_NPVSlicesPosToysMC[maxNPV-minNPV];
	TH1D *R4ConeLC_NPVSlicesPosToysMC[maxNPV-minNPV];

	TH1D *R4ConeEM_NPVSlicesSecondaryToysMC[maxNPV-minNPV];
	TH1D *R4ConeLC_NPVSlicesSecondaryToysMC[maxNPV-minNPV];



	TH1D *R4ConeEM_MuSlicesNPVAllToys[maxMu-minMu];
	TH1D *R4ConeLC_MuSlicesNPVAllToys[maxMu-minMu];

	TH1D *R4ConeEM_MuSlicesNPVAllSecondaryToys[maxMu-minMu];
	TH1D *R4ConeLC_MuSlicesNPVAllSecondaryToys[maxMu-minMu];

	TH1D *R4ConeEM_MuSlicesNPVAllToysMC[maxMu-minMu];
	TH1D *R4ConeLC_MuSlicesNPVAllToysMC[maxMu-minMu];

	TH1D *R4ConeEM_MuSlicesNPVAllSecondaryToysMC[maxMu-minMu];
	TH1D *R4ConeLC_MuSlicesNPVAllSecondaryToysMC[maxMu-minMu];



	TH1D *R4ConeEM_muSlicesPosToys[maxNPV-minNPV][maxMu-minMu];
	TH1D *R4ConeLC_muSlicesPosToys[maxNPV-minNPV][maxMu-minMu];

	TH1D *R4ConeEM_muSlicesSecondaryToys[maxNPV-minNPV][maxMu-minMu];
	TH1D *R4ConeLC_muSlicesSecondaryToys[maxNPV-minNPV][maxMu-minMu];

	TH1D *R4ConeEM_muSlicesPosToysMC[maxNPV-minNPV][maxMu-minMu];
	TH1D *R4ConeLC_muSlicesPosToysMC[maxNPV-minNPV][maxMu-minMu];

	TH1D *R4ConeEM_muSlicesSecondaryToysMC[maxNPV-minNPV][maxMu-minMu];
	TH1D *R4ConeLC_muSlicesSecondaryToysMC[maxNPV-minNPV][maxMu-minMu];
	for (int x=0;x<nToys;x++) {
		if (x%10==0) {
			cout <<"finished doing "<< x <<"  toys"<<endl;
		}
		//cout<<"heyo1b"<<endl;
		if (doData) {
			if (doEtaToys) {
				for (int y=0;y<nEtaSteps;y++) {
					R4ConeEM_etaSlicesPosToys[y]=new TH1D(("R4ConeEM_etaSlicesPosToys_"+std::to_string(y)+"_"+std::to_string(y+1)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocks[y])+"<|#eta|<"+std::to_string(etaBlocks[y+1])).c_str(),nBinsForA,-range,range);
					R4ConeLC_etaSlicesPosToys[y]=new TH1D(("R4ConeLC_etaSlicesPosToys_"+std::to_string(y)+"_"+std::to_string(y+1)).c_str(),("R=.4 LC Scale,"+std::to_string(etaBlocks[y])+"<|#eta|<"+std::to_string(etaBlocks[y+1])).c_str(),nBinsForA,-range,range);
				}
				if (doSecondary) {
					for (int y=0;y<nEtaStepsSecondary;y++) {
						R4ConeEM_etaSlicesSecondaryToys[y]=new TH1D(("R4ConeEM_etaSlicesSecondaryToys_"+std::to_string(y)+"_"+std::to_string(y+1)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocksSecondary[y])+"<|#eta|<"+std::to_string(etaBlocksSecondary[y+1])).c_str(),nBinsForA,-range,range);
						R4ConeLC_etaSlicesSecondaryToys[y]=new TH1D(("R4ConeLC_etaSlicesSecondaryToys_"+std::to_string(y)+"_"+std::to_string(y+1)).c_str(),("R=.4 LC Scale,"+std::to_string(etaBlocksSecondary[y])+"<|#eta|<"+std::to_string(etaBlocksSecondary[y+1])).c_str(),nBinsForA,-range,range);
					}
				}
			}
			//cout<<"heyo1c"<<endl;
			if (doNPVToys) {
				for (int y=minNPV;y<maxNPV;y++) {
					R4ConeEM_NPVSlicesPosToys[y-minNPV]=new TH1D(("R4ConeEM_NPVSlicesPosToys_NPV"+std::to_string(y)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocks[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocks[startEtaStep+nEtaStepsToTake])).c_str(),nBinsForA,-range,range);
					R4ConeLC_NPVSlicesPosToys[y-minNPV]=new TH1D(("R4ConeLC_NPVSlicesPosToys_NPV"+std::to_string(y)).c_str(),("R=.4 LC Scale,"+std::to_string(etaBlocks[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocks[startEtaStep+nEtaStepsToTake])).c_str(),nBinsForA,-range,range);
				}
				if (doSecondary) {
					for (int y=minNPV;y<maxNPV;y++) {
						R4ConeEM_NPVSlicesSecondaryToys[y-minNPV]=new TH1D(("R4ConeEM_NPVSlicesSecondaryToys_NPV"+std::to_string(y)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocksSecondary[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary])).c_str(),nBinsForA,-range,range);
						R4ConeLC_NPVSlicesSecondaryToys[y-minNPV]=new TH1D(("R4ConeLC_NPVSlicesSecondaryToys_NPV"+std::to_string(y)).c_str(),("R=.4 LC Scale,"+std::to_string(etaBlocksSecondary[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary])).c_str(),nBinsForA,-range,range);
					}
				}
			}
			//cout<<"heyo1d"<<endl;
			if (doMuNPVequalsAllToys) {
				for (int y=minMu;y<maxMu;y++) {
					R4ConeEM_MuSlicesNPVAllToys[y-minMu]=new TH1D(("R4ConeEM_MuSlicesNPVAllToys_mu"+std::to_string(y)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocks[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocks[startEtaStep+nEtaStepsToTake])).c_str(),nBinsForA,-range,range);
					R4ConeLC_MuSlicesNPVAllToys[y-minMu]=new TH1D(("R4ConeLC_MuSlicesNPVAllToys_mu"+std::to_string(y)).c_str(),("R=.4 LC Scale,"+std::to_string(etaBlocks[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocks[startEtaStep+nEtaStepsToTake])).c_str(),nBinsForA,-range,range);
				}
				if (doSecondary) {
					for (int y=minMu;y<maxMu;y++) {
						R4ConeEM_MuSlicesNPVAllSecondaryToys[y-minMu]=new TH1D(("R4ConeEM_MuSlicesNPVAllSecondaryToys_mu"+std::to_string(y)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocksSecondary[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary])).c_str(),nBinsForA,-range,range);
						R4ConeLC_MuSlicesNPVAllSecondaryToys[y-minMu]=new TH1D(("R4ConeLC_MuSlicesNPVAllSecondaryToys_mu"+std::to_string(y)).c_str(),("R=.4 LC Scale,"+std::to_string(etaBlocksSecondary[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary])).c_str(),nBinsForA,-range,range);
					}
				}
			}
			//cout<<"heyo1e"<<endl;
			if (doMuNPVFixedToys) {
				for (int y=minNPV;y<maxNPV;y++) {
					for (int z=minMu;z<maxMu;z++) {
						R4ConeEM_muSlicesPosToys[y-minNPV][z-minMu]=new TH1D(("R4ConeEM_muSlicesPosToys_NPV"+std::to_string(y)+"_mu"+std::to_string(z)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocks[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocks[startEtaStep+nEtaStepsToTake])).c_str(),nBinsForA,-range,range);
						R4ConeLC_muSlicesPosToys[y-minNPV][z-minMu]=new TH1D(("R4ConeLC_muSlicesPosToys_NPV"+std::to_string(y)+"_mu"+std::to_string(z)).c_str(),("R=.4 LC Scale,"+std::to_string(etaBlocks[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocks[startEtaStep+nEtaStepsToTake])).c_str(),nBinsForA,-range,range);
					}
				}
				if (doSecondary) {
					for (int y=minNPV;y<maxNPV;y++) {
						for (int z=minMu;z<maxMu;z++) {
							R4ConeEM_muSlicesSecondaryToys[y-minNPV][z-minMu]=new TH1D(("R4ConeEM_muSlicesSecondaryToys_NPV"+std::to_string(y)+"_mu"+std::to_string(z)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocksSecondary[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary])).c_str(),nBinsForA,-range,range);
							R4ConeLC_muSlicesSecondaryToys[y-minNPV][z-minMu]=new TH1D(("R4ConeLC_muSlicesSecondaryToys_NPV"+std::to_string(y)+"_mu"+std::to_string(z)).c_str(),("R=.4 LC Scale,"+std::to_string(etaBlocksSecondary[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary])).c_str(),nBinsForA,-range,range);
						}
					}
				}
			}
		}
		if (doMC) {
			if (doEtaToys) {
				for (int y=0;y<nEtaSteps;y++) {
					R4ConeEM_etaSlicesPosToysMC[y]=new TH1D(("R4ConeEM_etaSlicesPosToysMC_"+std::to_string(y)+"_"+std::to_string(y+1)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocks[y])+"<|#eta|<"+std::to_string(etaBlocks[y+1])).c_str(),nBinsForA,-range,range);
					R4ConeLC_etaSlicesPosToysMC[y]=new TH1D(("R4ConeLC_etaSlicesPosToysMC_"+std::to_string(y)+"_"+std::to_string(y+1)).c_str(),("R=.4 LC Scale,"+std::to_string(etaBlocks[y])+"<|#eta|<"+std::to_string(etaBlocks[y+1])).c_str(),nBinsForA,-range,range);
				}
				if (doSecondary) {
					for (int y=0;y<nEtaStepsSecondary;y++) {
						R4ConeEM_etaSlicesSecondaryToysMC[y]=new TH1D(("R4ConeEM_etaSlicesSecondaryToysMC_"+std::to_string(y)+"_"+std::to_string(y+1)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocksSecondary[y])+"<|#eta|<"+std::to_string(etaBlocksSecondary[y+1])).c_str(),nBinsForA,-range,range);
						R4ConeLC_etaSlicesSecondaryToysMC[y]=new TH1D(("R4ConeLC_etaSlicesSecondaryToysMC_"+std::to_string(y)+"_"+std::to_string(y+1)).c_str(),("R=.4 LC Scale,"+std::to_string(etaBlocksSecondary[y])+"<|#eta|<"+std::to_string(etaBlocksSecondary[y+1])).c_str(),nBinsForA,-range,range);
					}
				}
			}
			//cout<<"heyo1c"<<endl;
			if (doNPVToys) {
				for (int y=minNPV;y<maxNPV;y++) {
					R4ConeEM_NPVSlicesPosToysMC[y-minNPV]=new TH1D(("R4ConeEM_NPVSlicesPosToysMC_NPV"+std::to_string(y)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocks[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocks[startEtaStep+nEtaStepsToTake])).c_str(),nBinsForA,-range,range);
					R4ConeLC_NPVSlicesPosToysMC[y-minNPV]=new TH1D(("R4ConeLC_NPVSlicesPosToysMC_NPV"+std::to_string(y)).c_str(),("R=.4 LC Scale,"+std::to_string(etaBlocks[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocks[startEtaStep+nEtaStepsToTake])).c_str(),nBinsForA,-range,range);
				}
				if (doSecondary) {
					for (int y=minNPV;y<maxNPV;y++) {
						R4ConeEM_NPVSlicesSecondaryToysMC[y-minNPV]=new TH1D(("R4ConeEM_NPVSlicesSecondaryToysMC_NPV"+std::to_string(y)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocksSecondary[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary])).c_str(),nBinsForA,-range,range);
						R4ConeLC_NPVSlicesSecondaryToysMC[y-minNPV]=new TH1D(("R4ConeLC_NPVSlicesSecondaryToysMC_NPV"+std::to_string(y)).c_str(),("R=.4 LC Scale,"+std::to_string(etaBlocksSecondary[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary])).c_str(),nBinsForA,-range,range);
					}
				}
			}
			//cout<<"heyo1d"<<endl;
			if (doMuNPVequalsAllToys) {
				for (int y=minMu;y<maxMu;y++) {
					R4ConeEM_MuSlicesNPVAllToysMC[y-minMu]=new TH1D(("R4ConeEM_MuSlicesNPVAllToysMC_mu"+std::to_string(y)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocks[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocks[startEtaStep+nEtaStepsToTake])).c_str(),nBinsForA,-range,range);
					R4ConeLC_MuSlicesNPVAllToysMC[y-minMu]=new TH1D(("R4ConeLC_MuSlicesNPVAllToysMC_mu"+std::to_string(y)).c_str(),("R=.4 LC Scale,"+std::to_string(etaBlocks[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocks[startEtaStep+nEtaStepsToTake])).c_str(),nBinsForA,-range,range);
				}
				if (doSecondary) {
					for (int y=minMu;y<maxMu;y++) {
						R4ConeEM_MuSlicesNPVAllSecondaryToysMC[y-minMu]=new TH1D(("R4ConeEM_MuSlicesNPVAllSecondaryToysMC_mu"+std::to_string(y)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocksSecondary[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary])).c_str(),nBinsForA,-range,range);
						R4ConeLC_MuSlicesNPVAllSecondaryToysMC[y-minMu]=new TH1D(("R4ConeLC_MuSlicesNPVAllSecondaryToysMC_mu"+std::to_string(y)).c_str(),("R=.4 LC Scale,"+std::to_string(etaBlocksSecondary[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary])).c_str(),nBinsForA,-range,range);
					}
				}
			}
			//cout<<"heyo1e"<<endl;
			if (doMuNPVFixedToys) {
				for (int y=minNPV;y<maxNPV;y++) {
					for (int z=minMu;z<maxMu;z++) {
						R4ConeEM_muSlicesPosToysMC[y-minNPV][z-minMu]=new TH1D(("R4ConeEM_muSlicesPosToysMC_NPV"+std::to_string(y)+"_mu"+std::to_string(z)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocks[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocks[startEtaStep+nEtaStepsToTake])).c_str(),nBinsForA,-range,range);
						R4ConeLC_muSlicesPosToysMC[y-minNPV][z-minMu]=new TH1D(("R4ConeLC_muSlicesPosToysMC_NPV"+std::to_string(y)+"_mu"+std::to_string(z)).c_str(),("R=.4 LC Scale,"+std::to_string(etaBlocks[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocks[startEtaStep+nEtaStepsToTake])).c_str(),nBinsForA,-range,range);
					}
				}
				if (doSecondary) {
					for (int y=minNPV;y<maxNPV;y++) {
						for (int z=minMu;z<maxMu;z++) {
							R4ConeEM_muSlicesSecondaryToysMC[y-minNPV][z-minMu]=new TH1D(("R4ConeEM_muSlicesSecondaryToysMC_NPV"+std::to_string(y)+"_mu"+std::to_string(z)).c_str(),("R=.4 EM Scale,"+std::to_string(etaBlocksSecondary[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary])).c_str(),nBinsForA,-range,range);
							R4ConeLC_muSlicesSecondaryToysMC[y-minNPV][z-minMu]=new TH1D(("R4ConeLC_muSlicesSecondaryToysMC_NPV"+std::to_string(y)+"_mu"+std::to_string(z)).c_str(),("R=.4 LC Scale,"+std::to_string(etaBlocksSecondary[startEtaStep])+"<|#eta|<"+std::to_string(etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary])).c_str(),nBinsForA,-range,range);
						}
					}
				}
			}
		}



		cout<<"heyo2"<<endl;

		Double_t tempWidth=0;
		cout<<"heyo5"<<endl;

		if (doData) {
			if (doEtaToys) {
				for (int y=0;y<nEtaSteps;y++) {



					generateToy(R4ConeEM_etaSlicesPos[y],R4ConeEM_etaSlicesPosToys[y],toyType);
					//cout<<"test1"<<endl;
					tempWidth=getWidth(R4ConeEM_etaSlicesPosToys[y]);
					//cout<<"test2"<<endl;
					if (tempWidth>=0){
						//R4ConeEM_etaSlicesNoise[y]->Fill(3);
						//cout<<tempWidth<<endl;
						R4ConeEM_etaSlicesNoise[y]->Fill(tempWidth/TMath::Sqrt(2));
					}
					//cout<<"test3"<<endl;
					generateToy(R4ConeLC_etaSlicesPos[y],R4ConeLC_etaSlicesPosToys[y],toyType);
					tempWidth=getWidth(R4ConeLC_etaSlicesPosToys[y]);
					if (tempWidth>=0){
						R4ConeLC_etaSlicesNoise[y]->Fill(tempWidth/TMath::Sqrt(2));
					}
				}
				if (doSecondary) {
					for (int y=0;y<nEtaStepsSecondary;y++) {

					//cout<<"test1a"<<endl;
					//tempWidth=getWidth(R4ConeEM_etaSlicesPos[y]);
					//cout<<"tempWidth"<<tempWidth<<endl;

						generateToy(R4ConeEM_etaSlicesSecondary[y],R4ConeEM_etaSlicesSecondaryToys[y],toyType);
						//cout<<"test1"<<endl;
						tempWidth=getWidth(R4ConeEM_etaSlicesSecondaryToys[y]);
						//cout<<"test2"<<endl;
						if (tempWidth>=0){
							//R4ConeEM_etaSlicesNoise[y]->Fill(3);
							//cout<<tempWidth<<endl;
							R4ConeEM_etaSlicesNoiseSecondary[y]->Fill(tempWidth/TMath::Sqrt(2));
						}
						//cout<<"test3"<<endl;
						generateToy(R4ConeLC_etaSlicesSecondary[y],R4ConeLC_etaSlicesSecondaryToys[y],toyType);
						tempWidth=getWidth(R4ConeLC_etaSlicesSecondaryToys[y]);
						if (tempWidth>=0){
							R4ConeLC_etaSlicesNoiseSecondary[y]->Fill(tempWidth/TMath::Sqrt(2));
						}
					}
				}
			}
			if (doNPVToys) {
				for (int y=minNPV;y<maxNPV;y++) {
					generateToy(R4ConeEM_NPVSlicesPos[y-minNPV],R4ConeEM_NPVSlicesPosToys[y-minNPV],toyType);
					tempWidth=getWidth(R4ConeEM_NPVSlicesPosToys[y-minNPV]);
					if (tempWidth>=0){
						R4ConeEM_NoiseAsFcnOfNPV[y-minNPV]->Fill(tempWidth/TMath::Sqrt(2));
					}
					generateToy(R4ConeLC_NPVSlicesPos[y-minNPV],R4ConeLC_NPVSlicesPosToys[y-minNPV],toyType);
					tempWidth=getWidth(R4ConeLC_NPVSlicesPosToys[y-minNPV]);
					if (tempWidth>=0){
						R4ConeLC_NoiseAsFcnOfNPV[y-minNPV]->Fill(tempWidth/TMath::Sqrt(2));
					}
				}
				if (doSecondary) {
					for (int y=minNPV;y<maxNPV;y++) {
						generateToy(R4ConeEM_NPVSlicesSecondary[y-minNPV],R4ConeEM_NPVSlicesSecondaryToys[y-minNPV],toyType);
						tempWidth=getWidth(R4ConeEM_NPVSlicesSecondaryToys[y-minNPV]);
						if (tempWidth>=0){
							R4ConeEM_NoiseAsFcnOfNPVSecondary[y-minNPV]->Fill(tempWidth/TMath::Sqrt(2));
						}
						generateToy(R4ConeLC_NPVSlicesSecondary[y-minNPV],R4ConeLC_NPVSlicesSecondaryToys[y-minNPV],toyType);
						tempWidth=getWidth(R4ConeLC_NPVSlicesSecondaryToys[y-minNPV]);
						if (tempWidth>=0){
							R4ConeLC_NoiseAsFcnOfNPVSecondary[y-minNPV]->Fill(tempWidth/TMath::Sqrt(2));
						}
					}
				}
			}
			if (doMuNPVequalsAllToys) {
				for (int y=minMu;y<maxMu;y++) {
					//cout<<"y="<<y<<endl;
					generateToy(R4ConeEM_MuSlicesNPVAll[y-minMu],R4ConeEM_MuSlicesNPVAllToys[y-minMu],toyType);
					tempWidth=getWidth(R4ConeEM_MuSlicesNPVAllToys[y-minMu]);
					if (tempWidth>=0){
						R4ConeEM_NoiseAsFcnOfMuNPVAll[y-minMu]->Fill(tempWidth/TMath::Sqrt(2));
					}
					generateToy(R4ConeLC_MuSlicesNPVAll[y-minMu],R4ConeLC_MuSlicesNPVAllToys[y-minMu],toyType);
					tempWidth=getWidth(R4ConeLC_MuSlicesNPVAllToys[y-minMu]);
					if (tempWidth>=0){
						R4ConeLC_NoiseAsFcnOfMuNPVAll[y-minMu]->Fill(tempWidth/TMath::Sqrt(2));
					}
				}
				if (doSecondary) {
					for (int y=minMu;y<maxMu;y++) {
						//cout<<"y="<<y<<endl;
						generateToy(R4ConeEM_MuSlicesNPVAllSecondary[y-minMu],R4ConeEM_MuSlicesNPVAllSecondaryToys[y-minMu],toyType);
						tempWidth=getWidth(R4ConeEM_MuSlicesNPVAllSecondaryToys[y-minMu]);
						if (tempWidth>=0){
							R4ConeEM_NoiseAsFcnOfMuNPVAllSecondary[y-minMu]->Fill(tempWidth/TMath::Sqrt(2));
						}
						generateToy(R4ConeLC_MuSlicesNPVAllSecondary[y-minMu],R4ConeLC_MuSlicesNPVAllSecondaryToys[y-minMu],toyType);
						tempWidth=getWidth(R4ConeLC_MuSlicesNPVAllSecondaryToys[y-minMu]);
						if (tempWidth>=0){
							R4ConeLC_NoiseAsFcnOfMuNPVAllSecondary[y-minMu]->Fill(tempWidth/TMath::Sqrt(2));
						}
					}
				}
			}
			if (doMuNPVFixedToys) {
				for (int y=minNPV;y<maxNPV;y++) {
					for (int z=minMu;z<maxMu;z++) {
						generateToy(R4ConeEM_muSlicesPos[y-minNPV][z-minMu],R4ConeEM_muSlicesPosToys[y-minNPV][z-minMu],toyType);
						tempWidth=getWidth(R4ConeEM_muSlicesPosToys[y-minNPV][z-minMu]);
						if (tempWidth>=0){
							R4ConeEM_NoiseAsFcnOfMuNPVFixed[y-minNPV][z-minMu]->Fill(tempWidth/TMath::Sqrt(2));
						}
						generateToy(R4ConeLC_muSlicesPos[y-minNPV][z-minMu],R4ConeLC_muSlicesPosToys[y-minNPV][z-minMu],toyType);
						tempWidth=getWidth(R4ConeLC_muSlicesPosToys[y-minNPV][z-minMu]);
						if (tempWidth>=0){
							R4ConeLC_NoiseAsFcnOfMuNPVFixed[y-minNPV][z-minMu]->Fill(tempWidth/TMath::Sqrt(2));
						}
					}
				}
				if (doSecondary) {
					for (int y=minNPV;y<maxNPV;y++) {
						for (int z=minMu;z<maxMu;z++) {
							generateToy(R4ConeEM_muSlicesSecondary[y-minNPV][z-minMu],R4ConeEM_muSlicesSecondaryToys[y-minNPV][z-minMu],toyType);
							tempWidth=getWidth(R4ConeEM_muSlicesSecondaryToys[y-minNPV][z-minMu]);
							if (tempWidth>=0){
								R4ConeEM_NoiseAsFcnOfMuNPVFixedSecondary[y-minNPV][z-minMu]->Fill(tempWidth/TMath::Sqrt(2));
							}
							generateToy(R4ConeLC_muSlicesSecondary[y-minNPV][z-minMu],R4ConeLC_muSlicesSecondaryToys[y-minNPV][z-minMu],toyType);
							tempWidth=getWidth(R4ConeLC_muSlicesSecondaryToys[y-minNPV][z-minMu]);
							if (tempWidth>=0){
								R4ConeLC_NoiseAsFcnOfMuNPVFixedSecondary[y-minNPV][z-minMu]->Fill(tempWidth/TMath::Sqrt(2));
							}
						}
					}
				}
			}
		}
		if (doMC) {
			if (doEtaToys) {
				for (int y=0;y<nEtaSteps;y++) {

				//cout<<"test1a"<<endl;
				//tempWidth=getWidth(R4ConeEM_etaSlicesPos[y]);
				//cout<<"tempWidth"<<tempWidth<<endl;

					generateToy(R4ConeEM_etaSlicesPosMC[y],R4ConeEM_etaSlicesPosToysMC[y],toyType);
					//cout<<"test1"<<endl;
					tempWidth=getWidth(R4ConeEM_etaSlicesPosToysMC[y]);
					//cout<<"test2"<<endl;
					if (tempWidth>=0){
						//R4ConeEM_etaSlicesNoise[y]->Fill(3);
						//cout<<tempWidth<<endl;
						R4ConeEM_etaSlicesNoiseMC[y]->Fill(tempWidth/TMath::Sqrt(2));
					}
					//cout<<"test3"<<endl;
					generateToy(R4ConeLC_etaSlicesPosMC[y],R4ConeLC_etaSlicesPosToysMC[y],toyType);
					tempWidth=getWidth(R4ConeLC_etaSlicesPosToysMC[y]);
					if (tempWidth>=0){
						R4ConeLC_etaSlicesNoiseMC[y]->Fill(tempWidth/TMath::Sqrt(2));
					}
				}
				if (doSecondary) {
					for (int y=0;y<nEtaStepsSecondary;y++) {

					//cout<<"test1a"<<endl;
					//tempWidth=getWidth(R4ConeEM_etaSlicesPos[y]);
					//cout<<"tempWidth"<<tempWidth<<endl;

						generateToy(R4ConeEM_etaSlicesSecondaryMC[y],R4ConeEM_etaSlicesSecondaryToysMC[y],toyType);
						//cout<<"test1"<<endl;
						tempWidth=getWidth(R4ConeEM_etaSlicesSecondaryToysMC[y]);
						//cout<<"test2"<<endl;
						if (tempWidth>=0){
							//R4ConeEM_etaSlicesNoise[y]->Fill(3);
							//cout<<tempWidth<<endl;
							R4ConeEM_etaSlicesNoiseSecondaryMC[y]->Fill(tempWidth/TMath::Sqrt(2));
						}
						//cout<<"test3"<<endl;
						generateToy(R4ConeLC_etaSlicesSecondaryMC[y],R4ConeLC_etaSlicesSecondaryToysMC[y],toyType);
						tempWidth=getWidth(R4ConeLC_etaSlicesSecondaryToysMC[y]);
						if (tempWidth>=0){
							R4ConeLC_etaSlicesNoiseSecondaryMC[y]->Fill(tempWidth/TMath::Sqrt(2));
						}
					}
				}
			}
			if (doNPVToys) {
				for (int y=minNPV;y<maxNPV;y++) {
					generateToy(R4ConeEM_NPVSlicesPosMC[y-minNPV],R4ConeEM_NPVSlicesPosToysMC[y-minNPV],toyType);
					tempWidth=getWidth(R4ConeEM_NPVSlicesPosToysMC[y-minNPV]);
					if (tempWidth>=0){
						R4ConeEM_NoiseAsFcnOfNPVMC[y-minNPV]->Fill(tempWidth/TMath::Sqrt(2));
					}
					generateToy(R4ConeLC_NPVSlicesPosMC[y-minNPV],R4ConeLC_NPVSlicesPosToysMC[y-minNPV],toyType);
					tempWidth=getWidth(R4ConeLC_NPVSlicesPosToysMC[y-minNPV]);
					if (tempWidth>=0){
						R4ConeLC_NoiseAsFcnOfNPVMC[y-minNPV]->Fill(tempWidth/TMath::Sqrt(2));
					}
				}
				if (doSecondary) {
					for (int y=minNPV;y<maxNPV;y++) {
						generateToy(R4ConeEM_NPVSlicesSecondaryMC[y-minNPV],R4ConeEM_NPVSlicesSecondaryToysMC[y-minNPV],toyType);
						tempWidth=getWidth(R4ConeEM_NPVSlicesSecondaryToysMC[y-minNPV]);
						if (tempWidth>=0){
							R4ConeEM_NoiseAsFcnOfNPVSecondaryMC[y-minNPV]->Fill(tempWidth/TMath::Sqrt(2));
						}
						generateToy(R4ConeLC_NPVSlicesSecondaryMC[y-minNPV],R4ConeLC_NPVSlicesSecondaryToysMC[y-minNPV],toyType);
						tempWidth=getWidth(R4ConeLC_NPVSlicesSecondaryToysMC[y-minNPV]);
						if (tempWidth>=0){
							R4ConeLC_NoiseAsFcnOfNPVSecondaryMC[y-minNPV]->Fill(tempWidth/TMath::Sqrt(2));
						}
					}
				}
			}
			if (doMuNPVequalsAllToys) {
				for (int y=minMu;y<maxMu;y++) {
					//cout<<"y="<<y<<endl;
					generateToy(R4ConeEM_MuSlicesNPVAllMC[y-minMu],R4ConeEM_MuSlicesNPVAllToysMC[y-minMu],toyType);
					tempWidth=getWidth(R4ConeEM_MuSlicesNPVAllToysMC[y-minMu]);
					if (tempWidth>=0){
						R4ConeEM_NoiseAsFcnOfMuNPVAllMC[y-minMu]->Fill(tempWidth/TMath::Sqrt(2));
					}
					generateToy(R4ConeLC_MuSlicesNPVAllMC[y-minMu],R4ConeLC_MuSlicesNPVAllToysMC[y-minMu],toyType);
					tempWidth=getWidth(R4ConeLC_MuSlicesNPVAllToysMC[y-minMu]);
					if (tempWidth>=0){
						R4ConeLC_NoiseAsFcnOfMuNPVAllMC[y-minMu]->Fill(tempWidth/TMath::Sqrt(2));
					}
				}
				if (doSecondary) {
					for (int y=minMu;y<maxMu;y++) {
						//cout<<"y="<<y<<endl;
						generateToy(R4ConeEM_MuSlicesNPVAllSecondaryMC[y-minMu],R4ConeEM_MuSlicesNPVAllSecondaryToysMC[y-minMu],toyType);
						tempWidth=getWidth(R4ConeEM_MuSlicesNPVAllSecondaryToysMC[y-minMu]);
						if (tempWidth>=0){
							R4ConeEM_NoiseAsFcnOfMuNPVAllSecondaryMC[y-minMu]->Fill(tempWidth/TMath::Sqrt(2));
						}
						generateToy(R4ConeLC_MuSlicesNPVAllSecondaryMC[y-minMu],R4ConeLC_MuSlicesNPVAllSecondaryToysMC[y-minMu],toyType);
						tempWidth=getWidth(R4ConeLC_MuSlicesNPVAllSecondaryToysMC[y-minMu]);
						if (tempWidth>=0){
							R4ConeLC_NoiseAsFcnOfMuNPVAllSecondaryMC[y-minMu]->Fill(tempWidth/TMath::Sqrt(2));
						}
					}
				}
			}
			if (doMuNPVFixedToys) {
				for (int y=minNPV;y<maxNPV;y++) {
					for (int z=minMu;z<maxMu;z++) {
						generateToy(R4ConeEM_muSlicesPosMC[y-minNPV][z-minMu],R4ConeEM_muSlicesPosToysMC[y-minNPV][z-minMu],toyType);
						tempWidth=getWidth(R4ConeEM_muSlicesPosToysMC[y-minNPV][z-minMu]);
						if (tempWidth>=0){
							R4ConeEM_NoiseAsFcnOfMuNPVFixedMC[y-minNPV][z-minMu]->Fill(tempWidth/TMath::Sqrt(2));
						}
						generateToy(R4ConeLC_muSlicesPosMC[y-minNPV][z-minMu],R4ConeLC_muSlicesPosToysMC[y-minNPV][z-minMu],toyType);
						tempWidth=getWidth(R4ConeLC_muSlicesPosToysMC[y-minNPV][z-minMu]);
						if (tempWidth>=0){
							R4ConeLC_NoiseAsFcnOfMuNPVFixedMC[y-minNPV][z-minMu]->Fill(tempWidth/TMath::Sqrt(2));
						}
					}
				}
				if (doSecondary) {
					for (int y=minNPV;y<maxNPV;y++) {
						for (int z=minMu;z<maxMu;z++) {
							generateToy(R4ConeEM_muSlicesSecondaryMC[y-minNPV][z-minMu],R4ConeEM_muSlicesSecondaryToysMC[y-minNPV][z-minMu],toyType);
							tempWidth=getWidth(R4ConeEM_muSlicesSecondaryToysMC[y-minNPV][z-minMu]);
							if (tempWidth>=0){
								R4ConeEM_NoiseAsFcnOfMuNPVFixedSecondaryMC[y-minNPV][z-minMu]->Fill(tempWidth/TMath::Sqrt(2));
							}
							generateToy(R4ConeLC_muSlicesSecondaryMC[y-minNPV][z-minMu],R4ConeLC_muSlicesSecondaryToysMC[y-minNPV][z-minMu],toyType);
							tempWidth=getWidth(R4ConeLC_muSlicesSecondaryToysMC[y-minNPV][z-minMu]);
							if (tempWidth>=0){
								R4ConeLC_NoiseAsFcnOfMuNPVFixedSecondaryMC[y-minNPV][z-minMu]->Fill(tempWidth/TMath::Sqrt(2));
							}
						}
					}
				}
			}
		}
		if (doData) {
			if (doEtaToys) {
				for (int y=0;y<nEtaSteps;y++) {
					delete R4ConeEM_etaSlicesPosToys[y];
					delete R4ConeLC_etaSlicesPosToys[y];
				}
				if (doSecondary) {
					for (int y=0;y<nEtaStepsSecondary;y++) {
						delete R4ConeEM_etaSlicesSecondaryToys[y];
						delete R4ConeLC_etaSlicesSecondaryToys[y];
					}
				}
			}
			//cout<<"heyo1c"<<endl;
			if (doNPVToys) {
				for (int y=minNPV;y<maxNPV;y++) {
					delete R4ConeEM_NPVSlicesPosToys[y-minNPV];
					delete R4ConeLC_NPVSlicesPosToys[y-minNPV];
				}
				if (doSecondary) {
					for (int y=minNPV;y<maxNPV;y++) {
						delete R4ConeEM_NPVSlicesSecondaryToys[y-minNPV];
						delete R4ConeLC_NPVSlicesSecondaryToys[y-minNPV];
					}
				}
			}
			//cout<<"heyo1d"<<endl;
			if (doMuNPVequalsAllToys) {
				for (int y=minMu;y<maxMu;y++) {
					delete R4ConeEM_MuSlicesNPVAllToys[y-minMu];
					delete R4ConeLC_MuSlicesNPVAllToys[y-minMu];
				}
				if (doSecondary) {
					for (int y=minMu;y<maxMu;y++) {
						delete R4ConeEM_MuSlicesNPVAllSecondaryToys[y-minMu];
						delete R4ConeLC_MuSlicesNPVAllSecondaryToys[y-minMu];
					}
				}
			}
			//cout<<"heyo1e"<<endl;
			if (doMuNPVFixedToys) {
				for (int y=minNPV;y<maxNPV;y++) {
					for (int z=minMu;z<maxMu;z++) {
						delete R4ConeEM_muSlicesPosToys[y-minNPV][z-minMu];
						delete R4ConeLC_muSlicesPosToys[y-minNPV][z-minMu];
					}
				}
				if (doSecondary) {
					for (int y=minNPV;y<maxNPV;y++) {
						for (int z=minMu;z<maxMu;z++) {
							delete R4ConeEM_muSlicesSecondaryToys[y-minNPV][z-minMu];
							delete R4ConeLC_muSlicesSecondaryToys[y-minNPV][z-minMu];
						}
					}
				}
			}
		}
		if (doMC) {
			if (doEtaToys) {
				for (int y=0;y<nEtaSteps;y++) {
					delete R4ConeEM_etaSlicesPosToysMC[y];
					delete R4ConeLC_etaSlicesPosToysMC[y];
				}
				if (doSecondary) {
					for (int y=0;y<nEtaStepsSecondary;y++) {
						delete R4ConeEM_etaSlicesSecondaryToysMC[y];
						delete R4ConeLC_etaSlicesSecondaryToysMC[y];
					}
				}
			}
			//cout<<"heyo1c"<<endl;
			if (doNPVToys) {
				for (int y=minNPV;y<maxNPV;y++) {
					delete R4ConeEM_NPVSlicesPosToysMC[y-minNPV];
					delete R4ConeLC_NPVSlicesPosToysMC[y-minNPV];
				}
				if (doSecondary) {
					for (int y=minNPV;y<maxNPV;y++) {
						delete R4ConeEM_NPVSlicesSecondaryToysMC[y-minNPV];
						delete R4ConeLC_NPVSlicesSecondaryToysMC[y-minNPV];
					}
				}
			}
			//cout<<"heyo1d"<<endl;
			if (doMuNPVequalsAllToys) {
				for (int y=minMu;y<maxMu;y++) {
					delete R4ConeEM_MuSlicesNPVAllToysMC[y-minMu];
					delete R4ConeLC_MuSlicesNPVAllToysMC[y-minMu];
				}
				if (doSecondary) {
					for (int y=minMu;y<maxMu;y++) {
						delete R4ConeEM_MuSlicesNPVAllSecondaryToysMC[y-minMu];
						delete R4ConeLC_MuSlicesNPVAllSecondaryToysMC[y-minMu];
					}
				}
			}
			//cout<<"heyo1e"<<endl;
			if (doMuNPVFixedToys) {
				for (int y=minNPV;y<maxNPV;y++) {
					for (int z=minMu;z<maxMu;z++) {
						delete R4ConeEM_muSlicesPosToysMC[y-minNPV][z-minMu];
						delete R4ConeLC_muSlicesPosToysMC[y-minNPV][z-minMu];
					}
				}
				if (doSecondary) {
					for (int y=minNPV;y<maxNPV;y++) {
						for (int z=minMu;z<maxMu;z++) {
							delete R4ConeEM_muSlicesSecondaryToysMC[y-minNPV][z-minMu];
							delete R4ConeLC_muSlicesSecondaryToysMC[y-minNPV][z-minMu];
						}
					}
				}
			}
		}
	}


	TFile *outputFile=new TFile((OutputFileToyNoise+".root").c_str(),"RECREATE");
	if (doData) {
		if (doEtaToys) {
			for (int y=0;y<nEtaSteps;y++) {
				R4ConeEM_etaSlicesNoise[y]->Write();
				R4ConeLC_etaSlicesNoise[y]->Write();
			}
			if (doSecondary) {
				for (int y=0;y<nEtaStepsSecondary;y++) {
					R4ConeEM_etaSlicesNoiseSecondary[y]->Write();
					R4ConeLC_etaSlicesNoiseSecondary[y]->Write();
				}
			}

		}
		if (doNPVToys) {
			for (int y=minNPV;y<maxNPV;y++) {
				R4ConeEM_NoiseAsFcnOfNPV[y-minNPV]->Write();
				R4ConeLC_NoiseAsFcnOfNPV[y-minNPV]->Write();
			}
			if (doSecondary) {
				for (int y=minNPV;y<maxNPV;y++) {
					R4ConeEM_NoiseAsFcnOfNPVSecondary[y-minNPV]->Write();
					R4ConeLC_NoiseAsFcnOfNPVSecondary[y-minNPV]->Write();
				}
			}
		}
		if (doMuNPVequalsAllToys) {
			for (int y=minMu;y<maxMu;y++) {
				R4ConeEM_NoiseAsFcnOfMuNPVAll[y-minMu]->Write();
				R4ConeLC_NoiseAsFcnOfMuNPVAll[y-minMu]->Write();
			}
			if (doSecondary) {
				for (int y=minMu;y<maxMu;y++) {
					R4ConeEM_NoiseAsFcnOfMuNPVAllSecondary[y-minMu]->Write();
					R4ConeLC_NoiseAsFcnOfMuNPVAllSecondary[y-minMu]->Write();
				}
			}
		}
		if (doMuNPVFixedToys) {
			for (int y=minNPV;y<maxNPV;y++) {
				for (int z=minMu;z<maxMu;z++) {
					R4ConeEM_NoiseAsFcnOfMuNPVFixed[y-minNPV][z-minMu]->Write();
					R4ConeLC_NoiseAsFcnOfMuNPVFixed[y-minNPV][z-minMu]->Write();
				}
			}
			if (doSecondary) {
				for (int y=minNPV;y<maxNPV;y++) {
					for (int z=minMu;z<maxMu;z++) {
						R4ConeEM_NoiseAsFcnOfMuNPVFixedSecondary[y-minNPV][z-minMu]->Write();
						R4ConeLC_NoiseAsFcnOfMuNPVFixedSecondary[y-minNPV][z-minMu]->Write();
					}
				}
			}
		}
	}
	if (doMC) {
		if (doEtaToys) {
			for (int y=0;y<nEtaSteps;y++) {
				R4ConeEM_etaSlicesNoiseMC[y]->Write();
				R4ConeLC_etaSlicesNoiseMC[y]->Write();
			}
			if (doSecondary) {
				for (int y=0;y<nEtaStepsSecondary;y++) {
					R4ConeEM_etaSlicesNoiseSecondaryMC[y]->Write();
					R4ConeLC_etaSlicesNoiseSecondaryMC[y]->Write();
				}
			}

		}
		if (doNPVToys) {
			for (int y=minNPV;y<maxNPV;y++) {
				R4ConeEM_NoiseAsFcnOfNPVMC[y-minNPV]->Write();
				R4ConeLC_NoiseAsFcnOfNPVMC[y-minNPV]->Write();
			}
			if (doSecondary) {
				for (int y=minNPV;y<maxNPV;y++) {
					R4ConeEM_NoiseAsFcnOfNPVSecondaryMC[y-minNPV]->Write();
					R4ConeLC_NoiseAsFcnOfNPVSecondaryMC[y-minNPV]->Write();
				}
			}
		}
		if (doMuNPVequalsAllToys) {
			for (int y=minMu;y<maxMu;y++) {
				R4ConeEM_NoiseAsFcnOfMuNPVAllMC[y-minMu]->Write();
				R4ConeLC_NoiseAsFcnOfMuNPVAllMC[y-minMu]->Write();
			}
			if (doSecondary) {
				for (int y=minMu;y<maxMu;y++) {
					R4ConeEM_NoiseAsFcnOfMuNPVAllSecondaryMC[y-minMu]->Write();
					R4ConeLC_NoiseAsFcnOfMuNPVAllSecondaryMC[y-minMu]->Write();
				}
			}
		}
		if (doMuNPVFixedToys) {
			for (int y=minNPV;y<maxNPV;y++) {
				for (int z=minMu;z<maxMu;z++) {
					R4ConeEM_NoiseAsFcnOfMuNPVFixedMC[y-minNPV][z-minMu]->Write();
					R4ConeLC_NoiseAsFcnOfMuNPVFixedMC[y-minNPV][z-minMu]->Write();
				}
			}
			if (doSecondary) {
				for (int y=minNPV;y<maxNPV;y++) {
					for (int z=minMu;z<maxMu;z++) {
						R4ConeEM_NoiseAsFcnOfMuNPVFixedSecondaryMC[y-minNPV][z-minMu]->Write();
						R4ConeLC_NoiseAsFcnOfMuNPVFixedSecondaryMC[y-minNPV][z-minMu]->Write();
					}
				}
			}
		}
	}
	outputFile->Close();






}