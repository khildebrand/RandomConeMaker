bool debug=false;

void ATLASLabel3(Double_t x,Double_t y,const char* text,double labelOffset=1.,double tsize=.08,Color_t color = 1);
void ATLASLabel3(Double_t x,Double_t y,const char* text,double labelOffset,double tsize,Color_t color)
{
  TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize);
  l.SetTextSize(tsize);
  l.SetNDC();
  l.SetTextFont(72);
  l.SetTextColor(color);

  double delx = labelOffset*0.115*696*gPad->GetWh()/(472*gPad->GetWw());

  l.DrawLatex(x,y,"ATLAS");
  if (text) {
    TLatex p;
    p.SetTextSize(tsize);
    p.SetNDC();
    p.SetTextFont(42);
    p.SetTextColor(color);
    p.DrawLatex(x+delx,y,text);
  }
}
void saveSingleHisto(TH1* histoOrig,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale=false,bool rebin=false);
void saveSingleHisto(TH1* histoOrig,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);

	if (logScale) {
		c1->SetLogy();
	}
	TLatex latex;
	latex.SetNDC();
	latex.SetTextColor(1);
	latex.SetTextFont(42);
	latex.SetTextSize(.04);

	if (normalizedArea) {
		/*cout<<stack->GetNbinsX()<<" " <<dataHisto->GetNbinsX()<<endl;
		cout<<stack->Integral()<< " " << dataHisto->Integral()<<endl;
		stack->Scale(dataHisto->Integral()/(stack->Integral()));
		cout<<stack->Integral()<<endl;
		//dataHisto->Scale(1/(dataHisto->Integral()));*/

		cout<<histo->GetNbinsX()<<endl;
		cout<<histo->Integral(1,histo->GetNbinsX())<<endl;
		histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
		cout<<histo->Integral(1,histo->GetNbinsX())<<endl;
		//dataHisto->Scale(1/(dataHisto->Integral()));
		//stuffToWrite.push_back("Period A4");
	}
	if (rebin) {
		histo->Rebin();
	}

	histo->Draw("HIST");
	if (logScale) {
		histo->SetMinimum(.2);
		histo->SetMaximum(histo->GetMaximum()*1000);
	}
	histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
	histo->GetXaxis()->SetLabelSize(0.04);
	histo->GetXaxis()->SetNdivisions(505);
	histo->GetXaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
	histo->GetYaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitleOffset(1.75);
	//dataHisto->GetYaxis()->SetLabelSize(.04);
	histo->GetYaxis()->SetLabelSize(.04);


	ATLASLabel3(xStart,yStart,"Internal",.85,.04);
	Double_t x=xStart;Double_t y=yStart-.045;
	for (int j=0;j<stuffToWrite.size();j++) {
		latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
		y-=.045;
	}

	std::string temp(histo->GetName());
	if (rebin) {
		temp=temp+"rebin";
	}
	c1->Print((outPutDir+temp+".png").c_str(),"png");
	c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");

}
void saveTwoHisto(TH1* histoOrig,TH1* histoOrig2,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale=false,bool rebin=false);
void saveTwoHisto(TH1* histoOrig,TH1* histoOrig2,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	TH1D *histo2 = (TH1D*)histoOrig2->Clone();
	TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);

	if (logScale) {
		c1->SetLogy();
	}
	TLatex latex;
	latex.SetNDC();
	latex.SetTextColor(1);
	latex.SetTextFont(42);
	latex.SetTextSize(.04);

	if (normalizedArea) {
		histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
		histo2->Scale(1/(histo2->Integral(1,histo2->GetNbinsX())));

	}
	if (rebin) {
		histo->Rebin();
		histo2->Rebin();
	}

	histo->Draw("HIST");
	histo2->SetLineColor(2);
	histo2->Draw("HIST same");
	if (logScale) {
		histo->SetMinimum(.2);
		histo->SetMaximum(histo->GetMaximum()*1000);
	}
	histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
	histo->GetXaxis()->SetLabelSize(0.04);
	histo->GetXaxis()->SetNdivisions(505);
	histo->GetXaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
	histo->GetYaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitleOffset(1.75);
	//dataHisto->GetYaxis()->SetLabelSize(.04);
	histo->GetYaxis()->SetLabelSize(.04);

	legend->Draw();
	ATLASLabel3(xStart,yStart,"Internal",.85,.04);
	Double_t x=xStart;Double_t y=yStart-.045;
	for (int j=0;j<stuffToWrite.size();j++) {
		latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
		y-=.045;
	}

	std::string temp(histo->GetName());
	if (rebin) {
		temp=temp+"rebin";
	}
	if (!normalizedArea) {
		temp=temp+"NotNormalized";
	}
	c1->Print((outPutDir+temp+".png").c_str(),"png");
	c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");

}
void saveTwoHistoMarker(TH1* histoOrig,TH1* histoOrig2,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,Double_t topMultiplier=1.33,bool logScale=false,bool rebin=false);
void saveTwoHistoMarker(TH1* histoOrig,TH1* histoOrig2,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,Double_t topMultiplier,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	TH1D *histo2 = (TH1D*)histoOrig2->Clone();
	if (histo->GetEntries()+histo2->GetEntries()==0) {
		cout<<"histogram was empty"<<endl;
	} else {
		TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);

		if (logScale) {
			c1->SetLogy();
		}
		TLatex latex;
		latex.SetNDC();
		latex.SetTextColor(1);
		latex.SetTextFont(42);
		latex.SetTextSize(.04);

		if (normalizedArea) {
			histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
			histo2->Scale(1/(histo2->Integral(1,histo2->GetNbinsX())));

		}
		if (rebin) {
			histo->Rebin();
			histo2->Rebin();
		}

		histo->Draw("HIST PE");
		//histo->GetXaxis()->SetNdivisions(30);
		//histo2->SetLineColor(4);
		histo2->Draw("PE same HIST");
		Double_t yMax=0;
		Double_t yMin=0;
		if (histo->GetMaximum()>histo2->GetMaximum()) {
			yMax=histo->GetMaximum()*topMultiplier;
		} else {
			yMax=histo2->GetMaximum()*topMultiplier;
		}
		if (histo->GetMinimum()>histo2->GetMinimum()) {
			yMin=histo2->GetMinimum()*.95;
		} else {
			yMin=histo->GetMinimum()*.95;
		}
		histo->SetMinimum(yMin);
		histo->SetMaximum(yMax);
		if (logScale) {
			histo->SetMinimum(.2);
			histo->SetMaximum(histo->GetMaximum()*1000);
		}
		histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
		histo->GetXaxis()->SetLabelSize(0.04);
		//histo->GetXaxis()->SetNdivisions(505);
		//histo->GetXaxis()->SetLimits(-4.5,4.5);

		histo->GetXaxis()->SetTitleSize(.04);
		histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
		histo->GetYaxis()->SetTitleSize(.04);
		histo->GetYaxis()->SetTitleOffset(1.75);
		//dataHisto->GetYaxis()->SetLabelSize(.04);
		histo->GetYaxis()->SetLabelSize(.04);

		legend->Draw();
		ATLASLabel3(xStart,yStart,"Internal",.85,.04);
		Double_t x=xStart;Double_t y=yStart-.045;
		for (int j=0;j<stuffToWrite.size();j++) {
			latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
			y-=.045;
		}

		std::string temp(histo->GetName());
		if (rebin) {
			temp=temp+"rebin";
		}
		c1->Print((outPutDir+temp+".png").c_str(),"png");
		c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");
	}

}
void saveFourHistoMarker(TH1* histoOrig,TH1* histoOrig2,TH1* histoOrig3,TH1* histoOrig4,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale=false,bool rebin=false);
void saveFourHistoMarker(TH1* histoOrig,TH1* histoOrig2,TH1* histoOrig3,TH1* histoOrig4,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	TH1D *histo2 = (TH1D*)histoOrig2->Clone();
	TH1D *histo3 = (TH1D*)histoOrig3->Clone();
	TH1D *histo4 = (TH1D*)histoOrig4->Clone();
	if (histo->GetEntries()+histo2->GetEntries()+histo3->GetEntries()+histo4->GetEntries()==0) {
		cout<<"histograms were empty"<<endl;
	} else {
		TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);

		if (logScale) {
			c1->SetLogy();
		}
		TLatex latex;
		latex.SetNDC();
		latex.SetTextColor(1);
		latex.SetTextFont(42);
		latex.SetTextSize(.04);

		if (normalizedArea) {
			histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
			histo2->Scale(1/(histo2->Integral(1,histo2->GetNbinsX())));
			histo3->Scale(1/(histo3->Integral(1,histo3->GetNbinsX())));
			histo4->Scale(1/(histo4->Integral(1,histo4->GetNbinsX())));

		}
		if (rebin) {
			histo->Rebin();
			histo2->Rebin();
			histo3->Rebin();
			histo4->Rebin();
		}

		histo->Draw("HIST PE");
		//histo->GetXaxis()->SetNdivisions(30);
		//histo2->SetLineColor(4);
		histo2->Draw("PE same HIST");
		histo3->Draw("PE same HIST");
		histo4->Draw("PE same HIST");
		Double_t yMax=histo->GetMaximum();
		Double_t yMin=histo->GetMinimum();

		if (histo2->GetMaximum()>yMax) {
			yMax=histo2->GetMaximum();
		}
		if (histo3->GetMaximum()>yMax) {
			yMax=histo3->GetMaximum();
		}
		if (histo4->GetMaximum()>yMax) {
			yMax=histo4->GetMaximum();
		}
		//cout <<"yMax="<<yMax<<endl;
		yMax=yMax*1.33;

		if (histo2->GetMinimum()<yMin) {
			yMin=histo2->GetMinimum();
		}
		if (histo3->GetMinimum()<yMin) {
			yMin=histo3->GetMinimum();
		}
		if (histo4->GetMinimum()<yMin) {
			yMin=histo4->GetMinimum();
		}
		//cout <<"ymin="<<yMin<<endl;
		yMin=yMin*.95;
		//cout <<"ymin="<<yMin<<"  ymax="<<yMax<<endl;
		histo->SetMinimum(yMin);
		histo->SetMaximum(yMax);
		if (logScale) {
			histo->SetMinimum(.2);
			histo->SetMaximum(histo->GetMaximum()*1000);
		}
		histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
		histo->GetXaxis()->SetLabelSize(0.04);
		//histo->GetXaxis()->SetNdivisions(505);
		//histo->GetXaxis()->SetLimits(-4.5,4.5);

		histo->GetXaxis()->SetTitleSize(.04);
		histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
		histo->GetYaxis()->SetTitleSize(.04);
		histo->GetYaxis()->SetTitleOffset(1.75);
		//dataHisto->GetYaxis()->SetLabelSize(.04);
		histo->GetYaxis()->SetLabelSize(.04);

		legend->Draw();
		ATLASLabel3(xStart,yStart,"Internal",.85,.04);
		Double_t x=xStart;Double_t y=yStart-.045;
		for (int j=0;j<stuffToWrite.size();j++) {
			latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
			y-=.045;
		}

		std::string temp(histo->GetName());
		if (rebin) {
			temp=temp+"rebin";
		}
		c1->Print((outPutDir+temp+".png").c_str(),"png");
		c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");
	}

}
void saveTwoHistoRatio(TH1* histoOrig,TH1* histoOrig2,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale=false,bool rebin=false);
void saveTwoHistoRatio(TH1* histoOrig,TH1* histoOrig2,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	TH1D *histo2 = (TH1D*)histoOrig2->Clone();
	TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);
	TPad *pad1 = new TPad("pad1","pad1",0,0.35,1,1);
	pad1->SetBottomMargin(0.0);
	pad1->SetLeftMargin(0.185);
	pad1->Draw();
	c1->cd();
	TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.35);
	pad2->SetTopMargin(0.0);
	//pad2->SetBottomMargin(0.15);
	pad2->SetBottomMargin(0.3);
	pad2->SetLeftMargin(0.185);
	pad2->Draw();
	//pad2->SetGridy();

	pad1->cd();
	if (logScale) {
		c1->SetLogy();
	}
	TLatex latex;
	latex.SetNDC();
	latex.SetTextColor(1);
	latex.SetTextFont(42);
	latex.SetTextSize(.04);

	if (normalizedArea) {
		histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
		histo2->Scale(1/(histo2->Integral(1,histo2->GetNbinsX())));

	}
	if (rebin) {
		histo->Rebin();
		histo2->Rebin();
	}

	histo->Draw("hist");
	histo2->SetLineColor(2);
	histo2->Draw("hist same");
	if (logScale) {
		histo->SetMinimum(.2);
		histo->SetMaximum(histo->GetMaximum()*1000);
	}
	histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
	histo->GetXaxis()->SetLabelSize(0);
	histo->GetXaxis()->SetNdivisions(505);
	histo->GetXaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
	histo->GetYaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitleOffset(1.75);
	//dataHisto->GetYaxis()->SetLabelSize(.04);
	histo->GetYaxis()->SetLabelSize(.04);


	ATLASLabel3(xStart,yStart,"Internal",.85,.04);
	Double_t x=xStart;Double_t y=yStart-.045;
	for (int j=0;j<stuffToWrite.size();j++) {
		latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
		y-=.045;
	}
	pad2->cd();

	TH1F *h1_ratio = (TH1F*)histo->Clone();
	//h1_ratio->Add(h1_ratio,stack1,1,-1);
	h1_ratio->Divide(histo2);
	//h1_ratio->SetMarkerColor(colourAdd);
	//h1_ratio->GetYaxis()->SetRangeUser(0, 2.);
	h1_ratio->GetYaxis()->SetRangeUser(.5, 1.5);
	h1_ratio->GetYaxis()->SetTitle("Pos/Neg #eta");
	//h1_ratio->SetMaximum(.19);
	//h1_ratio->SetMinimum(-.19);
	h1_ratio->GetYaxis()->SetTitleSize(.12);
	h1_ratio->GetYaxis()->SetTitleOffset(.70);
	h1_ratio->GetYaxis()->SetLabelSize(.11);
	h1_ratio->GetXaxis()->SetTitle(xaxisTitle.c_str());
	h1_ratio->GetXaxis()->SetLabelSize(.11);
	h1_ratio->GetXaxis()->SetTitleOffset(1.1);
	/*h1_ratio->GetYaxis()->SetTitleSize(.08);
	h1_ratio->GetYaxis()->SetTitleOffset(.8);
	h1_ratio->GetYaxis()->SetLabelSize(.08);
	h1_ratio->GetXaxis()->SetTitle(xaxisTitle.c_str());
	h1_ratio->GetXaxis()->SetLabelSize(.08);
	h1_ratio->GetXaxis()->SetTitleOffset(.8);*/
	h1_ratio->GetYaxis()->SetNdivisions(10);
	//h1_ratio->GetYaxis()->SetTitleOffset(1.85);
	//h1_ratio->GetYaxis()->SetTitleFont(43);
	//h1_ratio->GetYaxis()->SetLabelSize(0);
	//h1_ratio->GetYaxis()->SetTitleSize(28);
	//h1_ratio->GetXaxis()->SetLabelFont(43);
	//h1_ratio->GetXaxis()->SetTitleFont(43);
	//h1_ratio->GetXaxis()->SetLabelSize(28);
	h1_ratio->GetXaxis()->SetTitleSize(.12);
	//h1_ratio->GetXaxis()->SetRangeUser(xmin, xmax);
	//TGaxis *ratioaxis = new TGaxis(xmin, 0.5, xmin, 1.5, 0.5, 1.5, 503, "");
	//ratioaxis->SetLabelFont(43);
	//ratioaxis->SetLabelSize(28);

	h1_ratio->Draw("P");


	TLine *line=new TLine(h1_ratio->GetXaxis()->GetXmin(),1,h1_ratio->GetXaxis()->GetXmax(),1);
	line->SetLineStyle(7);
	line->Draw();
	//ratioaxis->Draw();
	//cout<<"before save"<<endl;


	std::string temp(histo->GetName());
	if (rebin) {
		temp=temp+"rebin";
	}
	temp=temp+"ratio";
	c1->Print((outPutDir+temp+".png").c_str(),"png");
	c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");

}
void saveTwoHistoRatioAsy(TH1* histoOrig,TH1* histoOrig2,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale=false,bool rebin=false);
void saveTwoHistoRatioAsy(TH1* histoOrig,TH1* histoOrig2,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	TH1D *histo2 = (TH1D*)histoOrig2->Clone();
	TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);
	TPad *pad1 = new TPad("pad1","pad1",0,0.35,1,1);
	pad1->SetBottomMargin(0.0);
	pad1->SetLeftMargin(0.185);
	pad1->Draw();
	c1->cd();
	TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.35);
	pad2->SetTopMargin(0.0);
	//pad2->SetBottomMargin(0.15);
	pad2->SetBottomMargin(0.3);
	pad2->SetLeftMargin(0.185);
	pad2->Draw();
	//pad2->SetGridy();

	pad1->cd();
	if (logScale) {
		c1->SetLogy();
	}
	TLatex latex;
	latex.SetNDC();
	latex.SetTextColor(1);
	latex.SetTextFont(42);
	latex.SetTextSize(.04);
	Double_t chi2=histo->Chi2Test(histo2,"CHI2");
	Double_t chi2ndf=histo->Chi2Test(histo2,"CHI2/NDF");
	Double_t ksTest=histo->KolmogorovTest(histo2);
	stuffToWrite.push_back("CHI2="+std::to_string(chi2));
	stuffToWrite.push_back("chi2ndf="+std::to_string(chi2ndf));
	stuffToWrite.push_back("ksTest="+std::to_string(ksTest));
	if (normalizedArea) {
		histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
		histo2->Scale(1/(histo2->Integral(1,histo2->GetNbinsX())));

	}
	if (rebin) {
		histo->Rebin();
		histo2->Rebin();
	}

	histo->Draw("hist");
	histo2->SetLineColor(2);
	histo2->Draw("hist same");
	legend->Draw();
	if (logScale) {
		histo->SetMinimum(.2);
		histo->SetMaximum(histo->GetMaximum()*1000);
	}
	histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
	histo->GetXaxis()->SetLabelSize(0);
	histo->GetXaxis()->SetNdivisions(505);
	histo->GetXaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
	histo->GetYaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitleOffset(1.75);
	//dataHisto->GetYaxis()->SetLabelSize(.04);
	histo->GetYaxis()->SetLabelSize(.04);


	ATLASLabel3(xStart,yStart,"Internal",.85,.04);
	Double_t x=xStart;Double_t y=yStart-.045;
	for (int j=0;j<stuffToWrite.size();j++) {
		latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
		y-=.045;
	}
	pad2->cd();

	TH1F *h1_ratio = (TH1F*)histo->Clone();
	TH1F *h1_den=(TH1F*)histo->Clone();
	h1_den->Add(histo2);
	//h1_ratio->Add(h1_ratio,stack1,1,-1);
	h1_ratio->Add(histo2,-1);
	h1_ratio->Divide(h1_den);
	//h1_ratio->SetMarkerColor(colourAdd);
	//h1_ratio->GetYaxis()->SetRangeUser(0, 2.);
	h1_ratio->GetYaxis()->SetRangeUser(-.5, .5);
	h1_ratio->GetYaxis()->SetTitle("(Pos-Neg)/(Pos+Neg) #eta");
	//h1_ratio->SetMaximum(.19);
	//h1_ratio->SetMinimum(-.19);
	h1_ratio->GetXaxis()->SetTitle(xaxisTitle.c_str());
	h1_ratio->GetXaxis()->SetTitleSize(.075);
	h1_ratio->GetXaxis()->SetLabelSize(.075);
	//h1_ratio->GetYaxis()->SetTitle(yaxisTitle.c_str());
	h1_ratio->GetYaxis()->SetTitleSize(.075);
	h1_ratio->GetYaxis()->SetTitleOffset(1.0);
	//dataHisto->GetYaxis()->SetLabelSize(.04);
	h1_ratio->GetYaxis()->SetLabelSize(.075);

	/*h1_ratio->GetYaxis()->SetTitleSize(.08);
	h1_ratio->GetYaxis()->SetTitleOffset(.8);
	h1_ratio->GetYaxis()->SetLabelSize(.08);
	h1_ratio->GetXaxis()->SetTitle(xaxisTitle.c_str());
	h1_ratio->GetXaxis()->SetLabelSize(.08);
	h1_ratio->GetXaxis()->SetTitleOffset(.8);*/
	h1_ratio->GetYaxis()->SetNdivisions(5);
	//h1_ratio->GetXaxis()->SetRangeUser(xmin, xmax);
	//TGaxis *ratioaxis = new TGaxis(xmin, 0.5, xmin, 1.5, 0.5, 1.5, 503, "");
	//ratioaxis->SetLabelFont(43);
	//ratioaxis->SetLabelSize(28);

	h1_ratio->Draw("P");


	TLine *line=new TLine(h1_ratio->GetXaxis()->GetXmin(),1,h1_ratio->GetXaxis()->GetXmax(),1);
	line->SetLineStyle(7);
	line->Draw();
	//ratioaxis->Draw();
	//cout<<"before save"<<endl;


	std::string temp(histo->GetName());
	if (rebin) {
		temp=temp+"rebin";
	}
	temp=temp+"Asy";
	c1->Print((outPutDir+temp+".png").c_str(),"png");
	c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");

}
Double_t getWidth(TH1D* histo) {
	int mean_bin = histo->FindBin(histo->GetMean());
	if (debug==true) {
		cout<< "mean_bin="<<mean_bin<<" mean="<<histo->GetMean()<<endl;
	}

    int binup = mean_bin;
    int bindown = mean_bin-1;
    Double_t total = histo->Integral();
    Double_t runningup = 0;
    Double_t runningdown = 0;
    if (total<50) {
		return -10.;
	}
    while (runningup < 0.6827/2.0*total) {
        runningup += histo->GetBinContent(binup);
        binup += 1;
	}
    while (runningdown < 0.6827/2.0*total) {
        runningdown += histo->GetBinContent(bindown);
        bindown -= 1;
	}

    Double_t width = (histo->GetBinCenter(binup)-histo->GetBinCenter(bindown))/2.;
    return width/1000.;
}
void generateToy(TH1D* histoReal,TH1D* histoToy,int toyType) {
	TRandom *r1=new TRandom1();
	r1->SetSeed(0);
	if (toyType==0) {
		int numberOfEventsToFill=r1->Poisson(histoReal->GetEntries());
		if (debug) {
			cout<<"number of Events in real histo="<<histoReal->GetEntries()<<endl;
			cout<<"numberOfEventsToFill="<<numberOfEventsToFill<<endl;
		}
		for (int x=0;x<numberOfEventsToFill;x++) {
			histoToy->Fill(histoReal->GetRandom());
		}
	} else if (toyType==1) {
		//idiot check
		if (histoReal->GetNbinsX()!=histoToy->GetNbinsX()) {
			cout <<"AHHHH!!!!!!!!!!!!!!!!!!!!!!!!!!!!! The toy and real histograms dont have the same number of bins!!!"<<endl;
		}
		for (int bin=1;bin<=histoReal->GetNbinsX();bin++) {
			Double_t value = histoReal->GetBinContent(bin);
			Double_t error = histoReal->GetBinError(bin);
			histoToy->SetBinContent(bin,r1->Gaus(value,error));
		}
	}

}
void ToyTest2Result(const std::string& configFile) {
	TEnv *config = new TEnv(configFile.c_str());
	if ( !config ) {
		printf("configFile=%s doesn't appear to exists\n",configFile.c_str());
	}
	std::string outPutDir = config->GetValue("outPutDir","blah");
	std::string OutputFileToyNoise = config->GetValue("OutputFileToyNoise","blah");
	std::string OutputFile2 = config->GetValue("OutputFile2","blah");
	std::string ToyTestInputFile = config->GetValue("ToyTestInputFile","blah");

	bool doData = config->GetValue("doData",true);
	bool doMC = config->GetValue("doMC",true);
	bool doSecondary = config->GetValue("doSecondary",false);
	int startEtaStep = config->GetValue("startEtaStep",0);
	int nEtaStepsToTake = config->GetValue("nEtaStepsToTake",1);
	int nEtaStepsToTakeSecondary = config->GetValue("nEtaStepsToTakeSecondary",1);
	int errorType = config->GetValue("errorType",1);
	int rangeLowerNoise = config->GetValue("rangeLowerNoise",0);
	int rangeUpperNoise = config->GetValue("rangeUpperNoise",10);
	bool doEtaToys = config->GetValue("doEtaToys",false);
	bool doMuNPVequalsAllToys = config->GetValue("doMuNPVequalsAllToys",false);
	bool doMuNPVFixedToys = config->GetValue("doMuNPVFixedToys",false);
	bool doNPVToys = config->GetValue("doNPVToys",false);
	debug = config->GetValue("debug",false);
	int maxNPV = config->GetValue("maxNPV",30);
	int maxMu = config->GetValue("maxMu",35);
	int minNPV = config->GetValue("minNPV",0);
	int minMu = config->GetValue("minMu",0);

	std::string etaBlocksString= config->GetValue("etaBlocks","0,.8");
	std::string etaBlocksSecondaryString= config->GetValue("etaBlocksSecondary","0,2.1");

	std::string temp;

	int nEtaSteps=-1;
	printf("etaBlocksString=%s\n",etaBlocksString.c_str());
	stringstream ss(etaBlocksString);
	vector<double> etaBlocks;
	while(getline(ss,temp,',')) {
		etaBlocks.push_back(std::stod(temp));
		nEtaSteps++;
	}
	for (int i=0;i<etaBlocks.size();i++) {
		printf("etaBlocks[%i]=%f\n",i,etaBlocks[i]);
	}
	int nEtaStepsSecondary=-1;
	printf("etaBlocksSecondaryString=%s\n",etaBlocksSecondaryString.c_str());
	stringstream ss2(etaBlocksSecondaryString);
	vector<double> etaBlocksSecondary;
	while(getline(ss2,temp,',')) {
		etaBlocksSecondary.push_back(std::stod(temp));
		nEtaStepsSecondary++;
	}
	for (int i=0;i<etaBlocksSecondary.size();i++) {
		printf("etaBlocksSecondary[%i]=%f\n",i,etaBlocksSecondary[i]);
	}

	SetAtlasStyle();

	int histNameInt=0;

	//these are what hold all the noise terms from each toy
	//as fcn of eta
	TH1D *R4ConeEM_etaSlicesNoise[nEtaSteps];
	TH1D *R4ConeLC_etaSlicesNoise[nEtaSteps];

	TH1D *R4ConeEM_etaSlicesNoiseSecondary[nEtaSteps];
	TH1D *R4ConeLC_etaSlicesNoiseSecondary[nEtaSteps];

	TH1D *R4ConeEM_etaSlicesNoiseMC[nEtaSteps];
	TH1D *R4ConeLC_etaSlicesNoiseMC[nEtaSteps];

	TH1D *R4ConeEM_etaSlicesNoiseSecondaryMC[nEtaSteps];
	TH1D *R4ConeLC_etaSlicesNoiseSecondaryMC[nEtaSteps];

	//as fcn of NPV
	TH1D *R4ConeEM_NoiseAsFcnOfNPV[maxNPV-minNPV];
	TH1D *R4ConeLC_NoiseAsFcnOfNPV[maxNPV-minNPV];

	TH1D *R4ConeEM_NoiseAsFcnOfNPVSecondary[maxNPV-minNPV];
	TH1D *R4ConeLC_NoiseAsFcnOfNPVSecondary[maxNPV-minNPV];

	TH1D *R4ConeEM_NoiseAsFcnOfNPVMC[maxNPV-minNPV];
	TH1D *R4ConeLC_NoiseAsFcnOfNPVMC[maxNPV-minNPV];

	TH1D *R4ConeEM_NoiseAsFcnOfNPVSecondaryMC[maxNPV-minNPV];
	TH1D *R4ConeLC_NoiseAsFcnOfNPVSecondaryMC[maxNPV-minNPV];

	//as fcn of mu with no restriction on NPV
	TH1D *R4ConeEM_NoiseAsFcnOfMuNPVAll[maxMu-minMu];
	TH1D *R4ConeLC_NoiseAsFcnOfMuNPVAll[maxMu-minMu];

	TH1D *R4ConeEM_NoiseAsFcnOfMuNPVAllSecondary[maxMu-minMu];
	TH1D *R4ConeLC_NoiseAsFcnOfMuNPVAllSecondary[maxMu-minMu];

	TH1D *R4ConeEM_NoiseAsFcnOfMuNPVAllMC[maxMu-minMu];
	TH1D *R4ConeLC_NoiseAsFcnOfMuNPVAllMC[maxMu-minMu];

	TH1D *R4ConeEM_NoiseAsFcnOfMuNPVAllSecondaryMC[maxMu-minMu];
	TH1D *R4ConeLC_NoiseAsFcnOfMuNPVAllSecondaryMC[maxMu-minMu];

	//as fcn of mu with NPV restricted to single value
	TH1D *R4ConeEM_NoiseAsFcnOfMuNPVFixed[maxNPV-minNPV][maxMu-minMu];
	TH1D *R4ConeLC_NoiseAsFcnOfMuNPVFixed[maxNPV-minNPV][maxMu-minMu];

	TH1D *R4ConeEM_NoiseAsFcnOfMuNPVFixedSecondary[maxNPV-minNPV][maxMu-minMu];
	TH1D *R4ConeLC_NoiseAsFcnOfMuNPVFixedSecondary[maxNPV-minNPV][maxMu-minMu];

	TH1D *R4ConeEM_NoiseAsFcnOfMuNPVFixedMC[maxNPV-minNPV][maxMu-minMu];
	TH1D *R4ConeLC_NoiseAsFcnOfMuNPVFixedMC[maxNPV-minNPV][maxMu-minMu];

	TH1D *R4ConeEM_NoiseAsFcnOfMuNPVFixedSecondaryMC[maxNPV-minNPV][maxMu-minMu];
	TH1D *R4ConeLC_NoiseAsFcnOfMuNPVFixedSecondaryMC[maxNPV-minNPV][maxMu-minMu];

	//these are the noise term real ones from save plots
	//as fcn of noise
	TH1D* NoiseAsFcnEta4EM;
	TH1D* NoiseAsFcnEta4LC;

	TH1D* NoiseAsFcnEta4EMSecondary;
	TH1D* NoiseAsFcnEta4LCSecondary;

	TH1D* MCNoiseAsFcnEta4EM;
	TH1D* MCNoiseAsFcnEta4LC;

	TH1D* MCNoiseAsFcnEta4EMSecondary;
	TH1D* MCNoiseAsFcnEta4LCSecondary;

	//as fcn NPV
	TH1D *NoiseAsFcnNPV4EM;
	TH1D *NoiseAsFcnNPV4LC;

	TH1D *NoiseAsFcnNPV4EMSecondary;
	TH1D *NoiseAsFcnNPV4LCSecondary;

	TH1D *MCNoiseAsFcnNPV4EM;
	TH1D *MCNoiseAsFcnNPV4LC;

	TH1D *MCNoiseAsFcnNPV4EMSecondary;
	TH1D *MCNoiseAsFcnNPV4LCSecondary;

	//as fcn of mu with no restriction on NPV
	TH1D *NoiseAsFcnMu4EMAll;
	TH1D *NoiseAsFcnMu4LCAll;

	TH1D *NoiseAsFcnMu4EMAllSecondary;
	TH1D *NoiseAsFcnMu4LCAllSecondary;

	TH1D *MCNoiseAsFcnMu4EMAll;
	TH1D *MCNoiseAsFcnMu4LCAll;

	TH1D *MCNoiseAsFcnMu4EMAllSecondary;
	TH1D *MCNoiseAsFcnMu4LCAllSecondary;

	//as fcn of mu with NPV restricted to single value
	TH1D *NoiseAsFcnMu4EMNPV[maxNPV-minNPV];
	TH1D *NoiseAsFcnMu4LCNPV[maxNPV-minNPV];

	TH1D *NoiseAsFcnMu4EMNPVSecondary[maxNPV-minNPV];
	TH1D *NoiseAsFcnMu4LCNPVSecondary[maxNPV-minNPV];

	TH1D *MCNoiseAsFcnMu4EMNPV[maxNPV-minNPV];
	TH1D *MCNoiseAsFcnMu4LCNPV[maxNPV-minNPV];

	TH1D *MCNoiseAsFcnMu4EMNPVSecondary[maxNPV-minNPV];
	TH1D *MCNoiseAsFcnMu4LCNPVSecondary[maxNPV-minNPV];
	cout<<"heyo1a"<<endl;

	TFile *infile;

	infile = TFile::Open((ToyTestInputFile).c_str());
	cout<<"heyoA"<<endl;
	if (doData) {
		NoiseAsFcnEta4EM=(TH1D*)infile->Get("NoiseAsFcnEta4EM")->Clone();
		NoiseAsFcnEta4EM->SetDirectory(0);

		cout<<"heyoB"<<endl;

		NoiseAsFcnEta4LC=(TH1D*)infile->Get("NoiseAsFcnEta4LC")->Clone();
		NoiseAsFcnEta4LC->SetDirectory(0);
		cout<<"heyoC"<<endl;
		NoiseAsFcnNPV4EM=(TH1D*)infile->Get("NoiseAsFcnNPV4EM")->Clone();
		NoiseAsFcnNPV4EM->SetDirectory(0);
		cout<<"heyoD"<<endl;
		NoiseAsFcnNPV4LC=(TH1D*)infile->Get("NoiseAsFcnNPV4LC")->Clone();
		NoiseAsFcnNPV4LC->SetDirectory(0);

		cout<<"heyoE"<<endl;
		NoiseAsFcnMu4EMAll=(TH1D*)infile->Get("NoiseAsFcnMu4EMAll")->Clone();
		NoiseAsFcnMu4EMAll->SetDirectory(0);
		cout<<"heyoF"<<endl;
		NoiseAsFcnMu4LCAll=(TH1D*)infile->Get("NoiseAsFcnMu4LCAll")->Clone();
		NoiseAsFcnMu4LCAll->SetDirectory(0);

		for (int y=minNPV;y<maxNPV;y++) {
			NoiseAsFcnMu4EMNPV[y-minNPV]=(TH1D*)infile->Get(("NoiseAsFcnMu4EMNPV"+std::to_string(y)).c_str())->Clone();
			NoiseAsFcnMu4EMNPV[y-minNPV]->SetDirectory(0);
			NoiseAsFcnMu4LCNPV[y-minNPV]=(TH1D*)infile->Get(("NoiseAsFcnMu4LCNPV"+std::to_string(y)).c_str())->Clone();
			NoiseAsFcnMu4LCNPV[y-minNPV]->SetDirectory(0);
		}
		if (doSecondary) {
			NoiseAsFcnEta4EMSecondary=(TH1D*)infile->Get("NoiseAsFcnEta4EMSecondary")->Clone();
			NoiseAsFcnEta4EMSecondary->SetDirectory(0);

			cout<<"heyoB"<<endl;

			NoiseAsFcnEta4LCSecondary=(TH1D*)infile->Get("NoiseAsFcnEta4LCSecondary")->Clone();
			NoiseAsFcnEta4LCSecondary->SetDirectory(0);
			cout<<"heyoC"<<endl;
			NoiseAsFcnNPV4EMSecondary=(TH1D*)infile->Get("NoiseAsFcnNPV4EMSecondary")->Clone();
			NoiseAsFcnNPV4EMSecondary->SetDirectory(0);
			cout<<"heyoD"<<endl;
			NoiseAsFcnNPV4LCSecondary=(TH1D*)infile->Get("NoiseAsFcnNPV4LCSecondary")->Clone();
			NoiseAsFcnNPV4LCSecondary->SetDirectory(0);

			cout<<"heyoE"<<endl;
			NoiseAsFcnMu4EMAllSecondary=(TH1D*)infile->Get("NoiseAsFcnMu4EMAllSecondary")->Clone();
			NoiseAsFcnMu4EMAllSecondary->SetDirectory(0);
			cout<<"heyoF"<<endl;
			NoiseAsFcnMu4LCAllSecondary=(TH1D*)infile->Get("NoiseAsFcnMu4LCAllSecondary")->Clone();
			NoiseAsFcnMu4LCAllSecondary->SetDirectory(0);

			for (int y=minNPV;y<maxNPV;y++) {
				NoiseAsFcnMu4EMNPVSecondary[y-minNPV]=(TH1D*)infile->Get(("NoiseAsFcnMu4EMNPVSecondary"+std::to_string(y)).c_str())->Clone();
				NoiseAsFcnMu4EMNPVSecondary[y-minNPV]->SetDirectory(0);
				NoiseAsFcnMu4LCNPVSecondary[y-minNPV]=(TH1D*)infile->Get(("NoiseAsFcnMu4LCNPVSecondary"+std::to_string(y)).c_str())->Clone();
				NoiseAsFcnMu4LCNPVSecondary[y-minNPV]->SetDirectory(0);
			}
		}
	}
	if (doMC) {
		MCNoiseAsFcnEta4EM=(TH1D*)infile->Get("MCNoiseAsFcnEta4EM")->Clone();
		MCNoiseAsFcnEta4EM->SetDirectory(0);

		cout<<"heyoB"<<endl;

		MCNoiseAsFcnEta4LC=(TH1D*)infile->Get("MCNoiseAsFcnEta4LC")->Clone();
		MCNoiseAsFcnEta4LC->SetDirectory(0);
		cout<<"heyoC"<<endl;
		MCNoiseAsFcnNPV4EM=(TH1D*)infile->Get("MCNoiseAsFcnNPV4EM")->Clone();
		MCNoiseAsFcnNPV4EM->SetDirectory(0);
		cout<<"heyoD"<<endl;
		MCNoiseAsFcnNPV4LC=(TH1D*)infile->Get("MCNoiseAsFcnNPV4LC")->Clone();
		MCNoiseAsFcnNPV4LC->SetDirectory(0);

		cout<<"heyoE"<<endl;
		MCNoiseAsFcnMu4EMAll=(TH1D*)infile->Get("MCNoiseAsFcnMu4EMAll")->Clone();
		MCNoiseAsFcnMu4EMAll->SetDirectory(0);
		cout<<"heyoF"<<endl;
		MCNoiseAsFcnMu4LCAll=(TH1D*)infile->Get("MCNoiseAsFcnMu4LCAll")->Clone();
		MCNoiseAsFcnMu4LCAll->SetDirectory(0);

		for (int y=minNPV;y<maxNPV;y++) {
			MCNoiseAsFcnMu4EMNPV[y-minNPV]=(TH1D*)infile->Get(("MCNoiseAsFcnMu4EMNPV"+std::to_string(y)).c_str())->Clone();
			MCNoiseAsFcnMu4EMNPV[y-minNPV]->SetDirectory(0);
			MCNoiseAsFcnMu4LCNPV[y-minNPV]=(TH1D*)infile->Get(("MCNoiseAsFcnMu4LCNPV"+std::to_string(y)).c_str())->Clone();
			MCNoiseAsFcnMu4LCNPV[y-minNPV]->SetDirectory(0);
		}
		if (doSecondary) {
			MCNoiseAsFcnEta4EMSecondary=(TH1D*)infile->Get("MCNoiseAsFcnEta4EMSecondary")->Clone();
			MCNoiseAsFcnEta4EMSecondary->SetDirectory(0);

			cout<<"heyoB"<<endl;

			MCNoiseAsFcnEta4LCSecondary=(TH1D*)infile->Get("MCNoiseAsFcnEta4LCSecondary")->Clone();
			MCNoiseAsFcnEta4LCSecondary->SetDirectory(0);
			cout<<"heyoC"<<endl;
			MCNoiseAsFcnNPV4EMSecondary=(TH1D*)infile->Get("MCNoiseAsFcnNPV4EMSecondary")->Clone();
			MCNoiseAsFcnNPV4EMSecondary->SetDirectory(0);
			cout<<"heyoD"<<endl;
			MCNoiseAsFcnNPV4LCSecondary=(TH1D*)infile->Get("MCNoiseAsFcnNPV4LCSecondary")->Clone();
			MCNoiseAsFcnNPV4LCSecondary->SetDirectory(0);

			cout<<"heyoE"<<endl;
			MCNoiseAsFcnMu4EMAllSecondary=(TH1D*)infile->Get("MCNoiseAsFcnMu4EMAllSecondary")->Clone();
			MCNoiseAsFcnMu4EMAllSecondary->SetDirectory(0);
			cout<<"heyoF"<<endl;
			MCNoiseAsFcnMu4LCAllSecondary=(TH1D*)infile->Get("MCNoiseAsFcnMu4LCAllSecondary")->Clone();
			MCNoiseAsFcnMu4LCAllSecondary->SetDirectory(0);

			for (int y=minNPV;y<maxNPV;y++) {
				MCNoiseAsFcnMu4EMNPVSecondary[y-minNPV]=(TH1D*)infile->Get(("MCNoiseAsFcnMu4EMNPVSecondary"+std::to_string(y)).c_str())->Clone();
				MCNoiseAsFcnMu4EMNPVSecondary[y-minNPV]->SetDirectory(0);
				MCNoiseAsFcnMu4LCNPVSecondary[y-minNPV]=(TH1D*)infile->Get(("MCNoiseAsFcnMu4LCNPVSecondary"+std::to_string(y)).c_str())->Clone();
				MCNoiseAsFcnMu4LCNPVSecondary[y-minNPV]->SetDirectory(0);
			}
		}
	}
	infile->Close();
	infile = TFile::Open((OutputFileToyNoise).c_str());
	TF1 *gaus1    = new TF1("g1","gaus",rangeLowerNoise,rangeUpperNoise);

	Double_t tempSig;
	Double_t tempMean;
	cout<<"heyoG"<<endl;
	if (doData) {
		if (doEtaToys) {
			for (int y=0;y<nEtaSteps;y++) {
				R4ConeEM_etaSlicesNoise[y]=(TH1D*)infile->Get(("R4ConeEM_etaSlicesNoise"+std::to_string(y)+"_"+std::to_string(y+1)).c_str())->Clone();
				if (R4ConeEM_etaSlicesNoise[y]->Integral(1,R4ConeEM_etaSlicesNoise[y]->GetNbinsX())>=10) {
					if (errorType==0) {
						R4ConeEM_etaSlicesNoise[y]->Fit(gaus1,"R");
						tempSig=gaus1->GetParameter(2);
					} else if (errorType==1) {
						tempMean=R4ConeEM_etaSlicesNoise[y]->GetMean();
						tempSig=R4ConeEM_etaSlicesNoise[y]->GetRMS();
						NoiseAsFcnEta4EM->SetBinContent(y+1,tempMean);
					}
					NoiseAsFcnEta4EM->SetBinError(y+1,tempSig);

				} else {
					if (NoiseAsFcnEta4EM->GetBinContent(y+1)==0) {
						NoiseAsFcnEta4EM->SetBinError(y+1,0);
					} else {
						NoiseAsFcnEta4EM->SetBinError(y+1,10);
					}
				}

				R4ConeLC_etaSlicesNoise[y]=(TH1D*)infile->Get(("R4ConeLC_etaSlicesNoise"+std::to_string(y)+"_"+std::to_string(y+1)).c_str())->Clone();
				if (R4ConeLC_etaSlicesNoise[y]->Integral(1,R4ConeLC_etaSlicesNoise[y]->GetNbinsX())>=10) {
					if (errorType==0) {
						R4ConeLC_etaSlicesNoise[y]->Fit(gaus1,"R");
						tempSig=gaus1->GetParameter(2);
					} else if (errorType==1) {
						tempMean=R4ConeLC_etaSlicesNoise[y]->GetMean();
						tempSig=R4ConeLC_etaSlicesNoise[y]->GetRMS();
						NoiseAsFcnEta4LC->SetBinContent(y+1,tempMean);
					}
					NoiseAsFcnEta4LC->SetBinError(y+1,tempSig);
				} else {
					if (NoiseAsFcnEta4LC->GetBinContent(y+1)==0) {
						NoiseAsFcnEta4LC->SetBinError(y+1,0);
					} else {
						NoiseAsFcnEta4LC->SetBinError(y+1,10);
					}
				}
			}
			if (doSecondary) {
				for (int y=0;y<nEtaStepsSecondary;y++) {
					R4ConeEM_etaSlicesNoiseSecondary[y]=(TH1D*)infile->Get(("R4ConeEM_etaSlicesNoiseSecondary"+std::to_string(y)+"_"+std::to_string(y+1)).c_str())->Clone();
					if (R4ConeEM_etaSlicesNoiseSecondary[y]->Integral(1,R4ConeEM_etaSlicesNoiseSecondary[y]->GetNbinsX())>=10) {
						if (errorType==0) {
							R4ConeEM_etaSlicesNoiseSecondary[y]->Fit(gaus1,"R");
							tempSig=gaus1->GetParameter(2);
						} else if (errorType==1) {
							tempMean=R4ConeEM_etaSlicesNoiseSecondary[y]->GetMean();
							tempSig=R4ConeEM_etaSlicesNoiseSecondary[y]->GetRMS();
							NoiseAsFcnEta4EMSecondary->SetBinContent(y+1,tempMean);
						}
						NoiseAsFcnEta4EMSecondary->SetBinError(y+1,tempSig);

					} else {
						if (NoiseAsFcnEta4EMSecondary->GetBinContent(y+1)==0) {
							NoiseAsFcnEta4EMSecondary->SetBinError(y+1,0);
						} else {
							NoiseAsFcnEta4EMSecondary->SetBinError(y+1,10);
						}
					}

					R4ConeLC_etaSlicesNoiseSecondary[y]=(TH1D*)infile->Get(("R4ConeLC_etaSlicesNoiseSecondary"+std::to_string(y)+"_"+std::to_string(y+1)).c_str())->Clone();
					if (R4ConeLC_etaSlicesNoiseSecondary[y]->Integral(1,R4ConeLC_etaSlicesNoiseSecondary[y]->GetNbinsX())>=10) {
						if (errorType==0) {
							R4ConeLC_etaSlicesNoiseSecondary[y]->Fit(gaus1,"R");
							tempSig=gaus1->GetParameter(2);
						} else if (errorType==1) {
							tempMean=R4ConeLC_etaSlicesNoiseSecondary[y]->GetMean();
							tempSig=R4ConeLC_etaSlicesNoiseSecondary[y]->GetRMS();
							NoiseAsFcnEta4LCSecondary->SetBinContent(y+1,tempMean);
						}
						NoiseAsFcnEta4LCSecondary->SetBinError(y+1,tempSig);
					} else {
						if (NoiseAsFcnEta4LCSecondary->GetBinContent(y+1)==0) {
							NoiseAsFcnEta4LCSecondary->SetBinError(y+1,0);
						} else {
							NoiseAsFcnEta4LCSecondary->SetBinError(y+1,10);
						}
					}
				}
			}
		}
		cout<<"heyoH"<<endl;
		if (doNPVToys) {
			for (int y=minNPV;y<maxNPV;y++) {
				R4ConeEM_NoiseAsFcnOfNPV[y-minNPV]=(TH1D*)infile->Get(("R4ConeEM_NoiseAsFcnOfNPV"+std::to_string(y)).c_str())->Clone();
				if (R4ConeEM_NoiseAsFcnOfNPV[y-minNPV]->Integral(1,R4ConeEM_NoiseAsFcnOfNPV[y-minNPV]->GetNbinsX())>=10) {
					if (errorType==0) {
						R4ConeEM_NoiseAsFcnOfNPV[y-minNPV]->Fit(gaus1,"R");
						tempSig=gaus1->GetParameter(2);
					} else if (errorType==1) {
						tempMean=R4ConeEM_NoiseAsFcnOfNPV[y-minNPV]->GetMean();
						tempSig=R4ConeEM_NoiseAsFcnOfNPV[y-minNPV]->GetRMS();
						NoiseAsFcnNPV4EM->SetBinContent(y-minNPV+1,tempMean);
					}
					NoiseAsFcnNPV4EM->SetBinError(y-minNPV+1,tempSig);
				} else {
					if (NoiseAsFcnNPV4EM->GetBinContent(y-minNPV+1)==0) {
						NoiseAsFcnNPV4EM->SetBinError(y-minNPV+1,0);
					} else {
						NoiseAsFcnNPV4EM->SetBinError(y-minNPV+1,10);
					}
				}

				R4ConeLC_NoiseAsFcnOfNPV[y-minNPV]=(TH1D*)infile->Get(("R4ConeLC_NoiseAsFcnOfNPV"+std::to_string(y)).c_str())->Clone();
				if (R4ConeLC_NoiseAsFcnOfNPV[y-minNPV]->Integral(1,R4ConeEM_NoiseAsFcnOfNPV[y-minNPV]->GetNbinsX())>=10) {
					if (errorType==0) {
						R4ConeLC_NoiseAsFcnOfNPV[y-minNPV]->Fit(gaus1,"R");
						tempSig=gaus1->GetParameter(2);
					} else if (errorType==1) {
						tempMean=R4ConeLC_NoiseAsFcnOfNPV[y-minNPV]->GetMean();
						tempSig=R4ConeLC_NoiseAsFcnOfNPV[y-minNPV]->GetRMS();
						NoiseAsFcnNPV4LC->SetBinContent(y-minNPV+1,tempMean);
					}
					NoiseAsFcnNPV4LC->SetBinError(y-minNPV+1,tempSig);
				} else {
					if (NoiseAsFcnNPV4LC->GetBinContent(y-minNPV+1)==0) {
						NoiseAsFcnNPV4LC->SetBinError(y-minNPV+1,0);
					} else {
						NoiseAsFcnNPV4LC->SetBinError(y-minNPV+1,10);
					}
				}
			}
			if (doSecondary) {
				for (int y=minNPV;y<maxNPV;y++) {
					R4ConeEM_NoiseAsFcnOfNPVSecondary[y-minNPV]=(TH1D*)infile->Get(("R4ConeEM_NoiseAsFcnOfNPVSecondary"+std::to_string(y)).c_str())->Clone();
					if (R4ConeEM_NoiseAsFcnOfNPVSecondary[y-minNPV]->Integral(1,R4ConeEM_NoiseAsFcnOfNPVSecondary[y-minNPV]->GetNbinsX())>=10) {
						if (errorType==0) {
							R4ConeEM_NoiseAsFcnOfNPVSecondary[y-minNPV]->Fit(gaus1,"R");
							tempSig=gaus1->GetParameter(2);
						} else if (errorType==1) {
							tempMean=R4ConeEM_NoiseAsFcnOfNPVSecondary[y-minNPV]->GetMean();
							tempSig=R4ConeEM_NoiseAsFcnOfNPVSecondary[y-minNPV]->GetRMS();
							NoiseAsFcnNPV4EMSecondary->SetBinContent(y-minNPV+1,tempMean);
						}
						NoiseAsFcnNPV4EMSecondary->SetBinError(y-minNPV+1,tempSig);
					} else {
						if (NoiseAsFcnNPV4EMSecondary->GetBinContent(y-minNPV+1)==0) {
							NoiseAsFcnNPV4EMSecondary->SetBinError(y-minNPV+1,0);
						} else {
							NoiseAsFcnNPV4EMSecondary->SetBinError(y-minNPV+1,10);
						}
					}

					R4ConeLC_NoiseAsFcnOfNPVSecondary[y-minNPV]=(TH1D*)infile->Get(("R4ConeLC_NoiseAsFcnOfNPVSecondary"+std::to_string(y)).c_str())->Clone();
					if (R4ConeLC_NoiseAsFcnOfNPVSecondary[y-minNPV]->Integral(1,R4ConeEM_NoiseAsFcnOfNPVSecondary[y-minNPV]->GetNbinsX())>=10) {
						if (errorType==0) {
							R4ConeLC_NoiseAsFcnOfNPVSecondary[y-minNPV]->Fit(gaus1,"R");
							tempSig=gaus1->GetParameter(2);
						} else if (errorType==1) {
							tempMean=R4ConeLC_NoiseAsFcnOfNPVSecondary[y-minNPV]->GetMean();
							tempSig=R4ConeLC_NoiseAsFcnOfNPVSecondary[y-minNPV]->GetRMS();
							NoiseAsFcnNPV4LCSecondary->SetBinContent(y-minNPV+1,tempMean);
						}
						NoiseAsFcnNPV4LCSecondary->SetBinError(y-minNPV+1,tempSig);
					} else {
						if (NoiseAsFcnNPV4LCSecondary->GetBinContent(y-minNPV+1)==0) {
							NoiseAsFcnNPV4LCSecondary->SetBinError(y-minNPV+1,0);
						} else {
							NoiseAsFcnNPV4LCSecondary->SetBinError(y-minNPV+1,10);
						}
					}
				}
			}
		}
		cout<<"heyoI"<<endl;
		if (doMuNPVequalsAllToys) {
			for (int y=minMu;y<maxMu;y++) {
				R4ConeEM_NoiseAsFcnOfMuNPVAll[y-minMu]=(TH1D*)infile->Get(("R4ConeEM_NoiseAsFcnOfMuNPVAll"+std::to_string(y)).c_str())->Clone();
				if (R4ConeEM_NoiseAsFcnOfMuNPVAll[y-minMu]->Integral(1,R4ConeEM_NoiseAsFcnOfMuNPVAll[y-minMu]->GetNbinsX())>=10) {
					if (errorType==0) {
						R4ConeEM_NoiseAsFcnOfMuNPVAll[y-minMu]->Fit(gaus1,"R");
						tempSig=gaus1->GetParameter(2);
					} else if (errorType==1) {
						tempMean=R4ConeEM_NoiseAsFcnOfMuNPVAll[y-minMu]->GetMean();
						tempSig=R4ConeEM_NoiseAsFcnOfMuNPVAll[y-minMu]->GetRMS();
						NoiseAsFcnMu4EMAll->SetBinContent(y-minMu+1,tempMean);
					}
					NoiseAsFcnMu4EMAll->SetBinError(y-minMu+1,tempSig);
				} else {
					if (NoiseAsFcnMu4EMAll->GetBinContent(y-minMu+1)==0) {
						NoiseAsFcnMu4EMAll->SetBinError(y-minMu+1,0);
					} else {
						NoiseAsFcnMu4EMAll->SetBinError(y-minMu+1,10);
					}
				}

				R4ConeLC_NoiseAsFcnOfMuNPVAll[y-minMu]=(TH1D*)infile->Get(("R4ConeLC_NoiseAsFcnOfMuNPVAll"+std::to_string(y)).c_str())->Clone();
				if (R4ConeLC_NoiseAsFcnOfMuNPVAll[y-minMu]->Integral(1,R4ConeLC_NoiseAsFcnOfMuNPVAll[y-minMu]->GetNbinsX())>=10) {
					if (errorType==0) {
						R4ConeLC_NoiseAsFcnOfMuNPVAll[y-minMu]->Fit(gaus1,"R");
						tempSig=gaus1->GetParameter(2);
					} else if (errorType==1) {
						tempMean=R4ConeLC_NoiseAsFcnOfMuNPVAll[y-minMu]->GetMean();
						tempSig=R4ConeLC_NoiseAsFcnOfMuNPVAll[y-minMu]->GetRMS();
						NoiseAsFcnMu4LCAll->SetBinContent(y-minMu+1,tempMean);
					}
					NoiseAsFcnMu4LCAll->SetBinError(y-minMu+1,tempSig);
				} else {
					if (NoiseAsFcnMu4LCAll->GetBinContent(y-minMu+1)==0) {
						NoiseAsFcnMu4LCAll->SetBinError(y-minMu+1,0);
					} else {
						NoiseAsFcnMu4LCAll->SetBinError(y-minMu+1,10);
					}
				}
			}
			if (doSecondary) {
				for (int y=minMu;y<maxMu;y++) {
					R4ConeEM_NoiseAsFcnOfMuNPVAllSecondary[y-minMu]=(TH1D*)infile->Get(("R4ConeEM_NoiseAsFcnOfMuNPVAllSecondary"+std::to_string(y)).c_str())->Clone();
					if (R4ConeEM_NoiseAsFcnOfMuNPVAllSecondary[y-minMu]->Integral(1,R4ConeEM_NoiseAsFcnOfMuNPVAllSecondary[y-minMu]->GetNbinsX())>=10) {
						if (errorType==0) {
							R4ConeEM_NoiseAsFcnOfMuNPVAllSecondary[y-minMu]->Fit(gaus1,"R");
							tempSig=gaus1->GetParameter(2);
						} else if (errorType==1) {
							tempMean=R4ConeEM_NoiseAsFcnOfMuNPVAllSecondary[y-minMu]->GetMean();
							tempSig=R4ConeEM_NoiseAsFcnOfMuNPVAllSecondary[y-minMu]->GetRMS();
							NoiseAsFcnMu4EMAllSecondary->SetBinContent(y-minMu+1,tempMean);
						}
						NoiseAsFcnMu4EMAllSecondary->SetBinError(y-minMu+1,tempSig);
					} else {
						if (NoiseAsFcnMu4EMAllSecondary->GetBinContent(y-minMu+1)==0) {
							NoiseAsFcnMu4EMAllSecondary->SetBinError(y-minMu+1,0);
						} else {
							NoiseAsFcnMu4EMAllSecondary->SetBinError(y-minMu+1,10);
						}
					}

					R4ConeLC_NoiseAsFcnOfMuNPVAllSecondary[y-minMu]=(TH1D*)infile->Get(("R4ConeLC_NoiseAsFcnOfMuNPVAllSecondary"+std::to_string(y)).c_str())->Clone();
					if (R4ConeLC_NoiseAsFcnOfMuNPVAllSecondary[y-minMu]->Integral(1,R4ConeLC_NoiseAsFcnOfMuNPVAllSecondary[y-minMu]->GetNbinsX())>=10) {
						if (errorType==0) {
							R4ConeLC_NoiseAsFcnOfMuNPVAllSecondary[y-minMu]->Fit(gaus1,"R");
							tempSig=gaus1->GetParameter(2);
						} else if (errorType==1) {
							tempMean=R4ConeLC_NoiseAsFcnOfMuNPVAllSecondary[y-minMu]->GetMean();
							tempSig=R4ConeLC_NoiseAsFcnOfMuNPVAllSecondary[y-minMu]->GetRMS();
							NoiseAsFcnMu4LCAllSecondary->SetBinContent(y-minMu+1,tempMean);
						}
						NoiseAsFcnMu4LCAllSecondary->SetBinError(y-minMu+1,tempSig);
					} else {
						if (NoiseAsFcnMu4LCAllSecondary->GetBinContent(y-minMu+1)==0) {
							NoiseAsFcnMu4LCAllSecondary->SetBinError(y-minMu+1,0);
						} else {
							NoiseAsFcnMu4LCAllSecondary->SetBinError(y-minMu+1,10);
						}
					}
				}
			}
		}
		cout<<"heyoJ"<<endl;
		if (doMuNPVFixedToys) {
			for (int y=minNPV;y<maxNPV;y++) {
				for (int z=minMu;z<maxMu;z++) {
					R4ConeEM_NoiseAsFcnOfMuNPVFixed[y-minNPV][z-minMu]=(TH1D*)infile->Get(("R4ConeEM_NoiseAsFcnOfMuNPVFixed_NPV"+std::to_string(y)+"_mu"+std::to_string(z)).c_str())->Clone();
					if (R4ConeEM_NoiseAsFcnOfMuNPVFixed[y-minNPV][z-minMu]->Integral(1,R4ConeEM_NoiseAsFcnOfMuNPVFixed[y-minNPV][z-minMu]->GetNbinsX())>=10) {
						if (errorType==0) {
							R4ConeEM_NoiseAsFcnOfMuNPVFixed[y-minNPV][z-minMu]->Fit(gaus1,"R");
							tempSig=gaus1->GetParameter(2);
						} else if (errorType==1) {
							tempMean=R4ConeEM_NoiseAsFcnOfMuNPVFixed[y-minNPV][z-minMu]->GetMean();
							tempSig=R4ConeEM_NoiseAsFcnOfMuNPVFixed[y-minNPV][z-minMu]->GetRMS();
							NoiseAsFcnMu4EMNPV[y-minNPV]->SetBinContent(z-minMu+1,tempMean);
						}
						NoiseAsFcnMu4EMNPV[y-minNPV]->SetBinError(z-minMu+1,tempSig);
					} else {
						if (NoiseAsFcnMu4EMNPV[y-minNPV]->GetBinContent(z-minMu+1)==0) {
							NoiseAsFcnMu4EMNPV[y-minNPV]->SetBinError(z-minMu+1,0);
						} else {
							NoiseAsFcnMu4EMNPV[y-minNPV]->SetBinError(z-minMu+1,10);
						}
					}

					R4ConeLC_NoiseAsFcnOfMuNPVFixed[y-minNPV][z-minMu]=(TH1D*)infile->Get(("R4ConeLC_NoiseAsFcnOfMuNPVFixed_NPV"+std::to_string(y)+"_mu"+std::to_string(z)).c_str())->Clone();
					if (R4ConeLC_NoiseAsFcnOfMuNPVFixed[y-minNPV][z-minMu]->Integral(1,R4ConeLC_NoiseAsFcnOfMuNPVFixed[y-minNPV][z-minMu]->GetNbinsX())>=10) {
						if (errorType==0) {
							R4ConeLC_NoiseAsFcnOfMuNPVFixed[y-minNPV][z-minMu]->Fit(gaus1,"R");
							tempSig=gaus1->GetParameter(2);
						} else if (errorType==1) {
							tempMean=R4ConeLC_NoiseAsFcnOfMuNPVFixed[y-minNPV][z-minMu]->GetMean();
							tempSig=R4ConeLC_NoiseAsFcnOfMuNPVFixed[y-minNPV][z-minMu]->GetRMS();
							NoiseAsFcnMu4LCNPV[y-minNPV]->SetBinContent(z-minMu+1,tempMean);
						}
						NoiseAsFcnMu4LCNPV[y-minNPV]->SetBinError(z-minMu+1,tempSig);
					} else {
						if (NoiseAsFcnMu4LCNPV[y-minNPV]->GetBinContent(z-minMu+1)==0) {
							NoiseAsFcnMu4LCNPV[y-minNPV]->SetBinError(z-minMu+1,0);
						} else {
							NoiseAsFcnMu4LCNPV[y-minNPV]->SetBinError(z-minMu+1,10);
						}
					}
				}
			}
			if (doSecondary) {
				for (int y=minNPV;y<maxNPV;y++) {
					for (int z=minMu;z<maxMu;z++) {
						R4ConeEM_NoiseAsFcnOfMuNPVFixedSecondary[y-minNPV][z-minMu]=(TH1D*)infile->Get(("R4ConeEM_NoiseAsFcnOfMuNPVFixed_NPVSecondary"+std::to_string(y)+"_mu"+std::to_string(z)).c_str())->Clone();
						if (R4ConeEM_NoiseAsFcnOfMuNPVFixedSecondary[y-minNPV][z-minMu]->Integral(1,R4ConeEM_NoiseAsFcnOfMuNPVFixedSecondary[y-minNPV][z-minMu]->GetNbinsX())>=10) {
							if (errorType==0) {
								R4ConeEM_NoiseAsFcnOfMuNPVFixedSecondary[y-minNPV][z-minMu]->Fit(gaus1,"R");
								tempSig=gaus1->GetParameter(2);
							} else if (errorType==1) {
								tempMean=R4ConeEM_NoiseAsFcnOfMuNPVFixedSecondary[y-minNPV][z-minMu]->GetMean();
								tempSig=R4ConeEM_NoiseAsFcnOfMuNPVFixedSecondary[y-minNPV][z-minMu]->GetRMS();
								NoiseAsFcnMu4EMNPVSecondary[y-minNPV]->SetBinContent(z-minMu+1,tempMean);
							}
							NoiseAsFcnMu4EMNPVSecondary[y-minNPV]->SetBinError(z-minMu+1,tempSig);
						} else {
							if (NoiseAsFcnMu4EMNPVSecondary[y-minNPV]->GetBinContent(z-minMu+1)==0) {
								NoiseAsFcnMu4EMNPVSecondary[y-minNPV]->SetBinError(z-minMu+1,0);
							} else {
								NoiseAsFcnMu4EMNPVSecondary[y-minNPV]->SetBinError(z-minMu+1,10);
							}
						}

						R4ConeLC_NoiseAsFcnOfMuNPVFixedSecondary[y-minNPV][z-minMu]=(TH1D*)infile->Get(("R4ConeLC_NoiseAsFcnOfMuNPVFixed_NPVSecondary"+std::to_string(y)+"_mu"+std::to_string(z)).c_str())->Clone();
						if (R4ConeLC_NoiseAsFcnOfMuNPVFixedSecondary[y-minNPV][z-minMu]->Integral(1,R4ConeLC_NoiseAsFcnOfMuNPVFixedSecondary[y-minNPV][z-minMu]->GetNbinsX())>=10) {
							if (errorType==0) {
								R4ConeLC_NoiseAsFcnOfMuNPVFixedSecondary[y-minNPV][z-minMu]->Fit(gaus1,"R");
								tempSig=gaus1->GetParameter(2);
							} else if (errorType==1) {
								tempMean=R4ConeLC_NoiseAsFcnOfMuNPVFixedSecondary[y-minNPV][z-minMu]->GetMean();
								tempSig=R4ConeLC_NoiseAsFcnOfMuNPVFixedSecondary[y-minNPV][z-minMu]->GetRMS();
								NoiseAsFcnMu4LCNPVSecondary[y-minNPV]->SetBinContent(z-minMu+1,tempMean);
							}
							NoiseAsFcnMu4LCNPVSecondary[y-minNPV]->SetBinError(z-minMu+1,tempSig);
						} else {
							if (NoiseAsFcnMu4LCNPVSecondary[y-minNPV]->GetBinContent(z-minMu+1)==0) {
								NoiseAsFcnMu4LCNPVSecondary[y-minNPV]->SetBinError(z-minMu+1,0);
							} else {
								NoiseAsFcnMu4LCNPVSecondary[y-minNPV]->SetBinError(z-minMu+1,10);
							}
						}
					}
				}
			}
		}
	}
	if (doMC) {
		if (doEtaToys) {
			for (int y=0;y<nEtaSteps;y++) {
				R4ConeEM_etaSlicesNoiseMC[y]=(TH1D*)infile->Get(("R4ConeEM_etaSlicesNoiseMC"+std::to_string(y)+"_"+std::to_string(y+1)).c_str())->Clone();
				if (R4ConeEM_etaSlicesNoiseMC[y]->Integral(1,R4ConeEM_etaSlicesNoiseMC[y]->GetNbinsX())>=10) {
					if (errorType==0) {
						R4ConeEM_etaSlicesNoiseMC[y]->Fit(gaus1,"R");
						tempSig=gaus1->GetParameter(2);
					} else if (errorType==1) {
						tempMean=R4ConeEM_etaSlicesNoiseMC[y]->GetMean();
						tempSig=R4ConeEM_etaSlicesNoiseMC[y]->GetRMS();
						MCNoiseAsFcnEta4EM->SetBinContent(y+1,tempMean);
					}
					MCNoiseAsFcnEta4EM->SetBinError(y+1,tempSig);

				} else {
					if (MCNoiseAsFcnEta4EM->GetBinContent(y+1)==0) {
						MCNoiseAsFcnEta4EM->SetBinError(y+1,0);
					} else {
						MCNoiseAsFcnEta4EM->SetBinError(y+1,10);
					}
				}

				R4ConeLC_etaSlicesNoiseMC[y]=(TH1D*)infile->Get(("R4ConeLC_etaSlicesNoiseMC"+std::to_string(y)+"_"+std::to_string(y+1)).c_str())->Clone();
				if (R4ConeLC_etaSlicesNoiseMC[y]->Integral(1,R4ConeLC_etaSlicesNoiseMC[y]->GetNbinsX())>=10) {
					if (errorType==0) {
						R4ConeLC_etaSlicesNoiseMC[y]->Fit(gaus1,"R");
						tempSig=gaus1->GetParameter(2);
					} else if (errorType==1) {
						tempMean=R4ConeLC_etaSlicesNoiseMC[y]->GetMean();
						tempSig=R4ConeLC_etaSlicesNoiseMC[y]->GetRMS();
						MCNoiseAsFcnEta4LC->SetBinContent(y+1,tempMean);
					}
					MCNoiseAsFcnEta4LC->SetBinError(y+1,tempSig);
				} else {
					if (MCNoiseAsFcnEta4LC->GetBinContent(y+1)==0) {
						MCNoiseAsFcnEta4LC->SetBinError(y+1,0);
					} else {
						MCNoiseAsFcnEta4LC->SetBinError(y+1,10);
					}
				}
			}
			if (doSecondary) {
				for (int y=0;y<nEtaStepsSecondary;y++) {
					R4ConeEM_etaSlicesNoiseSecondaryMC[y]=(TH1D*)infile->Get(("R4ConeEM_etaSlicesNoiseSecondaryMC"+std::to_string(y)+"_"+std::to_string(y+1)).c_str())->Clone();
					if (R4ConeEM_etaSlicesNoiseSecondaryMC[y]->Integral(1,R4ConeEM_etaSlicesNoiseSecondaryMC[y]->GetNbinsX())>=10) {
						if (errorType==0) {
							R4ConeEM_etaSlicesNoiseSecondaryMC[y]->Fit(gaus1,"R");
							tempSig=gaus1->GetParameter(2);
						} else if (errorType==1) {
							tempMean=R4ConeEM_etaSlicesNoiseSecondaryMC[y]->GetMean();
							tempSig=R4ConeEM_etaSlicesNoiseSecondaryMC[y]->GetRMS();
							MCNoiseAsFcnEta4EMSecondary->SetBinContent(y+1,tempMean);
						}
						MCNoiseAsFcnEta4EMSecondary->SetBinError(y+1,tempSig);

					} else {
						if (MCNoiseAsFcnEta4EMSecondary->GetBinContent(y+1)==0) {
							MCNoiseAsFcnEta4EMSecondary->SetBinError(y+1,0);
						} else {
							MCNoiseAsFcnEta4EMSecondary->SetBinError(y+1,10);
						}
					}

					R4ConeLC_etaSlicesNoiseSecondaryMC[y]=(TH1D*)infile->Get(("R4ConeLC_etaSlicesNoiseSecondaryMC"+std::to_string(y)+"_"+std::to_string(y+1)).c_str())->Clone();
					if (R4ConeLC_etaSlicesNoiseSecondaryMC[y]->Integral(1,R4ConeLC_etaSlicesNoiseSecondaryMC[y]->GetNbinsX())>=10) {
						if (errorType==0) {
							R4ConeLC_etaSlicesNoiseSecondaryMC[y]->Fit(gaus1,"R");
							tempSig=gaus1->GetParameter(2);
						} else if (errorType==1) {
							tempMean=R4ConeLC_etaSlicesNoiseSecondaryMC[y]->GetMean();
							tempSig=R4ConeLC_etaSlicesNoiseSecondaryMC[y]->GetRMS();
							MCNoiseAsFcnEta4LCSecondary->SetBinContent(y+1,tempMean);
						}
						MCNoiseAsFcnEta4LCSecondary->SetBinError(y+1,tempSig);
					} else {
						if (MCNoiseAsFcnEta4LCSecondary->GetBinContent(y+1)==0) {
							MCNoiseAsFcnEta4LCSecondary->SetBinError(y+1,0);
						} else {
							MCNoiseAsFcnEta4LCSecondary->SetBinError(y+1,10);
						}
					}
				}
			}
		}
		cout<<"heyoH"<<endl;
		if (doNPVToys) {
			for (int y=minNPV;y<maxNPV;y++) {
				R4ConeEM_NoiseAsFcnOfNPVMC[y-minNPV]=(TH1D*)infile->Get(("R4ConeEM_NoiseAsFcnOfNPVMC"+std::to_string(y)).c_str())->Clone();
				if (R4ConeEM_NoiseAsFcnOfNPVMC[y-minNPV]->Integral(1,R4ConeEM_NoiseAsFcnOfNPVMC[y-minNPV]->GetNbinsX())>=10) {
					if (errorType==0) {
						R4ConeEM_NoiseAsFcnOfNPVMC[y-minNPV]->Fit(gaus1,"R");
						tempSig=gaus1->GetParameter(2);
					} else if (errorType==1) {
						tempMean=R4ConeEM_NoiseAsFcnOfNPVMC[y-minNPV]->GetMean();
						tempSig=R4ConeEM_NoiseAsFcnOfNPVMC[y-minNPV]->GetRMS();
						MCNoiseAsFcnNPV4EM->SetBinContent(y-minNPV+1,tempMean);
					}
					MCNoiseAsFcnNPV4EM->SetBinError(y-minNPV+1,tempSig);
				} else {
					if (MCNoiseAsFcnNPV4EM->GetBinContent(y-minNPV+1)==0) {
						MCNoiseAsFcnNPV4EM->SetBinError(y-minNPV+1,0);
					} else {
						MCNoiseAsFcnNPV4EM->SetBinError(y-minNPV+1,10);
					}
				}

				R4ConeLC_NoiseAsFcnOfNPVMC[y-minNPV]=(TH1D*)infile->Get(("R4ConeLC_NoiseAsFcnOfNPVMC"+std::to_string(y)).c_str())->Clone();
				if (R4ConeLC_NoiseAsFcnOfNPVMC[y-minNPV]->Integral(1,R4ConeEM_NoiseAsFcnOfNPVMC[y-minNPV]->GetNbinsX())>=10) {
					if (errorType==0) {
						R4ConeLC_NoiseAsFcnOfNPVMC[y-minNPV]->Fit(gaus1,"R");
						tempSig=gaus1->GetParameter(2);
					} else if (errorType==1) {
						tempMean=R4ConeLC_NoiseAsFcnOfNPVMC[y-minNPV]->GetMean();
						tempSig=R4ConeLC_NoiseAsFcnOfNPVMC[y-minNPV]->GetRMS();
						MCNoiseAsFcnNPV4LC->SetBinContent(y-minNPV+1,tempMean);
					}
					MCNoiseAsFcnNPV4LC->SetBinError(y-minNPV+1,tempSig);
				} else {
					if (MCNoiseAsFcnNPV4LC->GetBinContent(y-minNPV+1)==0) {
						MCNoiseAsFcnNPV4LC->SetBinError(y-minNPV+1,0);
					} else {
						MCNoiseAsFcnNPV4LC->SetBinError(y-minNPV+1,10);
					}
				}
			}
			if (doSecondary) {
				for (int y=minNPV;y<maxNPV;y++) {
					R4ConeEM_NoiseAsFcnOfNPVSecondaryMC[y-minNPV]=(TH1D*)infile->Get(("R4ConeEM_NoiseAsFcnOfNPVSecondaryMC"+std::to_string(y)).c_str())->Clone();
					if (R4ConeEM_NoiseAsFcnOfNPVSecondaryMC[y-minNPV]->Integral(1,R4ConeEM_NoiseAsFcnOfNPVSecondaryMC[y-minNPV]->GetNbinsX())>=10) {
						if (errorType==0) {
							R4ConeEM_NoiseAsFcnOfNPVSecondaryMC[y-minNPV]->Fit(gaus1,"R");
							tempSig=gaus1->GetParameter(2);
						} else if (errorType==1) {
							tempMean=R4ConeEM_NoiseAsFcnOfNPVSecondaryMC[y-minNPV]->GetMean();
							tempSig=R4ConeEM_NoiseAsFcnOfNPVSecondaryMC[y-minNPV]->GetRMS();
							MCNoiseAsFcnNPV4EMSecondary->SetBinContent(y-minNPV+1,tempMean);
						}
						MCNoiseAsFcnNPV4EMSecondary->SetBinError(y-minNPV+1,tempSig);
					} else {
						if (MCNoiseAsFcnNPV4EMSecondary->GetBinContent(y-minNPV+1)==0) {
							MCNoiseAsFcnNPV4EMSecondary->SetBinError(y-minNPV+1,0);
						} else {
							MCNoiseAsFcnNPV4EMSecondary->SetBinError(y-minNPV+1,10);
						}
					}

					R4ConeLC_NoiseAsFcnOfNPVSecondaryMC[y-minNPV]=(TH1D*)infile->Get(("R4ConeLC_NoiseAsFcnOfNPVSecondaryMC"+std::to_string(y)).c_str())->Clone();
					if (R4ConeLC_NoiseAsFcnOfNPVSecondaryMC[y-minNPV]->Integral(1,R4ConeEM_NoiseAsFcnOfNPVSecondaryMC[y-minNPV]->GetNbinsX())>=10) {
						if (errorType==0) {
							R4ConeLC_NoiseAsFcnOfNPVSecondaryMC[y-minNPV]->Fit(gaus1,"R");
							tempSig=gaus1->GetParameter(2);
						} else if (errorType==1) {
							tempMean=R4ConeLC_NoiseAsFcnOfNPVSecondaryMC[y-minNPV]->GetMean();
							tempSig=R4ConeLC_NoiseAsFcnOfNPVSecondaryMC[y-minNPV]->GetRMS();
							MCNoiseAsFcnNPV4LCSecondary->SetBinContent(y-minNPV+1,tempMean);
						}
						MCNoiseAsFcnNPV4LCSecondary->SetBinError(y-minNPV+1,tempSig);
					} else {
						if (MCNoiseAsFcnNPV4LCSecondary->GetBinContent(y-minNPV+1)==0) {
							MCNoiseAsFcnNPV4LCSecondary->SetBinError(y-minNPV+1,0);
						} else {
							MCNoiseAsFcnNPV4LCSecondary->SetBinError(y-minNPV+1,10);
						}
					}
				}
			}
		}
		cout<<"heyoI"<<endl;
		if (doMuNPVequalsAllToys) {
			for (int y=minMu;y<maxMu;y++) {
				R4ConeEM_NoiseAsFcnOfMuNPVAllMC[y-minMu]=(TH1D*)infile->Get(("R4ConeEM_NoiseAsFcnOfMuNPVAllMC"+std::to_string(y)).c_str())->Clone();
				if (R4ConeEM_NoiseAsFcnOfMuNPVAllMC[y-minMu]->Integral(1,R4ConeEM_NoiseAsFcnOfMuNPVAllMC[y-minMu]->GetNbinsX())>=10) {
					if (errorType==0) {
						R4ConeEM_NoiseAsFcnOfMuNPVAllMC[y-minMu]->Fit(gaus1,"R");
						tempSig=gaus1->GetParameter(2);
					} else if (errorType==1) {
						tempMean=R4ConeEM_NoiseAsFcnOfMuNPVAllMC[y-minMu]->GetMean();
						tempSig=R4ConeEM_NoiseAsFcnOfMuNPVAllMC[y-minMu]->GetRMS();
						MCNoiseAsFcnMu4EMAll->SetBinContent(y-minMu+1,tempMean);
					}
					MCNoiseAsFcnMu4EMAll->SetBinError(y-minMu+1,tempSig);
				} else {
					if (MCNoiseAsFcnMu4EMAll->GetBinContent(y-minMu+1)==0) {
						MCNoiseAsFcnMu4EMAll->SetBinError(y-minMu+1,0);
					} else {
						MCNoiseAsFcnMu4EMAll->SetBinError(y-minMu+1,10);
					}
				}

				R4ConeLC_NoiseAsFcnOfMuNPVAllMC[y-minMu]=(TH1D*)infile->Get(("R4ConeLC_NoiseAsFcnOfMuNPVAllMC"+std::to_string(y)).c_str())->Clone();
				if (R4ConeLC_NoiseAsFcnOfMuNPVAllMC[y-minMu]->Integral(1,R4ConeLC_NoiseAsFcnOfMuNPVAllMC[y-minMu]->GetNbinsX())>=10) {
					if (errorType==0) {
						R4ConeLC_NoiseAsFcnOfMuNPVAllMC[y-minMu]->Fit(gaus1,"R");
						tempSig=gaus1->GetParameter(2);
					} else if (errorType==1) {
						tempMean=R4ConeLC_NoiseAsFcnOfMuNPVAllMC[y-minMu]->GetMean();
						tempSig=R4ConeLC_NoiseAsFcnOfMuNPVAllMC[y-minMu]->GetRMS();
						MCNoiseAsFcnMu4LCAll->SetBinContent(y-minMu+1,tempMean);
					}
					MCNoiseAsFcnMu4LCAll->SetBinError(y-minMu+1,tempSig);
				} else {
					if (MCNoiseAsFcnMu4LCAll->GetBinContent(y-minMu+1)==0) {
						MCNoiseAsFcnMu4LCAll->SetBinError(y-minMu+1,0);
					} else {
						MCNoiseAsFcnMu4LCAll->SetBinError(y-minMu+1,10);
					}
				}
			}
			if (doSecondary) {
				for (int y=minMu;y<maxMu;y++) {
					R4ConeEM_NoiseAsFcnOfMuNPVAllSecondaryMC[y-minMu]=(TH1D*)infile->Get(("R4ConeEM_NoiseAsFcnOfMuNPVAllSecondaryMC"+std::to_string(y)).c_str())->Clone();
					if (R4ConeEM_NoiseAsFcnOfMuNPVAllSecondaryMC[y-minMu]->Integral(1,R4ConeEM_NoiseAsFcnOfMuNPVAllSecondaryMC[y-minMu]->GetNbinsX())>=10) {
						if (errorType==0) {
							R4ConeEM_NoiseAsFcnOfMuNPVAllSecondaryMC[y-minMu]->Fit(gaus1,"R");
							tempSig=gaus1->GetParameter(2);
						} else if (errorType==1) {
							tempMean=R4ConeEM_NoiseAsFcnOfMuNPVAllSecondaryMC[y-minMu]->GetMean();
							tempSig=R4ConeEM_NoiseAsFcnOfMuNPVAllSecondaryMC[y-minMu]->GetRMS();
							MCNoiseAsFcnMu4EMAllSecondary->SetBinContent(y-minMu+1,tempMean);
						}
						MCNoiseAsFcnMu4EMAllSecondary->SetBinError(y-minMu+1,tempSig);
					} else {
						if (MCNoiseAsFcnMu4EMAllSecondary->GetBinContent(y-minMu+1)==0) {
							MCNoiseAsFcnMu4EMAllSecondary->SetBinError(y-minMu+1,0);
						} else {
							MCNoiseAsFcnMu4EMAllSecondary->SetBinError(y-minMu+1,10);
						}
					}

					R4ConeLC_NoiseAsFcnOfMuNPVAllSecondaryMC[y-minMu]=(TH1D*)infile->Get(("R4ConeLC_NoiseAsFcnOfMuNPVAllSecondaryMC"+std::to_string(y)).c_str())->Clone();
					if (R4ConeLC_NoiseAsFcnOfMuNPVAllSecondaryMC[y-minMu]->Integral(1,R4ConeLC_NoiseAsFcnOfMuNPVAllSecondaryMC[y-minMu]->GetNbinsX())>=10) {
						if (errorType==0) {
							R4ConeLC_NoiseAsFcnOfMuNPVAllSecondaryMC[y-minMu]->Fit(gaus1,"R");
							tempSig=gaus1->GetParameter(2);
						} else if (errorType==1) {
							tempMean=R4ConeLC_NoiseAsFcnOfMuNPVAllSecondaryMC[y-minMu]->GetMean();
							tempSig=R4ConeLC_NoiseAsFcnOfMuNPVAllSecondaryMC[y-minMu]->GetRMS();
							MCNoiseAsFcnMu4LCAllSecondary->SetBinContent(y-minMu+1,tempMean);
						}
						MCNoiseAsFcnMu4LCAllSecondary->SetBinError(y-minMu+1,tempSig);
					} else {
						if (MCNoiseAsFcnMu4LCAllSecondary->GetBinContent(y-minMu+1)==0) {
							MCNoiseAsFcnMu4LCAllSecondary->SetBinError(y-minMu+1,0);
						} else {
							MCNoiseAsFcnMu4LCAllSecondary->SetBinError(y-minMu+1,10);
						}
					}
				}
			}
		}
		cout<<"heyoJ"<<endl;
		if (doMuNPVFixedToys) {
			for (int y=minNPV;y<maxNPV;y++) {
				for (int z=minMu;z<maxMu;z++) {
					R4ConeEM_NoiseAsFcnOfMuNPVFixedMC[y-minNPV][z-minMu]=(TH1D*)infile->Get(("R4ConeEM_NoiseAsFcnOfMuNPVFixed_NPVMC"+std::to_string(y)+"_mu"+std::to_string(z)).c_str())->Clone();
					if (R4ConeEM_NoiseAsFcnOfMuNPVFixedMC[y-minNPV][z-minMu]->Integral(1,R4ConeEM_NoiseAsFcnOfMuNPVFixedMC[y-minNPV][z-minMu]->GetNbinsX())>=10) {
						if (errorType==0) {
							R4ConeEM_NoiseAsFcnOfMuNPVFixedMC[y-minNPV][z-minMu]->Fit(gaus1,"R");
							tempSig=gaus1->GetParameter(2);
						} else if (errorType==1) {
							tempMean=R4ConeEM_NoiseAsFcnOfMuNPVFixedMC[y-minNPV][z-minMu]->GetMean();
							tempSig=R4ConeEM_NoiseAsFcnOfMuNPVFixedMC[y-minNPV][z-minMu]->GetRMS();
							MCNoiseAsFcnMu4EMNPV[y-minNPV]->SetBinContent(z-minMu+1,tempMean);
						}
						MCNoiseAsFcnMu4EMNPV[y-minNPV]->SetBinError(z-minMu+1,tempSig);
					} else {
						if (MCNoiseAsFcnMu4EMNPV[y-minNPV]->GetBinContent(z-minMu+1)==0) {
							MCNoiseAsFcnMu4EMNPV[y-minNPV]->SetBinError(z-minMu+1,0);
						} else {
							MCNoiseAsFcnMu4EMNPV[y-minNPV]->SetBinError(z-minMu+1,10);
						}
					}

					R4ConeLC_NoiseAsFcnOfMuNPVFixedMC[y-minNPV][z-minMu]=(TH1D*)infile->Get(("R4ConeLC_NoiseAsFcnOfMuNPVFixed_NPVMC"+std::to_string(y)+"_mu"+std::to_string(z)).c_str())->Clone();
					if (R4ConeLC_NoiseAsFcnOfMuNPVFixedMC[y-minNPV][z-minMu]->Integral(1,R4ConeLC_NoiseAsFcnOfMuNPVFixedMC[y-minNPV][z-minMu]->GetNbinsX())>=10) {
						if (errorType==0) {
							R4ConeLC_NoiseAsFcnOfMuNPVFixedMC[y-minNPV][z-minMu]->Fit(gaus1,"R");
							tempSig=gaus1->GetParameter(2);
						} else if (errorType==1) {
							tempMean=R4ConeLC_NoiseAsFcnOfMuNPVFixedMC[y-minNPV][z-minMu]->GetMean();
							tempSig=R4ConeLC_NoiseAsFcnOfMuNPVFixedMC[y-minNPV][z-minMu]->GetRMS();
							MCNoiseAsFcnMu4LCNPV[y-minNPV]->SetBinContent(z-minMu+1,tempMean);
						}
						MCNoiseAsFcnMu4LCNPV[y-minNPV]->SetBinError(z-minMu+1,tempSig);
					} else {
						if (MCNoiseAsFcnMu4LCNPV[y-minNPV]->GetBinContent(z-minMu+1)==0) {
							MCNoiseAsFcnMu4LCNPV[y-minNPV]->SetBinError(z-minMu+1,0);
						} else {
							MCNoiseAsFcnMu4LCNPV[y-minNPV]->SetBinError(z-minMu+1,10);
						}
					}
				}
			}
			if (doSecondary) {
				for (int y=minNPV;y<maxNPV;y++) {
					for (int z=minMu;z<maxMu;z++) {
						R4ConeEM_NoiseAsFcnOfMuNPVFixedSecondaryMC[y-minNPV][z-minMu]=(TH1D*)infile->Get(("R4ConeEM_NoiseAsFcnOfMuNPVFixed_NPVSecondaryMC"+std::to_string(y)+"_mu"+std::to_string(z)).c_str())->Clone();
						if (R4ConeEM_NoiseAsFcnOfMuNPVFixedSecondaryMC[y-minNPV][z-minMu]->Integral(1,R4ConeEM_NoiseAsFcnOfMuNPVFixedSecondaryMC[y-minNPV][z-minMu]->GetNbinsX())>=10) {
							if (errorType==0) {
								R4ConeEM_NoiseAsFcnOfMuNPVFixedSecondaryMC[y-minNPV][z-minMu]->Fit(gaus1,"R");
								tempSig=gaus1->GetParameter(2);
							} else if (errorType==1) {
								tempMean=R4ConeEM_NoiseAsFcnOfMuNPVFixedSecondaryMC[y-minNPV][z-minMu]->GetMean();
								tempSig=R4ConeEM_NoiseAsFcnOfMuNPVFixedSecondaryMC[y-minNPV][z-minMu]->GetRMS();
								MCNoiseAsFcnMu4EMNPVSecondary[y-minNPV]->SetBinContent(z-minMu+1,tempMean);
							}
							MCNoiseAsFcnMu4EMNPVSecondary[y-minNPV]->SetBinError(z-minMu+1,tempSig);
						} else {
							if (MCNoiseAsFcnMu4EMNPVSecondary[y-minNPV]->GetBinContent(z-minMu+1)==0) {
								MCNoiseAsFcnMu4EMNPVSecondary[y-minNPV]->SetBinError(z-minMu+1,0);
							} else {
								MCNoiseAsFcnMu4EMNPVSecondary[y-minNPV]->SetBinError(z-minMu+1,10);
							}
						}

						R4ConeLC_NoiseAsFcnOfMuNPVFixedSecondaryMC[y-minNPV][z-minMu]=(TH1D*)infile->Get(("R4ConeLC_NoiseAsFcnOfMuNPVFixed_NPVSecondaryMC"+std::to_string(y)+"_mu"+std::to_string(z)).c_str())->Clone();
						if (R4ConeLC_NoiseAsFcnOfMuNPVFixedSecondaryMC[y-minNPV][z-minMu]->Integral(1,R4ConeLC_NoiseAsFcnOfMuNPVFixedSecondaryMC[y-minNPV][z-minMu]->GetNbinsX())>=10) {
							if (errorType==0) {
								R4ConeLC_NoiseAsFcnOfMuNPVFixedSecondaryMC[y-minNPV][z-minMu]->Fit(gaus1,"R");
								tempSig=gaus1->GetParameter(2);
							} else if (errorType==1) {
								tempMean=R4ConeLC_NoiseAsFcnOfMuNPVFixedSecondaryMC[y-minNPV][z-minMu]->GetMean();
								tempSig=R4ConeLC_NoiseAsFcnOfMuNPVFixedSecondaryMC[y-minNPV][z-minMu]->GetRMS();
								MCNoiseAsFcnMu4LCNPVSecondary[y-minNPV]->SetBinContent(z-minMu+1,tempMean);
							}
							MCNoiseAsFcnMu4LCNPVSecondary[y-minNPV]->SetBinError(z-minMu+1,tempSig);
						} else {
							if (MCNoiseAsFcnMu4LCNPVSecondary[y-minNPV]->GetBinContent(z-minMu+1)==0) {
								MCNoiseAsFcnMu4LCNPVSecondary[y-minNPV]->SetBinError(z-minMu+1,0);
							} else {
								MCNoiseAsFcnMu4LCNPVSecondary[y-minNPV]->SetBinError(z-minMu+1,10);
							}
						}
					}
				}
			}
		}
	}
	infile->Close();
	cout<<"heyoK"<<endl;
	TLegend *legend2;
	TLegend *legend4;
	legend2 = new TLegend(.60,.825,1.,.92);
	legend2->SetFillColor(0);
	legend2->SetFillStyle(0);//make it transparent
	legend2->SetBorderSize(0);
	legend2->SetTextSize(0.04);
	legend2->SetTextFont(42);
	legend2->SetMargin(.15);

	legend4 = new TLegend(.60,.73,1.,.92);
	legend4->SetFillColor(0);
	legend4->SetFillStyle(0);//make it transparent
	legend4->SetBorderSize(0);
	legend4->SetTextSize(0.04);
	legend4->SetTextFont(42);
	legend4->SetMargin(.15);

	TFile *outputFile2=new TFile((OutputFile2).c_str(),"RECREATE");
	vector<std::string> stuffToWrite4;
	vector<std::string> stuffToWrite4b;
	cout<<"heyoL"<<endl;
	if (doEtaToys) {
		if (doData) {
			NoiseAsFcnEta4EM->SetMarkerStyle(20);
			NoiseAsFcnEta4LC->SetMarkerStyle(21);
			NoiseAsFcnEta4LC->SetMarkerColor(4);
		}
		if (doMC) {
			MCNoiseAsFcnEta4EM->SetMarkerStyle(24);
			MCNoiseAsFcnEta4LC->SetMarkerStyle(25);
			MCNoiseAsFcnEta4LC->SetMarkerColor(4);
		}
		//vector<std::string> stuffToWrite4;
		stuffToWrite4.push_back("Random Cone");
		stuffToWrite4.push_back("Noise Term");
		cout<<"heyoL1d"<<endl;
		//vector<std::string> stuffToWrite4b;
		stuffToWrite4b.push_back("Random Cone");
		stuffToWrite4b.push_back("Noise Term");
		if (doData&&!doMC) {
			legend2->AddEntry(NoiseAsFcnEta4EM,"R=0.4,EM Scale Data","P");
			legend2->AddEntry(NoiseAsFcnEta4LC,"R=0.4,LC Scale Data","P");
			saveTwoHistoMarker(NoiseAsFcnEta4EM,NoiseAsFcnEta4LC,legend2,"|#eta|","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0);
			NoiseAsFcnEta4EM->Write();
			NoiseAsFcnEta4LC->Write();
			legend2->Clear();
		} else if (!doData&&doMC) {
			legend2->AddEntry(MCNoiseAsFcnEta4EM,"R=0.4,EM Scale MC","P");
			legend2->AddEntry(MCNoiseAsFcnEta4LC,"R=0.4,LC Scale MC","P");
			saveTwoHistoMarker(MCNoiseAsFcnEta4EM,MCNoiseAsFcnEta4LC,legend2,"|#eta|","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0);
			MCNoiseAsFcnEta4EM->Write();
			MCNoiseAsFcnEta4LC->Write();
			legend2->Clear();
		} else if (doData&&doMC) {
			legend4->AddEntry(NoiseAsFcnEta4EM,"R=0.4,EM Scale Data","P");
			legend4->AddEntry(NoiseAsFcnEta4LC,"R=0.4,LC Scale Data","P");
			legend4->AddEntry(MCNoiseAsFcnEta4EM,"R=0.4,EM Scale MC","P");
			legend4->AddEntry(MCNoiseAsFcnEta4LC,"R=0.4,LC Scale MC","P");
			saveFourHistoMarker(NoiseAsFcnEta4EM,NoiseAsFcnEta4LC,MCNoiseAsFcnEta4EM,MCNoiseAsFcnEta4LC,legend4,"|#eta|","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0);
			NoiseAsFcnEta4EM->Write();
			NoiseAsFcnEta4LC->Write();
			MCNoiseAsFcnEta4EM->Write();
			MCNoiseAsFcnEta4LC->Write();
			legend4->Clear();
		}

		if (doSecondary) {
			if (doData) {
				NoiseAsFcnEta4EMSecondary->SetMarkerStyle(20);
				NoiseAsFcnEta4LCSecondary->SetMarkerStyle(21);
				NoiseAsFcnEta4LCSecondary->SetMarkerColor(4);
			}
			if (doMC) {
				MCNoiseAsFcnEta4EMSecondary->SetMarkerStyle(24);
				MCNoiseAsFcnEta4LCSecondary->SetMarkerStyle(25);
				MCNoiseAsFcnEta4LCSecondary->SetMarkerColor(4);
			}
			if (doData&&!doMC) {
				legend2->AddEntry(NoiseAsFcnEta4EMSecondary,"R=0.4,EM Scale Data","P");
				legend2->AddEntry(NoiseAsFcnEta4LCSecondary,"R=0.4,LC Scale Data","P");
				saveTwoHistoMarker(NoiseAsFcnEta4EMSecondary,NoiseAsFcnEta4LCSecondary,legend2,"|#eta|","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0);
				NoiseAsFcnEta4EMSecondary->Write();
				NoiseAsFcnEta4LCSecondary->Write();
				legend2->Clear();
			} else if (!doData&&doMC) {
				legend2->AddEntry(MCNoiseAsFcnEta4EMSecondary,"R=0.4,EM Scale MC","P");
				legend2->AddEntry(MCNoiseAsFcnEta4LCSecondary,"R=0.4,LC Scale MC","P");
				saveTwoHistoMarker(MCNoiseAsFcnEta4EMSecondary,MCNoiseAsFcnEta4LCSecondary,legend2,"|#eta|","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0);
				MCNoiseAsFcnEta4EMSecondary->Write();
				MCNoiseAsFcnEta4LCSecondary->Write();
				legend2->Clear();
			} else if (doData&&doMC) {
				legend4->AddEntry(NoiseAsFcnEta4EMSecondary,"R=0.4,EM Scale Data","P");
				legend4->AddEntry(NoiseAsFcnEta4LCSecondary,"R=0.4,LC Scale Data","P");
				legend4->AddEntry(MCNoiseAsFcnEta4EMSecondary,"R=0.4,EM Scale MC","P");
				legend4->AddEntry(MCNoiseAsFcnEta4LCSecondary,"R=0.4,LC Scale MC","P");
				saveFourHistoMarker(NoiseAsFcnEta4EMSecondary,NoiseAsFcnEta4LCSecondary,MCNoiseAsFcnEta4EMSecondary,MCNoiseAsFcnEta4LCSecondary,legend4,"|#eta|","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0);
				NoiseAsFcnEta4EMSecondary->Write();
				NoiseAsFcnEta4LCSecondary->Write();
				MCNoiseAsFcnEta4EMSecondary->Write();
				MCNoiseAsFcnEta4LCSecondary->Write();
				legend4->Clear();
			}
		}
	}
	cout<<"heyoM"<<endl;
	char buff1[15];
	char buff2[15];
	char sbuff1[15];
	char sbuff2[15];
	sprintf(buff1,"%.1f",etaBlocks[startEtaStep]);
	sprintf(buff2,"%.1f",etaBlocks[startEtaStep+nEtaStepsToTake]);
	sprintf(sbuff1,"%.1f",etaBlocksSecondary[startEtaStep]);
	sprintf(sbuff2,"%.1f",etaBlocksSecondary[startEtaStep+nEtaStepsToTakeSecondary]);
	stuffToWrite4.push_back(string(buff1)+"<|#eta|<"+string(buff2)+" NPV=ALL");
	stuffToWrite4b.push_back(string(sbuff1)+"<|#eta|<"+string(sbuff2)+" NPV=ALL");
	if (doNPVToys) {
		if (doData) {
			NoiseAsFcnNPV4EM->SetMarkerStyle(20);
			NoiseAsFcnNPV4LC->SetMarkerStyle(21);
			NoiseAsFcnNPV4LC->SetMarkerColor(4);
		}
		if (doMC) {
			MCNoiseAsFcnNPV4EM->SetMarkerStyle(24);
			MCNoiseAsFcnNPV4LC->SetMarkerStyle(25);
			MCNoiseAsFcnNPV4LC->SetMarkerColor(4);
		}

		if (doData&&!doMC) {
			legend2->AddEntry(NoiseAsFcnNPV4EM,"R=0.4,EM Scale Data","P");
			legend2->AddEntry(NoiseAsFcnNPV4LC,"R=0.4,LC Scale Data","P");
			saveTwoHistoMarker(NoiseAsFcnNPV4EM,NoiseAsFcnNPV4LC,legend2,"NPV","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0);
			NoiseAsFcnNPV4EM->Write();
			NoiseAsFcnNPV4LC->Write();
			legend2->Clear();
		} else if (!doData&&doMC) {
			legend2->AddEntry(MCNoiseAsFcnNPV4EM,"R=0.4,EM Scale MC","P");
			legend2->AddEntry(MCNoiseAsFcnNPV4LC,"R=0.4,LC Scale MC","P");
			saveTwoHistoMarker(MCNoiseAsFcnNPV4EM,MCNoiseAsFcnNPV4LC,legend2,"NPV","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0);
			MCNoiseAsFcnNPV4EM->Write();
			MCNoiseAsFcnNPV4LC->Write();
			legend2->Clear();
		} else if (doData&&doMC) {
			legend4->AddEntry(NoiseAsFcnNPV4EM,"R=0.4,EM Scale Data","P");
			legend4->AddEntry(NoiseAsFcnNPV4LC,"R=0.4,LC Scale Data","P");
			legend4->AddEntry(MCNoiseAsFcnNPV4EM,"R=0.4,EM Scale MC","P");
			legend4->AddEntry(MCNoiseAsFcnNPV4LC,"R=0.4,LC Scale MC","P");
			saveFourHistoMarker(NoiseAsFcnNPV4EM,NoiseAsFcnNPV4LC,MCNoiseAsFcnNPV4EM,MCNoiseAsFcnNPV4LC,legend4,"NPV","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0);
			NoiseAsFcnNPV4EM->Write();
			NoiseAsFcnNPV4LC->Write();
			MCNoiseAsFcnNPV4EM->Write();
			MCNoiseAsFcnNPV4LC->Write();
			legend4->Clear();
		}
		if (doSecondary) {
			if (doData) {
				NoiseAsFcnNPV4EMSecondary->SetMarkerStyle(20);
				NoiseAsFcnNPV4LCSecondary->SetMarkerStyle(21);
				NoiseAsFcnNPV4LCSecondary->SetMarkerColor(4);
			}
			if (doMC) {
				MCNoiseAsFcnNPV4EMSecondary->SetMarkerStyle(24);
				MCNoiseAsFcnNPV4LCSecondary->SetMarkerStyle(25);
				MCNoiseAsFcnNPV4LCSecondary->SetMarkerColor(4);
			}
			if (doData&&!doMC) {
				legend2->AddEntry(NoiseAsFcnNPV4EMSecondary,"R=0.4,EM Scale Data","P");
				legend2->AddEntry(NoiseAsFcnNPV4LCSecondary,"R=0.4,LC Scale Data","P");
				saveTwoHistoMarker(NoiseAsFcnNPV4EMSecondary,NoiseAsFcnNPV4LCSecondary,legend2,"NPV","Pile-up Noise[GeV]",stuffToWrite4b,outPutDir,false,0.20,0.88,0,0);
				NoiseAsFcnNPV4EMSecondary->Write();
				NoiseAsFcnNPV4LCSecondary->Write();
				legend2->Clear();
			} else if (!doData&&doMC) {
				legend2->AddEntry(MCNoiseAsFcnNPV4EMSecondary,"R=0.4,EM Scale MC","P");
				legend2->AddEntry(MCNoiseAsFcnNPV4LCSecondary,"R=0.4,LC Scale MC","P");
				saveTwoHistoMarker(MCNoiseAsFcnNPV4EMSecondary,MCNoiseAsFcnNPV4LCSecondary,legend2,"NPV","Pile-up Noise[GeV]",stuffToWrite4b,outPutDir,false,0.20,0.88,0,0);
				MCNoiseAsFcnNPV4EMSecondary->Write();
				MCNoiseAsFcnNPV4LCSecondary->Write();
				legend2->Clear();
			} else if (doData&&doMC) {
				legend4->AddEntry(NoiseAsFcnNPV4EMSecondary,"R=0.4,EM Scale Data","P");
				legend4->AddEntry(NoiseAsFcnNPV4LCSecondary,"R=0.4,LC Scale Data","P");
				legend4->AddEntry(MCNoiseAsFcnNPV4EMSecondary,"R=0.4,EM Scale MC","P");
				legend4->AddEntry(MCNoiseAsFcnNPV4LCSecondary,"R=0.4,LC Scale MC","P");
				saveFourHistoMarker(NoiseAsFcnNPV4EMSecondary,NoiseAsFcnNPV4LCSecondary,MCNoiseAsFcnNPV4EMSecondary,MCNoiseAsFcnNPV4LCSecondary,legend4,"NPV","Pile-up Noise[GeV]",stuffToWrite4b,outPutDir,false,0.20,0.88,0,0);
				NoiseAsFcnNPV4EMSecondary->Write();
				NoiseAsFcnNPV4LCSecondary->Write();
				MCNoiseAsFcnNPV4EMSecondary->Write();
				MCNoiseAsFcnNPV4LCSecondary->Write();
				legend4->Clear();
			}
		}
	}
	cout<<"heyoN"<<endl;
	if (doMuNPVequalsAllToys) {
		if (doData) {
			NoiseAsFcnMu4EMAll->SetMarkerStyle(20);
			NoiseAsFcnMu4LCAll->SetMarkerStyle(21);
			NoiseAsFcnMu4LCAll->SetMarkerColor(4);
		}
		if (doMC) {
			MCNoiseAsFcnMu4EMAll->SetMarkerStyle(24);
			MCNoiseAsFcnMu4LCAll->SetMarkerStyle(25);
			MCNoiseAsFcnMu4LCAll->SetMarkerColor(4);
		}
		if (doData&&!doMC) {
			legend2->AddEntry(NoiseAsFcnMu4EMAll,"R=0.4,EM Scale Data","P");
			legend2->AddEntry(NoiseAsFcnMu4LCAll,"R=0.4,LC Scale Data","P");
			saveTwoHistoMarker(NoiseAsFcnMu4EMAll,NoiseAsFcnMu4LCAll,legend2,"<#mu>","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0);
			NoiseAsFcnMu4EMAll->Write();
			NoiseAsFcnMu4LCAll->Write();
			legend2->Clear();
		} else if (!doData&&doMC) {
			legend2->AddEntry(MCNoiseAsFcnMu4EMAll,"R=0.4,EM Scale MC","P");
			legend2->AddEntry(MCNoiseAsFcnMu4LCAll,"R=0.4,LC Scale MC","P");
			saveTwoHistoMarker(MCNoiseAsFcnMu4EMAll,MCNoiseAsFcnMu4LCAll,legend2,"<#mu>","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0);
			MCNoiseAsFcnMu4EMAll->Write();
			MCNoiseAsFcnMu4LCAll->Write();
			legend2->Clear();
		} else if (doData&&doMC) {
			legend4->AddEntry(NoiseAsFcnMu4EMAll,"R=0.4,EM Scale Data","P");
			legend4->AddEntry(NoiseAsFcnMu4LCAll,"R=0.4,LC Scale Data","P");
			legend4->AddEntry(MCNoiseAsFcnMu4EMAll,"R=0.4,EM Scale MC","P");
			legend4->AddEntry(MCNoiseAsFcnMu4LCAll,"R=0.4,LC Scale MC","P");
			saveFourHistoMarker(NoiseAsFcnMu4EMAll,NoiseAsFcnMu4LCAll,MCNoiseAsFcnMu4EMAll,MCNoiseAsFcnMu4LCAll,legend4,"<#mu>","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0);
			NoiseAsFcnMu4EMAll->Write();
			NoiseAsFcnMu4LCAll->Write();
			MCNoiseAsFcnMu4EMAll->Write();
			MCNoiseAsFcnMu4LCAll->Write();
			legend4->Clear();
		}
		if (doSecondary) {
			if (doData) {
				NoiseAsFcnMu4EMAllSecondary->SetMarkerStyle(20);
				NoiseAsFcnMu4LCAllSecondary->SetMarkerStyle(21);
				NoiseAsFcnMu4LCAllSecondary->SetMarkerColor(4);
			}
			if (doMC) {
				MCNoiseAsFcnMu4EMAllSecondary->SetMarkerStyle(24);
				MCNoiseAsFcnMu4LCAllSecondary->SetMarkerStyle(25);
				MCNoiseAsFcnMu4LCAllSecondary->SetMarkerColor(4);
			}
			if (doData&&!doMC) {
				legend2->AddEntry(NoiseAsFcnMu4EMAllSecondary,"R=0.4,EM Scale Data","P");
				legend2->AddEntry(NoiseAsFcnMu4LCAllSecondary,"R=0.4,LC Scale Data","P");
				saveTwoHistoMarker(NoiseAsFcnMu4EMAllSecondary,NoiseAsFcnMu4LCAllSecondary,legend2,"<#mu>","Pile-up Noise[GeV]",stuffToWrite4b,outPutDir,false,0.20,0.88,0,0);
				NoiseAsFcnMu4EMAllSecondary->Write();
				NoiseAsFcnMu4LCAllSecondary->Write();
				legend2->Clear();
			} else if (!doData&&doMC) {
				legend2->AddEntry(MCNoiseAsFcnMu4EMAllSecondary,"R=0.4,EM Scale MC","P");
				legend2->AddEntry(MCNoiseAsFcnMu4LCAllSecondary,"R=0.4,LC Scale MC","P");
				saveTwoHistoMarker(MCNoiseAsFcnMu4EMAllSecondary,MCNoiseAsFcnMu4LCAllSecondary,legend2,"<#mu>","Pile-up Noise[GeV]",stuffToWrite4b,outPutDir,false,0.20,0.88,0,0);
				MCNoiseAsFcnMu4EMAllSecondary->Write();
				MCNoiseAsFcnMu4LCAllSecondary->Write();
				legend2->Clear();
			} else if (doData&&doMC) {
				legend4->AddEntry(NoiseAsFcnMu4EMAllSecondary,"R=0.4,EM Scale Data","P");
				legend4->AddEntry(NoiseAsFcnMu4LCAllSecondary,"R=0.4,LC Scale Data","P");
				legend4->AddEntry(MCNoiseAsFcnMu4EMAllSecondary,"R=0.4,EM Scale MC","P");
				legend4->AddEntry(MCNoiseAsFcnMu4LCAllSecondary,"R=0.4,LC Scale MC","P");
				saveFourHistoMarker(NoiseAsFcnMu4EMAllSecondary,NoiseAsFcnMu4LCAllSecondary,MCNoiseAsFcnMu4EMAllSecondary,MCNoiseAsFcnMu4LCAllSecondary,legend4,"<#mu>","Pile-up Noise[GeV]",stuffToWrite4b,outPutDir,false,0.20,0.88,0,0);
				NoiseAsFcnMu4EMAllSecondary->Write();
				NoiseAsFcnMu4LCAllSecondary->Write();
				MCNoiseAsFcnMu4EMAllSecondary->Write();
				MCNoiseAsFcnMu4LCAllSecondary->Write();
				legend4->Clear();
			}
		}
	}
	cout<<"heyoO"<<endl;
	if (doMuNPVFixedToys) {
		vector<std::string> stuffToWrite6;
		for (int y=minNPV;y<maxNPV;y++) {
			legend2->Clear();
			stuffToWrite6.push_back("Random Cone");
			stuffToWrite6.push_back("Noise Term");
			stuffToWrite6.push_back(string(buff1)+"<|#eta|<"+string(buff2)+" NPV= "+std::to_string(y));
			if (doData) {
				NoiseAsFcnMu4EMNPV[y-minNPV]->SetMarkerStyle(20);
				NoiseAsFcnMu4LCNPV[y-minNPV]->SetMarkerStyle(21);
				NoiseAsFcnMu4LCNPV[y-minNPV]->SetMarkerColor(4);
			}
			if (doMC) {
				MCNoiseAsFcnMu4EMNPV[y-minNPV]->SetMarkerStyle(24);
				MCNoiseAsFcnMu4LCNPV[y-minNPV]->SetMarkerStyle(25);
				MCNoiseAsFcnMu4LCNPV[y-minNPV]->SetMarkerColor(4);
			}
			if (doData&&!doMC) {
				legend2->AddEntry(NoiseAsFcnMu4EMNPV[y-minNPV],"R=0.4,EM Scale Data","P");
				legend2->AddEntry(NoiseAsFcnMu4LCNPV[y-minNPV],"R=0.4,LC Scale Data","P");
				saveTwoHistoMarker(NoiseAsFcnMu4EMNPV[y-minNPV],NoiseAsFcnMu4LCNPV[y-minNPV],legend2,"<#mu>","Pile-up Noise[GeV]",stuffToWrite6,outPutDir,false,0.20,0.88,0,0);
				NoiseAsFcnMu4EMNPV[y-minNPV]->Write();
				NoiseAsFcnMu4LCNPV[y-minNPV]->Write();
				legend2->Clear();
			} else if (!doData&&doMC) {
				legend2->AddEntry(MCNoiseAsFcnMu4EMNPV[y-minNPV],"R=0.4,EM Scale MC","P");
				legend2->AddEntry(MCNoiseAsFcnMu4LCNPV[y-minNPV],"R=0.4,LC Scale MC","P");
				saveTwoHistoMarker(MCNoiseAsFcnMu4EMNPV[y-minNPV],MCNoiseAsFcnMu4LCNPV[y-minNPV],legend2,"<#mu>","Pile-up Noise[GeV]",stuffToWrite6,outPutDir,false,0.20,0.88,0,0);
				MCNoiseAsFcnMu4EMNPV[y-minNPV]->Write();
				MCNoiseAsFcnMu4LCNPV[y-minNPV]->Write();
				legend2->Clear();
			} else if (doData&&doMC) {
				legend4->AddEntry(NoiseAsFcnMu4EMNPV[y-minNPV],"R=0.4,EM Scale Data","P");
				legend4->AddEntry(NoiseAsFcnMu4LCNPV[y-minNPV],"R=0.4,LC Scale Data","P");
				legend4->AddEntry(MCNoiseAsFcnMu4EMNPV[y-minNPV],"R=0.4,EM Scale MC","P");
				legend4->AddEntry(MCNoiseAsFcnMu4LCNPV[y-minNPV],"R=0.4,LC Scale MC","P");
				saveFourHistoMarker(NoiseAsFcnMu4EMNPV[y-minNPV],NoiseAsFcnMu4LCNPV[y-minNPV],MCNoiseAsFcnMu4EMNPV[y-minNPV],MCNoiseAsFcnMu4LCNPV[y-minNPV],legend4,"<#mu>","Pile-up Noise[GeV]",stuffToWrite6,outPutDir,false,0.20,0.88,0,0);
				NoiseAsFcnMu4EMNPV[y-minNPV]->Write();
				NoiseAsFcnMu4LCNPV[y-minNPV]->Write();
				MCNoiseAsFcnMu4EMNPV[y-minNPV]->Write();
				MCNoiseAsFcnMu4LCNPV[y-minNPV]->Write();
				legend4->Clear();
			}
			stuffToWrite6.clear();
			if (doSecondary) {
				legend2->Clear();
				stuffToWrite6.push_back("Random Cone");
				stuffToWrite6.push_back("Noise Term");
				stuffToWrite6.push_back(string(sbuff1)+"<|#eta|<"+string(sbuff2)+" NPV= "+std::to_string(y));
				if (doData) {
					NoiseAsFcnMu4EMNPVSecondary[y-minNPV]->SetMarkerStyle(20);
					NoiseAsFcnMu4LCNPVSecondary[y-minNPV]->SetMarkerStyle(21);
					NoiseAsFcnMu4LCNPVSecondary[y-minNPV]->SetMarkerColor(4);
				}
				if (doMC) {
					MCNoiseAsFcnMu4EMNPVSecondary[y-minNPV]->SetMarkerStyle(24);
					MCNoiseAsFcnMu4LCNPVSecondary[y-minNPV]->SetMarkerStyle(25);
					MCNoiseAsFcnMu4LCNPVSecondary[y-minNPV]->SetMarkerColor(4);
				}
				if (doData&&!doMC) {
					legend2->AddEntry(NoiseAsFcnMu4EMNPVSecondary[y-minNPV],"R=0.4,EM Scale Data","P");
					legend2->AddEntry(NoiseAsFcnMu4LCNPVSecondary[y-minNPV],"R=0.4,LC Scale Data","P");
					saveTwoHistoMarker(NoiseAsFcnMu4EMNPVSecondary[y-minNPV],NoiseAsFcnMu4LCNPVSecondary[y-minNPV],legend2,"<#mu>","Pile-up Noise[GeV]",stuffToWrite6,outPutDir,false,0.20,0.88,0,0);
					NoiseAsFcnMu4EMNPVSecondary[y-minNPV]->Write();
					NoiseAsFcnMu4LCNPVSecondary[y-minNPV]->Write();
					legend2->Clear();
				} else if (!doData&&doMC) {
					legend2->AddEntry(MCNoiseAsFcnMu4EMNPVSecondary[y-minNPV],"R=0.4,EM Scale MC","P");
					legend2->AddEntry(MCNoiseAsFcnMu4LCNPVSecondary[y-minNPV],"R=0.4,LC Scale MC","P");
					saveTwoHistoMarker(MCNoiseAsFcnMu4EMNPVSecondary[y-minNPV],MCNoiseAsFcnMu4LCNPVSecondary[y-minNPV],legend2,"<#mu>","Pile-up Noise[GeV]",stuffToWrite6,outPutDir,false,0.20,0.88,0,0);
					MCNoiseAsFcnMu4EMNPVSecondary[y-minNPV]->Write();
					MCNoiseAsFcnMu4LCNPVSecondary[y-minNPV]->Write();
					legend2->Clear();
				} else if (doData&&doMC) {
					legend4->AddEntry(NoiseAsFcnMu4EMNPVSecondary[y-minNPV],"R=0.4,EM Scale Data","P");
					legend4->AddEntry(NoiseAsFcnMu4LCNPVSecondary[y-minNPV],"R=0.4,LC Scale Data","P");
					legend4->AddEntry(MCNoiseAsFcnMu4EMNPVSecondary[y-minNPV],"R=0.4,EM Scale MC","P");
					legend4->AddEntry(MCNoiseAsFcnMu4LCNPVSecondary[y-minNPV],"R=0.4,LC Scale MC","P");
					saveFourHistoMarker(NoiseAsFcnMu4EMNPVSecondary[y-minNPV],NoiseAsFcnMu4LCNPVSecondary[y-minNPV],MCNoiseAsFcnMu4EMNPVSecondary[y-minNPV],MCNoiseAsFcnMu4LCNPVSecondary[y-minNPV],legend4,"<#mu>","Pile-up Noise[GeV]",stuffToWrite6,outPutDir,false,0.20,0.88,0,0);
					NoiseAsFcnMu4EMNPVSecondary[y-minNPV]->Write();
					NoiseAsFcnMu4LCNPVSecondary[y-minNPV]->Write();
					MCNoiseAsFcnMu4EMNPVSecondary[y-minNPV]->Write();
					MCNoiseAsFcnMu4LCNPVSecondary[y-minNPV]->Write();
					legend4->Clear();
				}
				stuffToWrite6.clear();
			}


		}
	}
	cout<<"heyoP"<<endl;
	outputFile2->Close();



}